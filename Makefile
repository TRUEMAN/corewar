# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/20 14:05:15 by olaurent          #+#    #+#              #
#    Updated: 2016/11/25 13:40:15 by olaurent         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	=	corewar
OS_NAME =	$(shell uname)

OUTPUT = echo -e
ifeq ($(OS_NAME), Darwin)
	OUTPUT = echo
endif

SRC_DIR =	src
INC_DIR =	include
OBJ_DIR =	obj

SDL_HEAD=	$(shell sdl2-config --cflags)
SDL_LIB	=	$(shell sdl2-config --libs) -lSDL2 -lSDL2_ttf -lSDL2_mixer

CC		=	clang
CFLAGS	=	-Wall -Wextra -Werror -g $(SDL_HEAD)

LIBS	=	-lm -L dep/libft/lib -lft $(SDL_LIB)
HEAD	=	-I $(INC_DIR) -I dep/libft/include

SRC_ASM =	op.c \
			compiler/compile.c \
			compiler/c_errors.c \
			compiler/c_arg.c \
			compiler/c_ins.c \
			compiler/c_print.c \
			compiler/c_label.c \
			compiler/c_validate.c \
			compiler/c_init.c \
			compiler/c_head.c \
			compiler/c_num.c \
			compiler/c_clean.c \
			compiler/main.c

SRC_DISASM =	op.c \
				disasm/decompile.c \
				disasm/utils.c \
				disasm/output.c \
				disasm/memory.c \
				disasm/param_tools.c \
				disasm/read_operation.c \
				disasm/resolve_labels.c \
				disasm/main.c

SRC_VM	=	corewar.c \
			cpu/check_pcode.c \
			cpu/eval_op.c \
			cpu/fetch_next_op.c \
			cpu/get_indirect.c \
			cpu/get_param.c \
			cpu/ops/add.c \
			cpu/ops/aff.c \
			cpu/ops/and.c \
			cpu/ops/fork.c \
			cpu/ops/ld.c \
			cpu/ops/ldi.c \
			cpu/ops/lfork.c \
			cpu/ops/live.c \
			cpu/ops/lld.c \
			cpu/ops/lldi.c \
			cpu/ops/or.c \
			cpu/ops/st.c \
			cpu/ops/sti.c \
			cpu/ops/sub.c \
			cpu/ops/xor.c \
			cpu/ops/zero.c \
			cpu/ops/zjmp.c \
			cpu/read_ram.c \
			cpu/write_in_ram.c \
			op.c \
			pc/pc_add_process.c \
			pc/pc_kill_process.c \
			pc/pc_loader.c \
			ram/load_champs.c \
			ram/set_up_ram.c \
			rev_byte.c \
			sound_system/sound_system.c \
			visu/byte_texture.c \
			visu/champ.c \
			visu/champ_draw.c \
			visu/champ_list.c \
			visu/champ_list_update.c \
			visu/colors.c \
			visu/console.c \
			visu/console_update.c \
			visu/error.c \
			visu/events.c \
			visu/events_processes.c \
			visu/events_sounds.c \
			visu/mem.c \
			visu/memview.c \
			visu/memview_events.c \
			visu/memview_update.c \
			visu/utils.c \
			visu/utils_sdl.c \
			visu/utils_math.c \
			visu/visual.c \
			visu/visual_events.c \
			visu/visual_loop.c \
			work_loop/init_vm.c \
			work_loop/load_champion.c \
			work_loop/pars_args.c \
			work_loop/arg_functions.c \
			work_loop/print_arg_functions.c \
			work_loop/pc_scan_steps.c \
			work_loop/verbosity.c \
			work_loop/verbosity_operations.c \
			work_loop/workloop_steps.c \
			work_loop/dump_ram.c

OBJ_ASM 	=	$(addprefix $(OBJ_DIR)/, $(SRC_ASM:.c=.o))
OBJ_DISASM  =	$(addprefix $(OBJ_DIR)/, $(SRC_DISASM:.c=.o))
OBJ_VM		=	$(addprefix $(OBJ_DIR)/, $(SRC_VM:.c=.o))


.PHONY: all clean fclean re bonus libft asm disasm vm refresh

all : $(NAME)

bonus : CFLAGS = -Wall -Wextra -Werror -g $(SDL_HEAD) -DBONUS=1
bonus :	refresh $(NAME) disasm

libft :
	@$(MAKE) -C dep/libft

asm : libft $(OBJ_ASM)
	@$(CC) -o asm $(OBJ_ASM) $(CFLAGS) $(HEAD) $(LIBS) \
		&& $(OUTPUT) "\033[32m[$(NAME):bin]\033[0m asm"
	
disasm : libft $(OBJ_DISASM)
	@$(CC) -o disasm $(OBJ_DISASM) $(CFLAGS) $(HEAD) $(LIBS) \
		&& $(OUTPUT) "\033[32m[$(NAME):bin]\033[0m disasm"

vm : libft $(OBJ_VM)
	@$(CC) -o $(NAME) $(OBJ_VM) $(CFLAGS) $(HEAD) $(LIBS) \
		&& $(OUTPUT) "\033[32m[$(NAME):bin]\033[0m $(NAME)"

$(NAME) : asm vm

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c
	@test -d $(dir $@) || mkdir -p $(dir $@) 2> /dev/null
	@$(CC) $(HEAD) -o $@ -c $< $(CFLAGS) && $(OUTPUT) "\033[33m[$(NAME):cmp]\033[0m $<"

refresh :
	@test ! -d $(OBJ_DIR) || \
		(rm -rf $(OBJ_DIR) && $(OUTPUT) "\033[31m[$(NAME):rmv]\033[0m Objects")
	@!(test -e $(NAME) || test -e disasm || test -e asm) || \
	(rm -rf $(NAME) asm disasm && $(OUTPUT) "\033[31m[$(NAME):rmv]\033[0m Binaries")

clean :
	@$(MAKE) -C dep/libft clean
	@test ! -d $(OBJ_DIR) || \
		(rm -rf $(OBJ_DIR) && $(OUTPUT) "\033[31m[$(NAME):rmv]\033[0m Objects")

fclean :
	@$(MAKE) -C dep/libft fclean
	@test ! -d $(OBJ_DIR) || \
		(rm -rf $(OBJ_DIR) && $(OUTPUT) "\033[31m[$(NAME):rmv]\033[0m Objects")
	@(rm -rf $(NAME) asm disasm && $(OUTPUT) "\033[31m[$(NAME):rmv]\033[0m Binaries")

re : fclean all
