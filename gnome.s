.name		"gnomebane"
.comment 	"Bring it on, moist towelettes!"

realload:
st r1, r16
fork:
live %0

forkwalls:
live1:	live %42
			fork %:agresswall
			live %42
			fork %:agresswall
live2:			live %42
			fork %:agresswall
live3:			live %42
			fork %:agresswall
live4:			live %42
			fork %:agresswall
live5:			live %42
			fork %:agresswall
live6:			live %42
			fork %:agresswall
			zjmp %:forkwalls

agresswall:
live8: live %42
			st r2, -236
			st r2, -145

			st r2, -254
			st r2, -163
			st r2, -272
			st r2, -181
			st r2, -290
			st r2, -199
			st r2, -308
			st r2, -217
live10:	live %42
			st r2, -326
			st r2, -235
			st r2, -344
			st r2, -253
			st r2, -362
				st r2, -271
			st r2, -380
			st r2, -289
			st r2, -398
			st r2, -307
live11:	live %42
			st r2, -416
			st r2, -325
				st r2, -434
			st r2, -343
live12:	live %42
			st r2, -452
			st r2, -361
				st r2, -470
			st r2, -379
			st r2, -488
			st r2, -397
live13:	live %42
			zjmp %:agresswall

live9:			live %56
			ld %0, r12
			live %42
			fork %:forkwalls
			live %42

load:
		sti r1, %:live1, %1
		sti r1, %:live2, %1
		sti r1, %:live3, %1
		sti r1, %:live4, %1
		sti r1, %:live5, %1
		sti r1, %:live6, %1
		sti r1, %:live8, %1
		sti r1, %:live9, %1
		sti r1, %:live10, %1
		sti r1, %:live11, %1
		sti r1, %:live12, %1
		sti r1, %:live13, %1
		ld %42, r9
		ld %0, r8
		live %42
		fork %:load
		live %42
		fork %:forkwalls
		live %42
			ld %76, r8
			aff r8
			ld %111, r8
			aff r8
			ld %114, r8
			aff r8
			ld %101, r8
			aff r8
			ld %109, r8
			aff r8
			ld %32, r8
			aff r8
		live %42
			ld %105, r8
			aff r8
			ld %112, r8
			aff r8
			ld %115, r8
			aff r8
			ld %117, r8
			aff r8
			ld %109, r8
			aff r8
			ld %32, r8
		live %42
			aff r8
			ld %32, r8
			aff r8
			ld %68, r8
			aff r8
			ld %111, r8
			aff r8
		live %42
			ld %108, r8
			aff r8
			ld %111, r8
			aff r8
			ld %114, r8
			aff r8
			ld %32, r8
			aff r8
			ld %115, r8
			aff r8
		live %42
			ld %105, r8
			aff r8
			ld %116, r8
			aff r8
			ld %32, r8
			aff r8
			zjmp %:load
