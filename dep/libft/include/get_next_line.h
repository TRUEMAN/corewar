/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 20:26:29 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/04 12:23:45 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include <fcntl.h>
# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <libft.h>
# define BUFF_SIZE 10
# define NO -2
# define ERR -1
# define END 0
# define YES 1
# ifndef NO_EMPTY
#  define NO_EMPTY 0
# endif
# ifndef NB_CHAR
#  define NB_CHAR 1
# endif
# ifndef NL
#  define NL 0
# endif

typedef struct		s_buf
{
	int				fd;
	int				p;
	int				size;
	char			*buf;
	char			*eof;
	struct s_buf	*next;
	struct s_buf	*prev;
}					t_buf;

int					get_next_line(int const fd, char **line);

#endif
