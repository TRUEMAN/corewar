/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_funs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/06 14:02:56 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 12:32:33 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FUNS_H
# define FT_FUNS_H
# include <stdlib.h>
# include "ft_defs.h"

int		ft_putstrn(int fd, char *s);
int		ft_putstrnstr(t_conv c, char *s);
int		ft_putwstrn(int fd, wchar_t *s, size_t max);
int		ft_putwstrnstr(t_conv c, wchar_t *s, size_t max);
size_t	ft_wstrlen(const wchar_t *s);
int		ft_utf16to8(wchar_t c, int x[7]);
int		ft_putnbrn(int fd, int n);
char	*ft_itoa_base(int value, int base, int up);
char	*ft_uitoa_base(unsigned int value, unsigned int base, int up);
char	*ft_ltoa_base(long value, int base, int up);
char	*ft_ultoa_base(unsigned int value, unsigned int base, int up);
char	*ft_lltoa_base(long long value, long long base, int up);
char	*ft_ulltoa_base(unsigned long long v, unsigned long long b, int up);

#endif
