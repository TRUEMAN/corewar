/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_defs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 18:24:05 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 18:24:52 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DEFS_H
# define FT_DEFS_H

typedef struct		s_flags
{
	int				none;
	int				alt;
	int				zero;
	int				neg;
	int				pos;
	int				sp;
	int				apstrph;
}					t_flags;

typedef struct		s_mods
{
	int				none;
	int				hh;
	int				h;
	int				l;
	int				ll;
	int				j;
	int				t;
	int				z;
	int				l_big;
}					t_mods;

typedef struct		s_conv
{
	int				fd;
	char			**s;
	unsigned int	out;
	char			type;
	int				prec;
	int				width;
	t_mods			mods;
	t_flags			flags;
}					t_conv;

typedef union		u_vars
{
	void				*p;
	int					*pi;
	char				*s;
	wchar_t				*ws;
	char				c;
	wchar_t				wc;
	unsigned char		uc;
	short				sh;
	unsigned short		ush;
	int					d;
	unsigned int		ud;
	long				l;
	unsigned long		ul;
	long long			ll;
	unsigned long long	ull;
}					t_vars;

#endif
