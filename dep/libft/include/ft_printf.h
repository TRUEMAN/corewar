/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 12:31:08 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 12:36:40 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdarg.h>
# include <stdlib.h>
# include <libft.h>
# include <unistd.h>
# include "ft_funs.h"
# define N_FLAGS 14
# define MIN(X, Y) ((Y < X) ? (Y) : (X))
# define MAX(X, Y) ((Y > X) ? (Y) : (X))

int					ft_printf(char *fmt, ...);
int					ft_dprintf(int fd, char *fmt, ...);
void				va_next(t_conv cc, t_vars *t_vv, va_list *ap);
int					va_make(t_conv cc, t_vars vv, int out);
int					deal_mods(t_conv *cc, char **f);
int					deal_flags(t_conv *cc, char **f);
int					deal_else(t_conv *cc, char **f, va_list *ap, va_list *ap_b);
int					deal_dollar(char **f, va_list *ap, va_list *ap_b);
void				init_cc(t_conv *cc);
int					get_conv(t_conv *cc, char **f, va_list *ap, va_list *ap_b);
int					put_num(t_conv cc, t_vars vv);
int					put_di(t_conv cc, t_vars vv);
int					put_cs(t_conv cc, t_vars vv);
void				put_extensions(t_conv c, char **s);

#endif
