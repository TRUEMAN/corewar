/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xstrjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 11:56:22 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:38 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xstrjoin(char const *s1, char const *s2)
{
	char	*result;

	result = ft_xstrnew(ft_strlen(s1) + ft_strlen(s2));
	ft_strcpy(result, s1);
	ft_strcat(result, s2);
	return (result);
}
