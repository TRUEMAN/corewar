/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puts.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 21:15:22 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 12:57:49 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			put_di(t_conv cc, t_vars vv)
{
	char	*s;
	int		out;
	t_mods	m;

	ft_memcpy(&m, &(cc.mods), sizeof(t_mods));
	s = NULL;
	if (m.ll || m.j || m.l || m.z)
		s = ft_lltoa_base(vv.ll, 10, 0);
	else if (m.hh)
		s = ft_itoa_base(vv.c, 10, 0);
	else if (m.h)
		s = ft_itoa_base(vv.sh, 10, 0);
	else if (m.none)
		s = ft_itoa_base(vv.d, 10, 0);
	if (s)
	{
		put_extensions(cc, &s);
		out = (cc.fd == -42) ? ft_strlen(s) : ft_putstrn(cc.fd, s);
		if (cc.fd == -42)
			ft_memmove(*cc.s + cc.out, s, out);
		free(s);
		return (out);
	}
	return (0);
}

int			put_u(t_conv cc, t_vars vv)
{
	char	*s;
	int		out;
	t_mods	m;

	ft_memcpy(&m, &(cc.mods), sizeof(t_mods));
	s = NULL;
	if (m.ll || m.j || m.l || m.z)
		s = ft_ulltoa_base(vv.ull, 10, 0);
	else if (m.hh)
		s = ft_uitoa_base(vv.uc, 10, 0);
	else if (m.h)
		s = ft_uitoa_base(vv.ush, 10, 0);
	else if (m.none)
		s = ft_uitoa_base(vv.ud, 10, 0);
	if (s)
	{
		put_extensions(cc, &s);
		out = (cc.fd == -42) ? ft_strlen(s) : ft_putstrn(cc.fd, s);
		if (cc.fd == -42)
			ft_memmove(*cc.s + cc.out, s, out);
		free(s);
		return (out);
	}
	return (0);
}

int			put_o(t_conv cc, t_vars vv)
{
	char	*s;
	int		out;
	t_mods	m;

	ft_memcpy(&m, &(cc.mods), sizeof(t_mods));
	s = NULL;
	if (m.ll || m.j || m.l || m.z)
		s = ft_ulltoa_base(vv.ull, 8, 0);
	else if (m.hh)
		s = ft_uitoa_base(vv.uc, 8, 0);
	else if (m.h)
		s = ft_uitoa_base(vv.ush, 8, 0);
	else if (m.none)
		s = ft_uitoa_base(vv.ud, 8, 0);
	if (s)
	{
		put_extensions(cc, &s);
		out = (cc.fd == -42) ? ft_strlen(s) : ft_putstrn(cc.fd, s);
		if (cc.fd == -42)
			ft_memmove(*cc.s + cc.out, s, out);
		free(s);
		return (out);
	}
	return (0);
}

int			put_num(t_conv cc, t_vars vv)
{
	char	*s;
	int		out;

	s = NULL;
	if (cc.type == 'u' || cc.type == 'o')
		return ((cc.type == 'u') ? (put_u(cc, vv)) : (put_o(cc, vv)));
	else if (ft_strchr("xXp", cc.type))
	{
		if (cc.mods.ll || cc.mods.l || cc.mods.j || cc.mods.z)
			s = ft_ulltoa_base(vv.ull, 16, (cc.type == 'X'));
		else
			s = ft_uitoa_base(vv.ud, 16, (cc.type == 'X'));
		cc.flags.alt = (vv.ud == 0 && !cc.mods.ll) ? 0 : cc.flags.alt;
	}
	if (s)
	{
		put_extensions(cc, &s);
		out = (cc.fd == -42) ? ft_strlen(s) : ft_putstrn(cc.fd, s);
		if (cc.fd == -42)
			ft_memmove(*cc.s + cc.out, s, out);
		free(s);
		return (out);
	}
	return (0);
}
