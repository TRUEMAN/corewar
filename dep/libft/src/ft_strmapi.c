/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:21:00 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/03 17:13:02 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	size;
	char			*str;

	if (s == NULL)
		return (NULL);
	size = ft_strlen(s);
	str = ft_strnew(size);
	if (str)
		while (size--)
			str[size] = f(size, s[size]);
	return (str);
}
