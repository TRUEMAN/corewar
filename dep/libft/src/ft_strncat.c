/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 19:13:22 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/16 13:45:02 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	size_t	len;
	size_t	iter;

	len = ft_strlen(s1);
	iter = 0;
	while (iter < n && s2[iter])
	{
		s1[len + iter] = s2[iter];
		iter++;
	}
	s1[len + iter] = '\0';
	while (++iter < n)
		s1[len + iter] = '\0';
	return (s1);
}
