/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:12:34 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/15 14:40:38 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	if (s2[0] == '\0')
		return ((char *)s1);
	while (*s1 && n > 0)
	{
		i = 0;
		while (s1[i] == s2[i] && s1[i] && n - i > 0)
			i++;
		if (s2[i] == '\0' && n >= i)
			return ((char *)s1);
		s1++;
		n--;
	}
	return (NULL);
}
