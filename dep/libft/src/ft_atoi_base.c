/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 18:19:24 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/05 13:12:40 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int	ft_atoi_get_number(char c, int base)
{
	int	value;

	if (c >= '0' && c <= '9')
		value = (c - '0');
	else if (c >= 'A' && c <= 'Z')
		value = (c - 'A') + 10;
	else
		return (-1);
	if (value >= base)
		return (-1);
	return (value);
}

int			ft_atoi_base(const char *str, int base)
{
	int	sign;
	int	value;
	int	result;

	if (base > 36 || base < 2)
		return (0);
	result = 0;
	while (*str == (int)' ' || (*str >= (int)'\t' && *str <= (int)'\r'))
		str++;
	sign = (*str == '-') ? -1 : 1;
	if (*str == '-' || *str == '+')
	{
		if (base != 10)
			return (0);
		str++;
	}
	while (*str != '\0')
	{
		value = ft_atoi_get_number(*str, base) % base;
		if (value == -1)
			break ;
		result = result * base + value;
		str++;
	}
	return (result * sign);
}
