/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xstrsub.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 18:19:05 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:38:17 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xstrsub(char const *s, unsigned int start, size_t len)
{
	char			*result;

	result = ft_xstrnew(len);
	ft_strncpy(result, s + start, len);
	return (result);
}
