/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xitoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 17:51:42 by jandre-j          #+#    #+#             */
/*   Updated: 2016/10/26 13:16:59 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xitoa(int n)
{
	size_t	size;
	long	buffer;
	int		negative;
	char	*rslt;

	size = 0;
	buffer = (long)n;
	negative = (buffer < 0);
	while (buffer /= 10)
		size++;
	rslt = (char *)ft_xmalloc(sizeof(char) * (++size + negative + 1));
	rslt[size + negative] = '\0';
	rslt[0] = '-';
	buffer = (long)n;
	buffer = (negative ? -buffer : buffer);
	while (size--)
	{
		rslt[size + negative] = buffer % 10 + '0';
		buffer /= 10;
	}
	return (rslt);
}
