/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 11:56:40 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/15 15:10:49 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strtrim(char const *s)
{
	char	*result;
	int		start;
	int		end;
	int		pos;

	start = 0;
	end = ft_strlen((char *)s);
	while (end > 0 && (s[end - 1] == ' ' || s[end - 1] == '\t'
		|| s[end - 1] == '\n'))
		end--;
	while ((s[start] == ' ' || s[start] == '\t' || s[start] == '\n')
		&& start < end)
		start++;
	result = ft_strnew(end - start);
	if (result == NULL)
		return (NULL);
	pos = end - start;
	while (pos-- > 0)
		result[pos] = s[start + pos];
	result[end] = '\0';
	return (result);
}
