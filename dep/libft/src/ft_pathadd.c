/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pathadd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:48:36 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:32 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_pathadd(const char *path, const char *name)
{
	char	*result;
	size_t	len;
	size_t	slash;

	len = ft_strlen(path);
	slash = 1;
	if (len == 0 || path[len - 1] == '/')
		slash = 0;
	if (!(result = ft_strnew(slash + len + ft_strlen(name))))
		return (NULL);
	ft_strcpy(result, path);
	if (slash != 0)
	{
		result[len] = '/';
		result[len + 1] = '\0';
	}
	ft_strcat(result, name);
	return (result);
}
