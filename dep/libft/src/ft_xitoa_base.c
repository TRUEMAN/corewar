/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xitoa_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 17:51:52 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:36:59 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xitoa_base(int n, int base)
{
	size_t	s;
	long	nb;
	int		ng;
	char	*r;

	if (base > 36 || base < 2)
		return (NULL);
	s = 0;
	nb = (long)n;
	ng = (nb < 0);
	if (base != 10 && ng)
		return (NULL);
	while (nb /= base)
		s++;
	r = (char *)ft_xmalloc(sizeof(char) * (++s + ng + 1));
	r[0] = '-';
	nb = (long)n;
	nb = (ng ? -nb : nb);
	while (s--)
	{
		r[s + ng] = nb % base + (nb % base >= 10 ? 'A' - 10 : '0');
		nb /= base;
	}
	return (r);
}
