/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:02:35 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/15 14:21:15 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strmap(char const *s, char (*f)(char))
{
	size_t	size;
	char	*str;

	if (s == NULL)
		return (NULL);
	size = ft_strlen(s);
	str = ft_strnew(size);
	if (str)
	{
		while (size--)
			str[size] = f(s[size]);
	}
	return (str);
}
