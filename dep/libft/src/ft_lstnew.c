/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:37:38 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/09 12:35:50 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*list;

	list = (t_list *)ft_memalloc(sizeof(*list));
	if (list)
	{
		list->next = NULL;
		if (content && content_size != 0)
		{
			list->content = ft_memalloc(content_size);
			if (!list->content)
			{
				ft_memdel((void **)&list);
				return (NULL);
			}
			list->content_size = content_size;
			ft_memmove(list->content, content, content_size);
		}
		else
		{
			list->content = NULL;
			list->content_size = 0;
		}
	}
	return (list);
}
