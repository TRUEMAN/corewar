/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 17:25:15 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/28 17:25:31 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include "ft_funs.h"

size_t	ft_wstrlen(const wchar_t *s)
{
	size_t	len;
	size_t	i;

	len = 0;
	i = 0;
	while (s[i])
		len += ft_utf16to8(*((wchar_t *)s + i++), 0);
	return (len);
}
