/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_switch.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 13:23:22 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 14:31:53 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	va_next_di(t_conv c, t_vars *t_vv, va_list *ap)
{
	if (c.mods.ll == 1)
		(*t_vv).ll = va_arg(*ap, long long);
	else if (c.mods.l == 1)
		(*t_vv).l = va_arg(*ap, long);
	else if (c.mods.z == 1)
		(*t_vv).l = va_arg(*ap, long);
	else if (c.mods.j == 1)
		(*t_vv).ll = va_arg(*ap, intmax_t);
	else if (c.mods.hh == 1)
		(*t_vv).c = va_arg(*ap, int);
	else if (c.mods.h == 1)
		(*t_vv).sh = va_arg(*ap, int);
	else
		(*t_vv).d = va_arg(*ap, int);
}

void	va_next_oux(t_conv c, t_vars *t_vv, va_list *ap)
{
	if (c.mods.ll == 1)
		(*t_vv).ull = va_arg(*ap, unsigned long long);
	else if (c.mods.l == 1)
		(*t_vv).ull = va_arg(*ap, unsigned long long);
	else if (c.mods.z == 1)
		(*t_vv).ul = va_arg(*ap, size_t);
	else if (c.mods.j == 1)
		(*t_vv).ull = va_arg(*ap, uintmax_t);
	else if (c.mods.hh == 1)
		(*t_vv).uc = va_arg(*ap, unsigned int);
	else if (c.mods.h == 1)
		(*t_vv).ush = va_arg(*ap, unsigned int);
	else
		(*t_vv).ud = va_arg(*ap, unsigned int);
}

void	va_next_sc(t_conv c, t_vars *t_vv, va_list *ap)
{
	if (c.type == 's' && c.mods.l)
		(*t_vv).ws = va_arg(*ap, wchar_t *);
	else if (c.type == 's')
		(*t_vv).s = va_arg(*ap, char *);
	else if (c.type == 'c' && c.mods.l)
		(*t_vv).wc = va_arg(*ap, wchar_t);
	else if (c.type == 'c')
		(*t_vv).c = va_arg(*ap, int);
}

void	va_next(t_conv cc, t_vars *t_vv, va_list *ap)
{
	if (ft_strchr("sc", cc.type))
		va_next_sc(cc, t_vv, ap);
	else if (ft_strchr("di", cc.type))
		va_next_di(cc, t_vv, ap);
	else if (ft_strchr("ouxX", cc.type))
		va_next_oux(cc, t_vv, ap);
	else if (ft_strchr("DOU", cc.type))
		t_vv->l = va_arg(*ap, long);
	else if (cc.type == 'p')
		t_vv->p = va_arg(*ap, void *);
	else if (cc.type == 'n')
		t_vv->pi = va_arg(*ap, int *);
}

int		va_make(t_conv cc, t_vars vv, int out)
{
	if (!ft_strchr("cCdDioOpsSuUxXn", cc.type))
	{
		vv.c = cc.type;
		cc.type = 'c';
	}
	if (ft_strchr("sS", cc.type) && vv.s == 0)
		vv.s = NULL;
	if (ft_strchr("sScC", cc.type))
		return (put_cs(cc, vv));
	else if (ft_strchr("dDi", cc.type))
		return (put_di(cc, vv));
	else if (ft_strchr("oOuUxX", cc.type))
		return (put_num(cc, vv));
	else if (cc.type == 'p')
	{
		cc.mods.ll = 1;
		cc.mods.l = 1;
		cc.flags.alt = 1;
		return (put_num(cc, vv));
	}
	else if (cc.type == 'n')
		*vv.pi = out;
	return (0);
}
