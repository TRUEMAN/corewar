/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 20:26:16 by olaurent          #+#    #+#             */
/*   Updated: 2016/09/02 16:34:00 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_buf	*pick_buf(t_buf **lst, int const fd, char **line)
{
	t_buf	*elem;
	int		i;

	i = 1;
	if (*lst == NULL && i-- && (!(*lst = (t_buf *)malloc(sizeof(t_buf)))))
		return (NULL);
	(*lst)->prev = NULL;
	elem = *lst;
	while (i && elem->next && elem->fd != fd)
		elem = elem->next;
	*line = NULL;
	if (i && elem->fd == fd)
		return (elem);
	if (i && elem->fd != fd && !(elem->next = (t_buf *)malloc(sizeof(t_buf))))
		return (NULL);
	if (elem != *lst && elem->next)
		elem->next->prev = (i && elem->fd != fd) ? elem : NULL;
	elem = (i && elem->fd != fd) ? elem->next : elem;
	if (!(elem->buf = (char *)malloc(sizeof(char) * BUFF_SIZE + 1)))
		return (NULL);
	elem->fd = fd;
	elem->next = NULL;
	elem->eof = NULL;
	elem->p = elem->size;
	return (elem);
}

static int		fill_b(t_buf *elem)
{
	int			i;

	i = read(elem->fd, elem->buf, BUFF_SIZE);
	if (i < 0)
		return (ERR);
	elem->p = 0;
	elem->size = i;
	if (i == 0)
	{
		elem->eof = elem->buf;
		*(elem->eof) = '\0';
	}
	return (YES);
}

static int		fill_l(char **line, t_buf *elem, char *end, int *l_siz)
{
	char	*tmp;
	int		len;

	tmp = *line;
	len = elem->size - elem->p;
	if (end || elem->eof)
		len = 1 + (int)(((end) ? end : elem->eof) - elem->buf) - elem->p;
	if (!(*line = (char *)malloc(sizeof(char) * (*l_siz + len))))
		return (ERR);
	if (tmp && *l_siz)
		ft_memcpy(*line, tmp, *l_siz);
	if (*l_siz && tmp)
		free(tmp);
	ft_memcpy(*line + *l_siz, elem->buf + elem->p, len);
	*l_siz += len;
	if (end || elem->eof)
		(*line)[*l_siz - 1] = (NL && end != elem->eof) ? '\n' : '\0';
	elem->p += len;
	if (end || elem->eof)
		return ((end) ? (YES) : (END));
	return (NO);
}

static void		noempty(char **line, int *l_siz)
{
	int		len;
	int		i;
	int		j;
	char	*tmp;

	len = 0;
	i = 0;
	while (i < *l_siz)
		len += (((*line)[i++] != '\0') ? 1 : 0);
	if (!NO_EMPTY || !*l_siz || !len || len == *l_siz - 1)
		return ;
	tmp = *line;
	*line = (char *)malloc(sizeof(char) * (len + 1));
	i = 0;
	j = 0;
	while (i < len)
	{
		if (tmp[j])
			(*line)[i++] = tmp[j];
		j++;
	}
	(*line)[i] = 0;
	free(tmp);
	*l_siz = len + 1;
}

int				get_next_line(int const fd, char **line)
{
	static t_buf	*lst;
	t_buf			*e;
	int				s;
	int				out;

	if (line == NULL || (e = pick_buf(&lst, fd, line)) == NULL)
		return (ERR);
	s = 0;
	out = (e->eof && e->buf + e->p >= e->eof) ? END : NO;
	while (out == NO)
	{
		if (e->p == e->size && (out = fill_b(e)) == ERR)
			return (out);
		out = fill_l(line, e, ft_memchr(e->buf + e->p, 10, e->size - e->p), &s);
	}
	noempty(line, &s);
	out = (out == END && s > 1) ? YES : out;
	if (out == END && s <= 1 && e->next)
		e->next->prev = e->prev;
	lst = (out == END && s <= 1 && e->prev == NULL) ? e->next : lst;
	if (out == END && e->prev)
		e->prev->next = e->next;
	if (out == END && e && ((e->buf && (((int (*)(void *))free)(e->buf))) || 1))
		free(e);
	return ((out == YES && NB_CHAR) ? (s) : (out));
}
