/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uitoa_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 11:21:02 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/09 21:07:41 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_uitoa_base(unsigned int value, unsigned int base, int up)
{
	char				*s;
	unsigned int		len;
	unsigned int		log;

	if (base < 2 || base > 16)
		return (NULL);
	len = 1;
	log = base;
	while (value / log >= base && ++len)
		log *= base;
	len += (value >= base) ? 1 : 0;
	if (!(s = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	s[len] = 0;
	if (value == 0)
		s[0] = '0';
	while (value)
	{
		log = value - base * (value / base);
		s[len - 1] = (log < 10) ? log + '0' : log + 'a' + up * ('A' - 'a') - 10;
		value /= base;
		len--;
	}
	return (s);
}
