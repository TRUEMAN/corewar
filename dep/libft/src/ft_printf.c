/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 18:00:39 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 20:03:19 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

int		next_s(char **s, unsigned int *c, t_conv *cc)
{
	int		i;

	i = 0;
	while ((*s)[i] && (*s)[i] != '%')
		++i;
	if (cc->fd != -42)
		*c += write(cc->fd, *s, i);
	else
	{
		ft_memmove(*cc->s + cc->out, *s, i);
		cc->out += i;
	}
	*s += i;
	if (**s == 0)
		return (0);
	return (1);
}

int		next(char **s, unsigned int *c, t_conv cc)
{
	int		i;

	i = 0;
	while ((*s)[i] && (*s)[i] != '%')
		++i;
	if (cc.fd != -42)
		*c += write(cc.fd, *s, i);
	else
		ft_memmove(*cc.s + cc.out, *s, i);
	*s += i;
	if (**s == 0)
		return (0);
	return (1);
}

int		ft_printf(char *fmt, ...)
{
	va_list			ap;
	va_list			ap_back;
	t_conv			cc;
	t_vars			vv;
	unsigned int	out;

	cc.fd = 1;
	va_start(ap, fmt);
	va_copy(ap_back, ap);
	out = 0;
	while (*fmt && next(&fmt, &out, cc))
	{
		if (0 == get_conv(&cc, &fmt, &ap, &ap_back))
			continue ;
		vv = (t_vars)0;
		va_next(cc, &vv, &ap);
		out += va_make(cc, vv, out);
		fmt++;
	}
	va_end(ap);
	return (out);
}

int		ft_dprintf(int fd, char *fmt, ...)
{
	va_list			ap;
	va_list			ap_back;
	t_conv			cc;
	t_vars			vv;
	unsigned int	out;

	cc.fd = fd;
	va_start(ap, fmt);
	va_copy(ap_back, ap);
	out = 0;
	while (*fmt && next(&fmt, &out, cc))
	{
		if (0 == get_conv(&cc, &fmt, &ap, &ap_back))
			continue ;
		vv = (t_vars)0;
		va_next(cc, &vv, &ap);
		out += va_make(cc, vv, out);
		fmt++;
	}
	va_end(ap);
	return (out);
}

int		ft_sprintf(char *str, char *fmt, ...)
{
	va_list			ap;
	va_list			ap_back;
	t_conv			cc;
	t_vars			vv;

	cc.fd = -42;
	cc.s = &str;
	cc.out = 0;
	va_start(ap, fmt);
	va_copy(ap_back, ap);
	while (*fmt && next_s(&fmt, &cc.out, &cc))
	{
		if (0 == get_conv(&cc, &fmt, &ap, &ap_back))
			continue ;
		vv = (t_vars)0;
		va_next(cc, &vv, &ap);
		cc.out += va_make(cc, vv, cc.out);
		fmt++;
	}
	va_end(ap);
	cc.s[0][cc.out] = 0;
	return (cc.out);
}
