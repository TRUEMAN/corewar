/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 18:36:49 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/15 14:53:39 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

static int		ft_strsplit_addword(char **array, size_t index, char *s, char c)
{
	size_t	iter;

	iter = 0;
	while (s[iter] != c && s[iter] != '\0')
		iter++;
	if ((array[index] = ft_strnew(iter)) == NULL)
		return (0);
	ft_strncpy(array[index], s, iter);
	return (1);
}

static size_t	ft_strsplit_scan(char **array, char *s, char c)
{
	size_t	count;
	int		is_word;

	count = 0;
	is_word = 0;
	while (*s != '\0')
	{
		if (!is_word && *s != c)
		{
			is_word = 1;
			if (array && !ft_strsplit_addword(array, count, s, c))
				return (0);
			count++;
		}
		else if (is_word && *s == c)
			is_word = 0;
		s++;
	}
	return (count);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**array;
	size_t	array_size;

	if (s == NULL)
		return (NULL);
	array_size = ft_strsplit_scan(NULL, (char *)s, c);
	array = ft_memalloc((array_size + 1) * sizeof(char *));
	if (!array)
		return (NULL);
	if (array_size != 0 && !ft_strsplit_scan(array, (char *)s, c))
	{
		while (array_size--)
			if (array[array_size])
				ft_memdel((void **)array + array_size);
		free(array);
		return (NULL);
	}
	return (array);
}
