/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwstrn.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:36:00 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 19:52:58 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "ft_funs.h"
#include <libft.h>

int		ft_putwstrnstr(t_conv c, wchar_t *s, size_t max)
{
	int		i;
	int		j;
	int		out;
	int		x[7];

	if (s == NULL)
	{
		ft_memmove(*c.s + c.out, "(null)", max);
		return (max);
	}
	i = 0;
	out = 0;
	while ((*((wchar_t *)s + i) || out == 0) && max > (size_t)out)
	{
		j = ft_utf16to8(*((wchar_t *)s + i), x);
		j--;
		while (j >= 0 && max > (size_t)out + j)
			(*c.s)[c.out + out++] = x[j--];
		i++;
	}
	return (out);
}

int		ft_putwstrn(int fd, wchar_t *s, size_t max)
{
	int		i;
	int		j;
	int		out;
	int		x[7];

	if (s == NULL)
	{
		write(fd, "(null)", max);
		return (max);
	}
	i = 0;
	out = 0;
	while ((*((wchar_t *)s + i) || out == 0) && max > (size_t)out)
	{
		j = ft_utf16to8(*((wchar_t *)s + i), x);
		j--;
		while (j >= 0 && max > (size_t)out + j)
			out += write(fd, x + j--, 1);
		i++;
	}
	return (out);
}
