/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: guiricha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 14:33:32 by guiricha          #+#    #+#             */
/*   Updated: 2016/11/24 14:39:58 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <limits.h>

int	ft_strtoint(char *str, int *ret)
{
	int		sign;
	int		value;
	long	result;

	if (!ft_str_is_number(str))
		return (-3);
	if (!*str)
		return (-4);
	result = 0;
	while (*str == (int)' ' || (*str >= (int)'\t' && *str <= (int)'\r'))
		str++;
	sign = (*str == '-') ? -1 : 1;
	str += (*str == '-' || *str == '+');
	while (*str >= '0' && *str <= '9')
	{
		value = *str - '0';
		result = result * 10 + (long)value;
		str++;
	}
	result = result * sign;
	if ((result <= INT_MAX && result >= INT_MIN) && ((*ret = result) || 1))
		return (1);
	if (result < INT_MIN)
		return (-1);
	return (-2);
}
