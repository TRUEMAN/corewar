/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchrc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 09:47:54 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/15 14:41:54 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strrchr(const char *s, int c)
{
	size_t	position;

	position = ft_strlen(s);
	while (position > 0 && s[position] != (char)c)
		position--;
	if (s[position] == (char)c)
		return ((char *)s + position);
	return (NULL);
}
