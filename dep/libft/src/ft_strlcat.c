/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 19:39:19 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/17 13:55:32 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dst_size;
	size_t	src_size;
	size_t	backup;

	dst_size = 0;
	src_size = 0;
	while (dst[dst_size] != '\0')
	{
		if (dst_size++ >= size)
			return (size + ft_strlen(src));
	}
	backup = dst_size;
	while (dst_size < size - 1 && src[src_size] != '\0')
		dst[dst_size++] = src[src_size++];
	dst[dst_size] = '\0';
	return (backup + ft_strlen(src));
}
