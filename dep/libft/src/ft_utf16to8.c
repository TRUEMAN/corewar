/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utf16to8.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:36:00 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/12 13:37:34 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static void	set_x(int x[7], int i, int val, int add)
{
	if (!x)
		return ;
	x[i] = val + add * x[i];
}

int			ft_utf16to8(wchar_t c, int x[7])
{
	int		i;
	wchar_t	tmp;

	i = 0;
	tmp = c;
	while (i < 6 && x)
	{
		x[i++] = 128 + tmp % 64;
		tmp /= 64;
	}
	i = 0;
	if (++i && c < 128)
		set_x(x, 0, c, 0);
	else if (++i && c < 2048)
		set_x(x, 1, 64, 1);
	else if (++i && c < 65536)
		set_x(x, 2, 96, 1);
	else if (++i && c < 2097152)
		set_x(x, 3, 112, 1);
	else if (++i && c < 67108864)
		set_x(x, 4, 120, 1);
	else if (x)
		set_x(x, 5, 124, 1);
	set_x(x, i, 0, 0);
	return (i);
}
