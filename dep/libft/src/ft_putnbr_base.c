/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 22:08:40 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/16 13:18:13 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	ft_putnbr_base(int n, int base)
{
	long	nb;

	if (base > 36 || base < 2 || (n < 0 && base != 10))
		return ;
	nb = (long)n;
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb >= base)
		ft_putnbr_base(nb / base, base);
	ft_putchar(nb % base + (nb % base >= 10 ? 'A' - 10 : '0'));
}
