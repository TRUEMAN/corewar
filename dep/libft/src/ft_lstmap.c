/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 11:42:24 by jandre-j          #+#    #+#             */
/*   Updated: 2015/11/29 16:08:42 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_lst;
	t_list	*buffer;

	buffer = f(lst);
	new_lst = buffer;
	while (lst->next)
	{
		lst = lst->next;
		buffer->next = f(lst);
		buffer = buffer->next;
	}
	return (new_lst);
}
