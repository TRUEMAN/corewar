/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 11:56:32 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/15 13:41:03 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*destination;
	char	*source;
	size_t	index;

	destination = (char *)dst;
	source = (char *)src;
	if (source <= destination)
	{
		index = len;
		while (index-- > 0)
			destination[index] = source[index];
		return (dst);
	}
	index = 0;
	while (index < len)
	{
		destination[index] = source[index];
		index++;
	}
	return (dst);
}
