/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xpathname.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/29 19:47:22 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 19:47:23 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xpathname(const char *path)
{
	char	*str;
	size_t	len;
	size_t	found;
	size_t	iter;

	found = 0;
	len = 0;
	while (path[len] != '\0' && !(path[len] == '/' && path[len + 1] == '\0'))
		if (path[len++] == '/')
			found = len;
	str = ft_xmalloc(sizeof(char) * (len - found));
	iter = 0;
	while (found + iter < len)
	{
		str[iter] = path[found + iter];
		iter++;
	}
	str[iter] = '\0';
	return (str);
}
