/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_num.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 11:08:19 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 09:44:52 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	calc_zer(size_t pad[3], char *s, t_conv c, size_t l)
{
	int		z;

	z = 0;
	if (ft_strchr("dDi", c.type))
		z = (*s != '-' && (c.flags.pos || c.flags.sp));
	pad[0] = 0;
	if (c.prec > -1)
	{
		if (ft_strchr("dDioOuUxXp", c.type) && c.prec >= (int)l)
			pad[0] = c.prec - l + (s[0] == '-');
	}
	else if (c.width > -1 && c.flags.zero)
	{
		if (c.width >= (int)l + z)
			pad[0] = c.width - l - z;
	}
	if (ft_strchr("oO", c.type) && c.flags.alt && pad[0] < 1 && s[0] != '0')
		pad[0] = 1;
	if (ft_strchr("xX", c.type) && c.flags.alt && c.prec > -1 && s[0] != '0')
		pad[0] += 2;
	else if ('p' == c.type)
		pad[0] = (c.prec > -1) ? pad[0] + 2 : MAX(pad[0], 2);
	else if (ft_strchr("xX", c.type) && c.flags.alt && pad[0] < 2 && s[0] != 48)
		pad[0] = 2;
	return (z);
}

static void	calc_bef(size_t pad[3], char *s, t_conv c, size_t l)
{
	int		z;

	pad[1] = 0;
	if (c.flags.neg)
		return ;
	z = 0;
	if (ft_strchr("dDi", c.type))
		z = (*s != '-' && (c.flags.pos || c.flags.sp));
	if (c.width)
	{
		if (c.width > -1 && (size_t)c.width >= l + pad[0] + z)
			pad[1] = c.width - l - pad[0] - z;
	}
}

static void	calc_aft(size_t pad[3], char *s, t_conv c, size_t l)
{
	int		z;

	z = 0;
	if (ft_strchr("dDi", c.type))
		z = (*s != '-' && (c.flags.pos || c.flags.sp));
	pad[2] = 0;
	if (!c.flags.neg)
		return ;
	if (c.width > -1 && (size_t)c.width > l + pad[1] + pad[0] + z)
		pad[2] = c.width - l - pad[1] - pad[0] - z;
}

static void	put_alt(char *tmp, char **s, size_t p[3], t_conv c)
{
	if (c.flags.alt && ft_strchr("xX", c.type) && tmp[0] != '0')
		(*s)[p[1] + 1] = c.type;
	(*s)[p[1] + 1] = (c.type == 'p') ? 'x' : (*s)[p[1] + 1];
	if (c.flags.alt && ft_strchr("xXoOp", c.type) && tmp[0] != '0')
		(*s)[p[1]] = '0';
}

void		put_extensions(t_conv c, char **s)
{
	size_t	l;
	size_t	p[3];
	char	*tmp;
	int		z;

	l = ft_strlen(*s) - (!c.prec && **s == '0' && !*(*s + 1) &&
			!(c.flags.alt && c.type == 'o'));
	tmp = *s;
	z = calc_zer(p, *s, c, l);
	calc_bef(p, *s, c, l);
	calc_aft(p, *s, c, l);
	if ((*s = ft_memalloc(p[0] + z + p[1] + l + p[2] + 1)))
	{
		ft_memset((*s) + p[0] + z + p[1] + l, ' ', p[2]);
		ft_memcpy(*s + p[0] + z + p[1], tmp, l);
		p[0] += (tmp[0] == '-') ? 1 : 0;
		ft_memset((*s) + z + p[1], '0', p[0]);
		(*s)[p[1]] = (tmp[0] == '-') ? '-' : (*s)[p[1]];
		put_alt(tmp, s, p, c);
		if (z)
			(*s)[p[1]] = (c.flags.pos) ? '+' : ' ';
		ft_memset((*s), ' ', p[1]);
	}
	free(tmp);
}
