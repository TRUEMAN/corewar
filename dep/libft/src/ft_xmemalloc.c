/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xmemalloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 13:44:18 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:23 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*ft_xmemalloc(size_t size)
{
	void	*address;

	address = ft_xmalloc(size);
	while (size--)
		*(char *)(address + size) = '\0';
	return (address);
}
