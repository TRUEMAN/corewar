/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mods.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 18:01:08 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 10:59:07 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

void	init_cc(t_conv *cc)
{
	cc->type = 0;
	cc->prec = -1;
	cc->width = -1;
	cc->mods.none = 1;
	cc->mods.hh = 0;
	cc->mods.h = 0;
	cc->mods.l = 0;
	cc->mods.ll = 0;
	cc->mods.j = 0;
	cc->mods.t = 0;
	cc->mods.z = 0;
	cc->mods.l_big = 0;
	cc->flags.none = 1;
	cc->flags.alt = 0;
	cc->flags.zero = 0;
	cc->flags.neg = 0;
	cc->flags.pos = 0;
	cc->flags.sp = 0;
	cc->flags.apstrph = 0;
}

void	check_none(t_conv *c)
{
	if (!c->mods.l && !c->mods.h && !c->mods.j && !c->mods.z)
		c->mods.none = 1;
	else
		c->mods.none = 0;
	if (!c->flags.alt && !c->flags.zero && !c->flags.neg && !c->flags.sp &&
			!c->flags.pos && !c->flags.apstrph)
		c->flags.none = 1;
	else
		c->flags.none = 0;
	if (!ft_strchr("dDi", c->type))
	{
		c->flags.sp = 0;
		c->flags.pos = 0;
	}
}

int		get_conv(t_conv *cc, char **fmt, va_list *ap, va_list *ap_back)
{
	char	*tmp;
	int		out;

	++*fmt;
	init_cc(cc);
	out = 1;
	while (out && **fmt && !(tmp = ft_strchr("sSpdDioOuUxXcnC", **fmt)))
	{
		out = deal_mods(cc, fmt);
		out += deal_dollar(fmt, ap, ap_back);
		out += deal_flags(cc, fmt);
		out += deal_else(cc, fmt, ap, ap_back);
		(*fmt)++;
	}
	*fmt -= (out == 0) ? 1 : 0;
	if (**fmt == 0)
		return (0);
	cc->type = **fmt;
	check_none(cc);
	if (ft_strchr("DOUSC", cc->type))
	{
		cc->mods.l = 1;
		cc->type = ft_tolower(cc->type);
	}
	return (1);
}
