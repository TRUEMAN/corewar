/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_char.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 13:30:42 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 20:01:58 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	wmin(t_conv cc, t_vars vv)
{
	int		min;
	int		i;
	int		c;

	if (!vv.ws)
		return (cc.prec);
	i = 0;
	min = 0;
	while (((wchar_t *)vv.ws)[i])
	{
		c = ft_utf16to8(((wchar_t *)vv.ws)[i++], 0);
		if (min + c > cc.prec)
			break ;
		min += c;
	}
	return (min);
}

static int	calc_min(t_conv cc, t_vars vv)
{
	int		min;

	min = (cc.type == 's' && cc.prec > -1) ? cc.prec : 1;
	min = (cc.type == 'c' && cc.mods.l) ? ft_utf16to8(vv.wc, 0) : min;
	if (cc.type == 's' && vv.s == 0)
		min = (cc.prec > -1) ? MIN(cc.prec, 6) : 6;
	if (cc.mods.l)
	{
		min = (cc.type == 's' && vv.s) ? (int)ft_wstrlen(vv.ws) : min;
		if (cc.prec > -1 && cc.type == 's')
			min = wmin(cc, vv);
	}
	else
	{
		min = (cc.type == 's' && vv.s) ? (int)ft_strlen(vv.s) : min;
		min = (cc.type == 's' && cc.prec > -1) ? MIN(cc.prec, min) : min;
	}
	return (min);
}

int			put_cs_str(t_conv cc, t_vars vv)
{
	int		out;
	int		min;

	min = calc_min(cc, vv);
	out = cc.out;
	if (!cc.flags.neg && cc.width != -1)
		while (cc.width-- > min)
			ft_memmove(*cc.s + cc.out++, (cc.flags.zero) ? "0" : " ", 1);
	if (cc.prec > -1 && cc.type == 's' && !cc.mods.l && (cc.out += min))
		ft_memmove(*cc.s + cc.out - min, (vv.s) ? vv.s : "(null)", min);
	else if (cc.prec > -1 && cc.type == 's')
		cc.out += ft_putwstrnstr(cc, vv.ws, min);
	else if (cc.type == 's' && !cc.mods.l)
		cc.out += ft_putstrnstr(cc, vv.s);
	else if (cc.type == 's')
		cc.out += ft_putwstrnstr(cc, vv.ws, min);
	else if (cc.type == 'c' && cc.mods.l)
		cc.out += ft_putwstrnstr(cc, &vv.wc, ft_utf16to8(vv.wc, 0));
	else if (cc.type == 'c')
		(*cc.s)[cc.out++] = vv.c;
	if (cc.flags.neg)
		while (cc.width-- > min)
			(*cc.s)[cc.out++] = ' ';
	return (cc.out - out);
}

int			put_cs(t_conv cc, t_vars vv)
{
	int		out;
	int		min;

	if (cc.fd == -42)
		return (put_cs_str(cc, vv));
	min = calc_min(cc, vv);
	out = 0;
	if (!cc.flags.neg && cc.width != -1)
		while (cc.width-- > min)
			out += write(cc.fd, (cc.flags.zero) ? "0" : " ", 1);
	if (cc.prec > -1 && cc.type == 's' && !cc.mods.l)
		out += write(cc.fd, (vv.s) ? vv.s : "(null)", min);
	else if (cc.prec > -1 && cc.type == 's')
		out += ft_putwstrn(cc.fd, vv.ws, min);
	else if (cc.type == 's' && !cc.mods.l)
		out += ft_putstrn(cc.fd, vv.s);
	else if (cc.type == 's')
		out += ft_putwstrn(cc.fd, vv.ws, min);
	else if (cc.type == 'c' && cc.mods.l)
		out += ft_putwstrn(cc.fd, &vv.wc, ft_utf16to8(vv.wc, 0));
	else if (cc.type == 'c')
		out += (int)write(cc.fd, &(vv.c), 1);
	while (cc.flags.neg && cc.width-- > min)
		out += write(cc.fd, " ", 1);
	return (out);
}
