/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lltoa_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 11:21:02 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/06 13:50:42 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_lltoa_base(long long value, long long base, int up)
{
	char			*s;
	long long		len;
	long long		log;
	long long		sgn;

	sgn = (value < 0) ? -1 : 1;
	if (base < 2 || base > 16)
		return (NULL);
	len = 1;
	log = base;
	while ((value / log) * sgn >= base && ++len)
		log *= base;
	len += (value >= base || value <= -base) ? 1 : 0;
	len += (sgn == -1 && base == 10) ? 1 : 0;
	if (!(s = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	s[len--] = 0;
	while (len >= 0)
	{
		log = sgn * (value - base * (value / base));
		s[len--] = (log < 10) ? log + '0' : log + 'a' + up * ('A' - 'a') - 10;
		value /= base;
	}
	s[0] = (sgn == -1 && base == 10) ? '-' : s[0];
	return (s);
}
