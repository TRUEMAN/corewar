/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xmalloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/29 17:34:56 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:18 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

void	*ft_xmalloc(size_t size)
{
	void	*mem;

	if (!(mem = malloc(size)))
	{
		ft_putendl_fd("Error: malloc() failed!", 2);
		exit(0);
	}
	return (mem);
}
