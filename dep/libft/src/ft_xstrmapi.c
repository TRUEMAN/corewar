/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xstrmapi.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:21:00 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:53 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xstrmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	size;
	char			*str;

	if (s == NULL)
		return (NULL);
	size = ft_strlen(s);
	str = ft_xstrnew(size);
	while (size--)
		str[size] = f(size, s[size]);
	return (str);
}
