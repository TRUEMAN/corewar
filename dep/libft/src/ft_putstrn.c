/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstrn.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:36:00 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 18:51:02 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_printf.h"

int		ft_putstrnstr(t_conv c, char *s)
{
	if (s == NULL)
	{
		ft_memmove(*c.s + c.out, "(null)", 6);
		return (6);
	}
	ft_memmove(*c.s + c.out, s, ft_strlen(s));
	return (ft_strlen(s));
}

int		ft_putstrn(int fd, char *s)
{
	int		i;

	if (s == NULL)
		i = write(fd, "(null)", 6);
	else
		i = write(fd, s, ft_strlen(s));
	return (i);
}
