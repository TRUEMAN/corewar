/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 13:50:12 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/16 13:25:30 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

void	*ft_memdup(const void *src, size_t n)
{
	char	*destination;
	char	*source;

	if (!(destination = malloc(sizeof(*destination) * n)))
		return (NULL);
	source = (char *)src;
	while (n--)
		destination[n] = source[n];
	return (destination);
}
