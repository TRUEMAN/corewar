/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base_fd.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 17:50:48 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/03 17:50:50 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	ft_putnbr_base_fd(int n, int base, int fd)
{
	long	nb;

	if (base > 36 || base < 2 || (n < 0 && base != 10))
		return ;
	nb = (long)n;
	if (nb < 0)
	{
		ft_putchar_fd('-', fd);
		nb = -nb;
	}
	if (nb >= base)
		ft_putnbr_base_fd(nb / base, base, fd);
	ft_putchar_fd(nb % base + (nb % base >= 10 ? 'A' - 10 : '0'), fd);
}
