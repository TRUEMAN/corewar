/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 10:57:58 by jandre-j          #+#    #+#             */
/*   Updated: 2015/11/30 12:10:09 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strrev(char *str)
{
	char	buffer;
	size_t	start;
	size_t	end;

	start = 0;
	end = ft_strlen(str);
	while (start < end)
	{
		buffer = str[start];
		str[start++] = str[end - 1];
		str[end - 1] = buffer;
		end--;
	}
	return (str);
}
