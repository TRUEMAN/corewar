/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xstrmap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:02:35 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:47 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xstrmap(char const *s, char (*f)(char))
{
	size_t	size;
	char	*str;

	if (s == NULL)
		return (NULL);
	size = ft_strlen(s);
	str = ft_xstrnew(size);
	while (size--)
		str[size] = f(s[size]);
	return (str);
}
