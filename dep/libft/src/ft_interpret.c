/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_interpret.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 10:43:28 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/28 18:53:59 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

int		deal_flags(t_conv *cc, char **f)
{
	if (!ft_strchr("#0-+' ", **f))
		return (0);
	cc->flags.alt = (**f == '#') ? 1 : cc->flags.alt;
	cc->flags.zero = (**f == '0') ? 1 : cc->flags.zero;
	cc->flags.neg = (**f == '-') ? 1 : cc->flags.neg;
	cc->flags.sp = (**f == ' ') ? 1 : cc->flags.sp;
	cc->flags.pos = (**f == '+') ? 1 : cc->flags.pos;
	cc->flags.apstrph = (**f == 39) ? 1 : cc->flags.apstrph;
	cc->flags.zero = (cc->flags.neg) ? 0 : cc->flags.zero;
	cc->flags.sp = (cc->flags.pos) ? 0 : cc->flags.sp;
	return (1);
}

int		deal_mods(t_conv *cc, char **f)
{
	if (!ft_strchr("jzhl", **f))
		return (0);
	cc->mods.j = (**f == 'j') ? 1 : cc->mods.j;
	cc->mods.z = (**f == 'z') ? 1 : cc->mods.z;
	if (**f == 'h')
	{
		if (cc->mods.hh)
			cc->mods.hh = 0;
		else
			cc->mods.hh = (cc->mods.h) ? 1 : cc->mods.hh;
		cc->mods.h = 1;
	}
	if (**f == 'l')
	{
		cc->mods.ll = (cc->mods.l) ? 1 : cc->mods.ll;
		cc->mods.l = 1;
	}
	return (1);
}

int		read_star(char **f, va_list *ap, va_list *ap_back)
{
	int		i;
	int		argnb;
	va_list	ap_cpy;
	int		val;

	i = 1;
	while (ft_isdigit((*f)[i]))
		i++;
	if ((*f)[i] != '$')
		return (va_arg(*ap, int));
	++*f;
	argnb = ft_atoi(*f);
	while (ft_isdigit(**f))
		++*f;
	va_copy(ap_cpy, *ap_back);
	i = 0;
	while (i++ < argnb)
		val = va_arg(ap_cpy, int);
	return (val);
}

int		deal_else(t_conv *cc, char **f, va_list *ap, va_list *ap_back)
{
	if (**f == '.')
	{
		cc->prec = 0;
		if (!ft_strchr("0123456789*", *(*f + 1)))
			return (1);
		++*f;
		cc->prec = (**f == '*') ? read_star(f, ap, ap_back) : ft_atoi(*f);
		cc->prec = (cc->prec < 0) ? -1 : cc->prec;
		while (!ft_strchr("$*", **f) && ft_strchr("0123456789", *(*f + 1)))
			++*f;
		return (1);
	}
	if (!ft_strchr("123456789*", **f))
		return (0);
	cc->width = (**f == '*') ? read_star(f, ap, ap_back) : ft_atoi(*f);
	if (cc->width < 0)
	{
		cc->flags.neg = 1;
		cc->flags.zero = 0;
		cc->width *= -1;
	}
	while (!ft_strchr("$*", **f) && ft_strchr("0123456789", *(*f + 1)))
		++*f;
	return (1 + deal_dollar(f, ap, ap_back));
}

int		deal_dollar(char **f, va_list *ap, va_list *ap_back)
{
	int		i;
	int		j;
	void	*tmp;

	i = 0;
	while (ft_isdigit((*f)[i]))
		i++;
	if ((**f) == '0' || (*f)[i] != '$' || i == 0)
		return (0);
	i = ft_atoi((*f));
	if (i > 0)
	{
		va_copy(*ap, *ap_back);
		j = 0;
		while (++j < i)
			tmp = va_arg(*ap, void *);
	}
	while (ft_isdigit(**f))
		(*f)++;
	return (1);
}
