/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrn.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:58:56 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/29 14:26:24 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_putnbrn(int fd, int n)
{
	char	c;
	int		log;
	int		sign;
	int		count;

	count = 0;
	sign = (n < 0) ? -1 : 1;
	if (n < 0)
	{
		c = '-';
		count += write(fd, &c, 1);
	}
	log = 1;
	while (sign * (n / 10) >= log)
		log *= 10;
	while (log > 0)
	{
		c = '0' + sign * (n / log);
		count += write(fd, &c, 1);
		n = n - log * (n / log);
		log /= 10;
	}
	return (count);
}
