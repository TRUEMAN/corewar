/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xstrdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:48:36 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:32 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_xstrdup(const char *s1)
{
	char	*str;
	size_t	iter;

	str = ft_xmalloc(sizeof(char) * (ft_strlen(s1) + 1));
	iter = 0;
	while (s1[iter])
	{
		str[iter] = s1[iter];
		iter++;
	}
	str[iter] = '\0';
	return (str);
}
