/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ulltoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 11:21:02 by olaurent          #+#    #+#             */
/*   Updated: 2016/01/11 14:14:15 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_ulltoa_base(unsigned long long v, unsigned long long b, int up)
{
	char				*s;
	unsigned long long	len;
	unsigned long long	log;

	if (b < 2 || b > 16)
		return (NULL);
	len = 1;
	log = b;
	while ((v / log) >= b && ++len)
		log *= b;
	len += (v >= b) ? 1 : 0;
	if (!(s = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	s[len--] = 0;
	while (42)
	{
		log = v - b * (v / b);
		s[len] = (log < 10) ? log + '0' : log + 'a' + up * ('A' - 'a') - 10;
		if (len-- == 0)
			break ;
		v /= b;
	}
	return (s);
}
