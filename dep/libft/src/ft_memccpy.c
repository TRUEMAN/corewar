/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 15:55:42 by jandre-j          #+#    #+#             */
/*   Updated: 2015/12/09 18:00:39 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	char	*destination;
	char	*source;

	destination = (char *)dst;
	source = (char *)src;
	while (n--)
	{
		*destination++ = *source;
		if (*source++ == (char)c)
			return (destination);
	}
	return (NULL);
}
