/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xlstnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 15:37:38 by jandre-j          #+#    #+#             */
/*   Updated: 2016/05/29 17:37:07 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*ft_xlstnew(void const *content, size_t content_size)
{
	t_list	*list;

	list = (t_list *)ft_xmemalloc(sizeof(*list));
	list->next = NULL;
	if (content && content_size != 0)
	{
		list->content = ft_xmemalloc(content_size);
		list->content_size = content_size;
		ft_memmove(list->content, content, content_size);
	}
	else
	{
		list->content = NULL;
		list->content_size = 0;
	}
	return (list);
}
