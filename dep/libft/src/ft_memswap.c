/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memswap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 11:36:07 by jandre-j          #+#    #+#             */
/*   Updated: 2015/11/30 11:56:00 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	ft_memswap(void *dst, void *src, size_t size)
{
	char	buffer;
	size_t	i;

	i = 0;
	while (i < size)
	{
		buffer = ((char *)dst)[i];
		((char *)dst)[i] = ((char *)src)[i];
		((char *)src)[i++] = buffer;
	}
}
