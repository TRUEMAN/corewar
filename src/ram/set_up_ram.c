/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_up_ram.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 22:10:11 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/15 13:03:26 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ram.h"

void	*set_up_ram(int memory_size)
{
	void	*ram;

	ram = (void *)malloc(memory_size);
	if (ram == NULL)
		return (NULL);
	ft_memset(ram, 0, memory_size);
	return (ram);
}
