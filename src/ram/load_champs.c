/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_champs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 22:20:43 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:21:51 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include "ram.h"
#include <stdint.h>

void	load_champs_in_ram(struct s_vm *vm, struct s_champion **champ,
							int nb_player)
{
	int			ld_champ;
	int			mem_off_set;
	size_t		progress;
	size_t		offset;

	ld_champ = -1;
	while (++ld_champ < nb_player)
	{
		mem_off_set = ld_champ * (vm->memory_size / nb_player);
		progress = 0;
		while (progress < champ[ld_champ]->size)
		{
			offset = ((uintptr_t)mem_off_set + progress) % vm->args.memory_size;
			*(uint8_t *)((uintptr_t)vm->ram + offset) = *(uint8_t *)((uintptr_t)
					champ[ld_champ]->code + (uintptr_t)progress);
			progress++;
		}
	}
}
