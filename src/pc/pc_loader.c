/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pc_loader.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/30 01:11:08 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:19:54 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "pc.h"

/*
** create_registers:
** Create a bydimentional tab of void which will serve as registers for the
** process.
** If a register adress is given as a parameter the new register will be a copy
** of this register.
*/

static void			**create_registers(void)
{
	void	**new_reg;
	int		i;

	new_reg = (void**)malloc(sizeof(void*) * (REG_NUMBER + 1));
	if (new_reg == NULL)
		return (NULL);
	new_reg[REG_NUMBER] = NULL;
	i = -1;
	while (++i < REG_NUMBER)
	{
		new_reg[i] = (void*)malloc(REG_SIZE);
		if (new_reg[i] == NULL)
		{
			while (--i > 0)
				free(new_reg[i]);
			return (NULL);
		}
		ft_memset(new_reg[i], 0, REG_SIZE);
	}
	return (new_reg);
}

static void			set_up(void *adr_op, int pr_own, void *orgn, t_pc_elem *new)
{
	int		val;

	new->op_code = *((unsigned char*)(adr_op));
	val = new->op_code <= 16 && new->op_code >= 1 ? new->op_code - 1 : 17;
	new->process_owner = pr_own;
	new->adr_crnt_pros = adr_op;
	new->cpt_cycles = 0;
	new->instruct_cycle = g_op_tab[val].nb_cycles;
	new->nb_live_since_last_cycle = 0;
	new->nb_live = 0;
	new->carry = 0;
	new->step_in = 0;
	new->ram_orgn = orgn;
	new->last_live_cycle = 0;
}

/*
** get_elem_def:
** Create a standard elem with no particular starting value.
** Except the process owner.
*/

static t_pc_elem	*elem_def(t_champion *adr_op, int owr, void *orgn, t_vm *vm)
{
	t_pc_elem		*elem_def;
	unsigned int	champ;

	elem_def = (t_pc_elem*)malloc(sizeof(t_pc_elem));
	if (owr > vm->args.nb_player || owr < 0 || elem_def == NULL)
	{
		if (elem_def != NULL)
			free(elem_def);
		return (NULL);
	}
	set_up(adr_op, owr, orgn, elem_def);
	elem_def->reg = create_registers();
	if (elem_def->reg == NULL)
	{
		free(elem_def);
		return (NULL);
	}
	champ = vm->args.champions_id[owr];
	reverse_bytes(&champ, REG_SIZE);
	ft_memcpy(elem_def->reg[0], &champ, REG_SIZE);
	elem_def->vm = vm;
	return (elem_def);
}

static void			lock_pc(t_pc_lst **pc_orgn, int nb_player)
{
	t_pc_lst	*elem;

	elem = *pc_orgn;
	while (--nb_player > 0)
		elem = elem->next;
	elem->next = NULL;
}

/*
** pc_load_champs:
** Create the process counter then load the champions inside.
*/

t_pc_lst			*pc_load_champs(t_vm *vm, t_pc_lst **pc_orgn)
{
	t_pc_elem	*new_elem;
	t_pc_lst	*pc;
	int			pr_ownr;
	void		*ram_orgn;
	void		*champion_orgn;

	pr_ownr = -1;
	pc = NULL;
	ram_orgn = vm->ram;
	while (++pr_ownr < vm->args.nb_player)
	{
		champion_orgn = ram_orgn +
						(pr_ownr * (vm->memory_size / vm->args.nb_player));
		new_elem = elem_def(champion_orgn, pr_ownr, ram_orgn, vm);
		new_elem->proc_num = pr_ownr;
		if (add_to_pc_lst(&pc, new_elem) == -1)
		{
			clean_pc_lst(&pc);
			return (NULL);
		}
		new_elem->vm = vm;
		new_elem->pc_orgn = pc_orgn;
	}
	lock_pc(&pc, vm->args.nb_player);
	return (pc);
}
