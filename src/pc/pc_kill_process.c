/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pc_kill_process.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 20:50:40 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 14:10:59 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "pc.h"
#include "events.h"
#include "flags.h"

void		kill_process(t_pc_elem *process)
{
	int	i;

	show_death_verbosity(process);
	if (process->vm->args.enable_visualiser == 1)
		on_process_left(((t_vm *)process->vm)->visual, process->adr_crnt_pros -
				process->ram_orgn);
	i = -1;
	on_process_killed(((t_vm *)process->vm)->visual);
	while (process->reg[++i] != NULL)
		free(process->reg[i]);
	free(process->reg);
	free(process);
}

void		clean_pc_lst(t_pc_lst **pc)
{
	t_pc_lst	*strt_ptr;
	t_pc_lst	*del_ptr;

	strt_ptr = *pc;
	del_ptr = NULL;
	while (*pc != NULL)
	{
		if ((*pc)->process->nb_live_since_last_cycle == 0)
		{
			del_ptr = *pc;
			kill_process((*pc)->process);
			if ((*pc)->prev != NULL)
				(*pc)->prev->next = (*pc)->next;
			else
				strt_ptr = (*pc)->next;
			if ((*pc)->next != NULL)
				(*pc)->next->prev = (*pc)->prev;
		}
		else
			(*pc)->process->nb_live_since_last_cycle = 0;
		*pc = (*pc)->next;
		if (del_ptr != NULL)
			ft_memdel((void**)&del_ptr);
	}
	*pc = strt_ptr;
}
