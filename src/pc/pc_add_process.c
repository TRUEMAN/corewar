/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pc_add_process.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 20:27:14 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:19:32 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "pc.h"
#include "events.h"

static t_pc_elem	*create_process(const t_pc_elem *process)
{
	t_pc_elem	*new_process;
	int			i;

	new_process = (t_pc_elem*)malloc(sizeof(t_pc_elem));
	if (new_process == NULL)
		return (NULL);
	ft_memcpy(new_process, process, sizeof(t_pc_elem));
	new_process->reg = (void**)malloc(sizeof(void*) * (REG_NUMBER + 1));
	if (new_process->reg == NULL)
		return (NULL);
	new_process->reg[REG_NUMBER] = NULL;
	i = -1;
	new_process->proc_num = (*(process->pc_orgn))->process->proc_num + 1;
	while (++i < REG_NUMBER)
		if ((new_process->reg[i] = (void*)malloc(REG_SIZE)) == NULL)
		{
			while (--i >= 0)
				free(new_process->reg[i]);
			free(new_process->reg);
			free(new_process);
			return (NULL);
		}
		else
			ft_memcpy(new_process->reg[i], process->reg[i], REG_SIZE);
	return (new_process);
}

int					add_to_pc_lst(t_pc_lst **pc, t_pc_elem *process)
{
	if (*pc == NULL)
	{
		*pc = (t_pc_lst*)malloc(sizeof(t_pc_lst));
		if (*pc == NULL)
			return (-1);
		(*pc)->prev = NULL;
		(*pc)->next = NULL;
		(*pc)->process = process;
	}
	else
	{
		(*pc)->prev = (t_pc_lst*)malloc(sizeof(t_pc_lst));
		if ((*pc)->prev == NULL)
			return (-1);
		(*pc)->prev->next = *pc;
		*pc = (*pc)->prev;
		(*pc)->prev = NULL;
		(*pc)->process = process;
	}
	return (0);
}

int					pc_add_process(t_pc_lst **pc, t_pc_elem *process)
{
	t_pc_elem	*new_process;

	new_process = create_process(process);
	if (new_process == NULL)
		return (-1);
	if (add_to_pc_lst(pc, new_process) == -1)
	{
		kill_process(new_process);
		return (-1);
	}
	on_process_forked(((t_vm *)process->vm)->visual);
	if (new_process->vm->args.enable_visualiser == 1)
		on_process_arrived(((t_vm *)process->vm)->visual,
		process->adr_crnt_pros - process->ram_orgn);
	return (0);
}
