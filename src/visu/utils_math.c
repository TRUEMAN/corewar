/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_math.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:37 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/25 15:59:10 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_math.h>

int		int_clamp(int value, int min, int max)
{
	if (value < min)
		return (min);
	if (value > max)
		return (max);
	return (value);
}

int		int_min(int a, int b)
{
	return (a < b ? a : b);
}

int		int_max(int a, int b)
{
	return (a < b ? b : a);
}

float	float_clamp(float value, float min, float max)
{
	if (value < min)
		return (min);
	if (value > max)
		return (max);
	return (value);
}

float	float_serp(float start, float end, float value)
{
	float	result;

	result = (1 - ft_cos(value * PI)) / 2;
	return (start * (1 - result) + end * result);
}
