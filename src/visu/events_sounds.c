/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events_sounds.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:09:34 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 15:40:00 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "events.h"

void	on_process_jumped(t_visual *visual)
{
	if (visual && visual->sys)
		visual->sounds[JUMP_SOUND] = 1;
}

void	on_process_forked(t_visual *visual)
{
	if (visual && visual->sys)
		visual->sounds[FORK_SOUND] = 1;
}

void	on_process_killed(t_visual *visual)
{
	if (visual && visual->sys)
		visual->sounds[KILL_SOUND] = 1;
}
