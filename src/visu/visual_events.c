/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual_events.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:42 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/24 15:44:51 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"

static inline void	visual_play_sounds(t_visual *obj)
{
	size_t	i;

	i = sizeof(obj->sounds) / sizeof(*obj->sounds);
	while (i--)
		if (obj->sounds[i])
		{
			sound_system_play(obj->sys, i);
			obj->sounds[i] = 0;
		}
}

void				visual_on_resize(t_visual *obj, int width, int height)
{
	obj->width = width;
	obj->height = height;
	champ_list_update(obj->champ_list);
	if (obj->console)
		console_update(obj->console);
	memview_update(obj->memview);
}

void				visual_draw(t_visual *obj)
{
	set_sdl_color(obj->ren, &obj->colors[0]);
	SDL_RenderClear(obj->ren);
	champ_list_draw(obj->champ_list);
	if (obj->console)
		console_draw(obj->console);
	memview_draw(obj->memview);
	SDL_RenderPresent(obj->ren);
}

static inline void	visual_scroll(t_visual *obj)
{
	float	diff;

	diff = (float)obj->memview->scroll - obj->memview->scroll_fp;
	if (diff > -0.1 && diff < 0.1)
		obj->memview->scroll_fp = (float)obj->memview->scroll;
	else
		obj->memview->scroll_fp += diff / 24;
	diff = (float)obj->champ_list->scroll - obj->champ_list->scroll_fp;
	if (diff > -0.1 && diff < 0.1)
		obj->champ_list->scroll_fp = (float)obj->champ_list->scroll;
	else
		obj->champ_list->scroll_fp += diff / 24;
	if (obj->console)
	{
		diff = (float)obj->console->scroll - obj->console->scroll_fp;
		if (diff > -0.1 && diff < 0.1)
			obj->console->scroll_fp = (float)obj->console->scroll;
		else
			obj->console->scroll_fp += diff / 24;
	}
}

void				visual_on_tick(t_visual *obj)
{
	visual_scroll(obj);
	obj->champ_list->name_scroll_pos += obj->champ_list->name_scroll_factor;
	if (obj->champ_list->name_scroll_pos <= 0 ||
			obj->champ_list->name_scroll_pos >= 100)
		obj->champ_list->count_down--;
	else
		obj->champ_list->count_down = 512;
	if (obj->champ_list->name_scroll_pos >= 100 &&
			obj->champ_list->count_down <= 0)
		obj->champ_list->name_scroll_factor = -0.2;
	else if (obj->champ_list->name_scroll_pos <= 0 &&
			obj->champ_list->count_down <= 0)
		obj->champ_list->name_scroll_factor = 0.1;
	obj->champ_list->name_scroll_pos =
			float_clamp(obj->champ_list->name_scroll_pos, 0.0, 100.0);
	obj->champ_list->name_scroll = float_serp(0, 100,
			obj->champ_list->name_scroll_pos / 100);
	if (obj->sys)
		visual_play_sounds(obj);
}
