/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:34 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 11:35:19 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

char	get_hex_representation(uint8_t value)
{
	if (value < 10)
		return ('0' + value);
	return ('a' + value - 10);
}

void	byte_to_string(uint8_t value, char buffer[3])
{
	buffer[0] = get_hex_representation(value / 0x10);
	buffer[1] = get_hex_representation(value % 0x10);
	buffer[2] = '\0';
}

size_t	utf8_len(char *str)
{
	size_t	i;

	i = 0;
	while (*str)
	{
		if ((*str & 0xc0) != 0x80)
			i++;
		str++;
	}
	return (i);
}
