/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   console.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:15 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/21 16:25:37 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"
#include "error.h"
#include "corewar.h"
#include <libft.h>

static char	*get_champ_nick(t_vm *vm, size_t index)
{
	char	*res;
	char	*tmp;
	size_t	len;

	res = ft_strdup(vm->champions[index]->header->prog_name);
	while ((len = utf8_len(res)) > 12)
		res[ft_strlen(res) - 1] = '\0';
	tmp = ft_strsub(res, 0,
			ft_strlen(vm->champions[index]->header->prog_name));
	free(res);
	while (utf8_len(tmp) < 12)
	{
		res = ft_strjoin(tmp, " ");
		free(tmp);
		tmp = res;
	}
	res = ft_strjoin(tmp, " :");
	free(tmp);
	return (res);
}

static void	console_new_set_vars(t_console *obj)
{
	size_t	i;
	char	*tmp;

	i = obj->visual->mem->champ_count;
	obj->scroll = 0;
	obj->scroll_fp = 0.0;
	obj->textures = ft_xmemalloc(sizeof(*obj->textures) * i * 2);
	obj->sizes = ft_xmemalloc(sizeof(*obj->sizes) * i * 2);
	obj->strings = ft_xmemalloc(sizeof(*obj->strings) * i);
	obj->string_len = ft_xmemalloc(sizeof(*obj->string_len) * i * 2);
	while (i--)
	{
		tmp = get_champ_nick(obj->visual->vm, i);
		obj->textures[i * 2] = sdl_render_utf8(obj->ren, obj->font, tmp,
			(void *[2]){ &obj->sizes[i * 2], &obj->colors[4 + i % 8] });
		if (obj->sizes[i * 2].h > (int)obj->line_height)
			obj->line_height = obj->sizes[i * 2].h;
		if (obj->sizes[i * 2].w > (int)obj->name_len)
			obj->name_len = obj->sizes[i * 2].w;
		free(tmp);
	}
}

t_console	*console_new(t_visual *visual)
{
	t_console	*obj;
	size_t		i;

	obj = ft_xmemalloc(sizeof(*obj));
	console_set_colors(obj);
	obj->visual = visual;
	obj->ren = visual->ren;
	obj->font_size = 12;
	obj->font_path = ft_xpathadd(obj->visual->base_path, "/res/noto-mono.ttf");
	if (!(obj->font = TTF_OpenFont(obj->font_path, obj->font_size)))
		err_abort(TTF_GetError(), 1);
	console_new_set_vars(obj);
	i = obj->visual->mem->champ_count * 2;
	while (i--)
		obj->sizes[i].y = obj->line_height * (i / 2);
	obj->char_width = obj->name_len / 14;
	return (obj);
}

void		console_delete(t_console *obj)
{
	size_t	i;

	i = obj->visual->mem->champ_count;
	while (i--)
	{
		if (obj->textures && obj->textures[i * 2])
			SDL_DestroyTexture(obj->textures[i * 2]);
		if (obj->textures && obj->textures[i * 2 + 1])
			SDL_DestroyTexture(obj->textures[i * 2 + 1]);
		if (obj->strings && obj->strings[i])
			free(obj->strings[i]);
	}
	if (obj->textures)
		free(obj->textures);
	if (obj->strings)
		free(obj->strings);
	if (obj->string_len)
		free(obj->string_len);
	if (obj->sizes)
		free(obj->sizes);
	if (obj->font_path)
		free(obj->font_path);
	SDL_DestroyTexture(obj->texture);
	TTF_CloseFont(obj->font);
	free(obj);
}
