/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memview_update.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:48:31 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 10:48:34 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"
#include "error.h"
#include <libft.h>

inline static void	memview_update_byte_text(t_memview *obj,
							SDL_Rect rect, size_t index, size_t colors[2])
{
	rect.x += obj->byte_full_size.x;
	rect.y += obj->byte_full_size.y;
	rect.w = obj->bytes[obj->mem->mem[index] / 16]->size.w;
	rect.h = obj->bytes[obj->mem->mem[index] / 16]->size.h;
	SDL_RenderCopy(obj->ren, obj->bytes[obj->mem->mem[index] / 16 +
			16 * colors[1]]->texture, NULL, &rect);
	rect.x += obj->byte_size.w;
	rect.w = obj->bytes[obj->mem->mem[index] % 16]->size.w;
	rect.h = obj->bytes[obj->mem->mem[index] % 16]->size.h;
	SDL_RenderCopy(obj->ren, obj->bytes[obj->mem->mem[index] % 16 +
			16 * colors[1]]->texture, NULL, &rect);
}

inline static void	memview_update_byte_texture(t_memview *obj, SDL_Rect *rect,
							size_t index, size_t colors[2])
{
	size_t	tmp;

	if (obj->stealth)
	{
		tmp = obj->mem->ids[index] ? obj->mem->ids[index] + 1 : 0;
		while (tmp > 9)
			tmp -= 8;
		set_sdl_color(obj->ren, &obj->colors[tmp]);
		SDL_RenderFillRect(obj->ren, rect);
		return ;
	}
	set_sdl_color(obj->ren, &obj->colors[colors[0]]);
	SDL_RenderFillRect(obj->ren, rect);
	if (obj->mem->live[index])
	{
		set_sdl_color(obj->ren, &obj->colors[colors[1]]);
		SDL_RenderDrawRect(obj->ren, rect);
	}
	memview_update_byte_text(obj, *rect, index, colors);
	if (obj->mem->changed[index])
	{
		rect->x--;
		rect->y--;
		memview_update_byte_text(obj, *rect, index, colors);
	}
}

void				memview_update_byte(t_memview *obj, size_t index)
{
	size_t		x;
	size_t		y;
	SDL_Rect	rect;
	size_t		colors[2];

	colors[0] = 0;
	colors[1] = 0;
	if (obj->mem->pcs[index] != 0)
		colors[0] = obj->mem->ids[index] + 1;
	else
		colors[1] = obj->mem->ids[index] + 1;
	while (colors[0] > 9)
		colors[0] -= 8;
	while (colors[1] > 9)
		colors[1] -= 8;
	x = index % obj->line_size;
	y = index / obj->line_size;
	rect.x = obj->line_offset + obj->byte_full_size.w * x;
	rect.y = 0;
	rect.w = obj->byte_full_size.w;
	rect.h = obj->byte_full_size.h;
	SDL_SetRenderTarget(obj->ren, obj->lines[y]);
	memview_update_byte_texture(obj, &rect, index, colors);
	SDL_SetRenderTarget(obj->ren, NULL);
}

void				memview_update_lines(t_memview *obj)
{
	size_t	i;

	obj->texture = create_sdl_texture(obj->ren, obj->size.w, obj->size.h);
	obj->lines = ft_xmemalloc(sizeof(*(obj->lines)) * obj->line_count);
	i = obj->line_count;
	while (i--)
	{
		obj->lines[i] = create_sdl_texture(obj->ren, obj->size.w,
				obj->line_height);
		SDL_SetRenderTarget(obj->ren, obj->lines[i]);
		set_sdl_color(obj->ren, &obj->colors[0]);
		SDL_RenderClear(obj->ren);
		SDL_SetRenderTarget(obj->ren, NULL);
	}
	i = obj->mem->size;
	while (i--)
		memview_update_byte(obj, i);
}

void				memview_update(t_memview *obj)
{
	memview_delete_lines(obj);
	obj->size.x = 16;
	obj->size.y = 16;
	obj->size.w = obj->visual->champ_list->size_top.x - 2 * obj->size.x;
	obj->size.h = ((obj->visual->console) ? obj->visual->console->size.y :
			obj->visual->height) - obj->size.y * 2;
	obj->line_size = obj->line_max > 0 ?
			int_min(obj->size.w / obj->byte_full_size.w, obj->line_max) :
			obj->size.w / obj->byte_full_size.w;
	if (obj->line_size == 0)
		err_abort("Window too small", 1);
	obj->line_count = (obj->mem->size - 1) / obj->line_size + 1;
	obj->line_height = obj->byte_full_size.h;
	obj->line_offset = (obj->size.w - obj->line_size * obj->byte_full_size.w) /
			2;
	obj->scroll_max = obj->line_height * obj->line_count;
	if (obj->scroll_max < obj->size.h)
		obj->scroll_max = 0;
	else
		obj->scroll_max -= obj->size.h;
	if (obj->scroll_fp > obj->scroll_max)
		obj->scroll_fp = (float)obj->scroll_max;
	memview_update_lines(obj);
}
