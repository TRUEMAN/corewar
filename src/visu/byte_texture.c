/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   byte_texture.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:48:46 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 10:48:47 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "error.h"
#include "utils.h"
#include <libft.h>

t_byte_texture	*byte_texture_new(t_memview *memview, uint8_t value,
		SDL_Color color)
{
	t_byte_texture	*obj;
	SDL_Surface		*surface;
	char			buffer[2];

	buffer[0] = get_hex_representation(value);
	buffer[1] = '\0';
	obj = ft_xmemalloc(sizeof(*obj));
	TTF_SizeText(memview->font, buffer, &(obj->size.w), &(obj->size.h));
	if (!(surface = TTF_RenderText_Blended(memview->font, buffer, color)))
		return ((t_byte_texture *)err_abort(TTF_GetError(), 1));
	if (!(obj->texture = SDL_CreateTextureFromSurface(memview->ren, surface)))
		return ((t_byte_texture *)err_abort(SDL_GetError(), 1));
	SDL_FreeSurface(surface);
	return (obj);
}

void			byte_texture_delete(t_byte_texture *obj)
{
	SDL_DestroyTexture(obj->texture);
	free(obj);
}
