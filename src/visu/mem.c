/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mem.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:25 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 10:49:25 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "error.h"
#include <libft.h>

t_mem	*mem_new(size_t size, size_t champ_count)
{
	t_mem	*obj;

	if (size == 0)
		return ((t_mem *)err_abort("Invalid memory size", 1));
	obj = ft_xmalloc(sizeof(*obj));
	obj->size = size;
	obj->champ_count = champ_count;
	obj->process_count = 0;
	obj->mem = ft_xmemalloc(sizeof(*(obj->mem)) * size);
	obj->ids = ft_xmemalloc(sizeof(*(obj->ids)) * size);
	obj->pcs = ft_xmemalloc(sizeof(*(obj->pcs)) * size);
	obj->changed = ft_xmemalloc(sizeof(*(obj->changed)) * size);
	obj->live = ft_xmemalloc(sizeof(*(obj->live)) * size);
	return (obj);
}

void	mem_delete(t_mem *obj)
{
	free(obj->mem);
	free(obj->ids);
	free(obj->pcs);
	free(obj->changed);
	free(obj->live);
	free(obj);
}
