/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   champ.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:48:51 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/25 16:42:21 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"
#include "error.h"
#include <libft.h>

t_champ	*champ_new(t_champ_list *list, char *name, int id)
{
	t_champ		*obj;
	size_t		i;
	char		*tmp;

	obj = ft_xmemalloc(sizeof(*obj));
	obj->list = list;
	obj->name = ft_xstrdup(*name == '\0' ? "[unnamed]" : name);
	obj->id = id;
	i = 0;
	while (obj->name[i])
		if (ft_isspace(obj->name[i++]))
			obj->name[i - 1] = ' ';
	tmp = ft_xitoa(obj->id);
	obj->textures[0] = sdl_render_text(obj->list->ren, obj->list->font2, tmp,
		(void *[2]){ &obj->sizes[0], &obj->list->colors[1] });
	obj->textures[1] = sdl_render_utf8(obj->list->ren, obj->list->font2,
		obj->name, (void *[2]){ &obj->sizes[1], &obj->list->colors[1] });
	obj->textures[2] = sdl_render_text(obj->list->ren, obj->list->font2, "0",
		(void *[2]){ &obj->sizes[2], &obj->list->colors[1] });
	obj->textures[3] = sdl_render_text(obj->list->ren, obj->list->font2, "0",
		(void *[2]){ &obj->sizes[3], &obj->list->colors[1] });
	free(tmp);
	return (obj);
}

void	champ_delete(t_champ *obj)
{
	size_t	i;

	if (obj->name)
		free(obj->name);
	i = sizeof(obj->sizes) / sizeof(*obj->sizes);
	while (i--)
		if (obj->textures[i])
			SDL_DestroyTexture(obj->textures[i]);
	if (obj->texture)
		SDL_DestroyTexture(obj->texture);
	free(obj);
}

void	champ_update(t_champ *obj)
{
	size_t	i;

	obj->size.x = 12;
	obj->size.w = obj->list->size_list.w - 24;
	obj->size.h = 98;
	obj->sizes[0].x = obj->list->sizes[8].x + obj->list->sizes[8].w;
	obj->sizes[1].x = obj->list->sizes[10].x + obj->list->sizes[10].w;
	obj->sizes[1].y = 24;
	obj->sizes[2].x = obj->list->sizes[11].x + obj->list->sizes[11].w;
	obj->sizes[2].y = 48;
	obj->sizes[3].x = obj->list->sizes[12].x + obj->list->sizes[12].w;
	obj->sizes[3].y = 72;
	i = sizeof(obj->sizes) / sizeof(*obj->sizes);
	while (i--)
	{
		obj->srcs[i].w = obj->sizes[i].w;
		obj->srcs[i].h = obj->sizes[i].h;
		obj->dsts[i] = obj->sizes[i];
	}
	if (obj->texture)
		SDL_DestroyTexture(obj->texture);
	obj->texture = create_sdl_texture(obj->list->ren, obj->size.w,
			obj->size.h);
}

void	champ_update_lives(t_champ *obj)
{
	char	*tmp;

	if (obj->prev_lives == obj->lives)
		return ;
	obj->prev_lives = obj->lives;
	SDL_DestroyTexture(obj->textures[2]);
	SDL_DestroyTexture(obj->textures[3]);
	tmp = ft_xitoa(obj->prev_lives);
	obj->textures[2] = sdl_render_text(obj->list->ren, obj->list->font2, tmp,
		(void *[2]){ &obj->sizes[2], &obj->list->colors[1] });
	free(tmp);
	tmp = ft_xitoa(obj->total_lives);
	obj->textures[3] = sdl_render_text(obj->list->ren, obj->list->font2, tmp,
		(void *[2]){ &obj->sizes[3], &obj->list->colors[1] });
	free(tmp);
	obj->srcs[2].w = obj->sizes[2].w;
	obj->srcs[2].h = obj->sizes[2].h;
	obj->dsts[2] = obj->sizes[2];
	obj->srcs[3].w = obj->sizes[3].w;
	obj->srcs[3].h = obj->sizes[3].h;
	obj->dsts[3] = obj->sizes[3];
}
