/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:20 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/25 17:13:10 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "events.h"
#include "utils.h"
#include "error.h"

void	on_set_speed(t_visual *visual, int modifier)
{
	if (visual->vm->cycle_per_sec == 1000 && modifier > 0)
		visual->vm->cycle_per_sec = 0;
	else if (visual->vm->cycle_per_sec == 0)
		visual->vm->cycle_per_sec = modifier < 0 ? 1000 : 0;
	else
		visual->vm->cycle_per_sec = int_clamp(
				visual->vm->cycle_per_sec + modifier, 1, 1000);
	if (visual->vm->cycle_per_sec)
		visual->vm->interval = 1000 / visual->vm->cycle_per_sec;
	champ_list_update_speed(visual->champ_list);
}

void	on_next_cycle(t_visual *visual)
{
	size_t	i;

	visual->cycle++;
	i = visual->mem->size;
	while (i--)
	{
		if (visual->mem->changed[i] &&
				visual->cycle - visual->mem->changed[i] >= 200)
		{
			visual->mem->changed[i] = 0;
			memview_update_byte(visual->memview, i);
		}
		if (visual->mem->live[i] &&
				visual->cycle - visual->mem->live[i] >= 200)
		{
			visual->mem->live[i] = 0;
			memview_update_byte(visual->memview, i);
		}
	}
	champ_list_update_cycle(visual->champ_list);
}

void	on_cycle_delta_changed(t_visual *visual, size_t delta)
{
	size_t	i;

	visual->cycle_delta = delta;
	i = visual->mem->champ_count;
	while (i--)
		visual->champ_list->champs[i]->lives = 0;
	champ_list_update_cycle(visual->champ_list);
}
