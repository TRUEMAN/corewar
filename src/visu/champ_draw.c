/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   champ_draw.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 11:26:42 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 12:21:23 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"

static inline void	draw_textures(t_champ *obj)
{
	size_t	i;

	set_sdl_color(obj->list->ren, &obj->list->colors[0]);
	SDL_RenderClear(obj->list->ren);
	set_sdl_color(obj->list->ren, &obj->list->colors[(obj->id % 8) + 4]);
	SDL_RenderDrawLine(obj->list->ren, 0, 0, 0, obj->size.h);
	i = sizeof(obj->sizes) / sizeof(*obj->sizes);
	while (i--)
		SDL_RenderCopy(obj->list->ren, obj->textures[i], &obj->srcs[i],
				&obj->dsts[i]);
	SDL_RenderCopy(obj->list->ren, obj->list->textures[8], NULL,
			&obj->list->sizes[8]);
	obj->list->sizes[9].x = obj->sizes[0].x + obj->sizes[0].w;
	SDL_RenderCopy(obj->list->ren, obj->list->textures[9], NULL,
			&obj->list->sizes[9]);
	obj->list->sizes[10].y = 24;
	SDL_RenderCopy(obj->list->ren, obj->list->textures[10], NULL,
			&obj->list->sizes[10]);
	obj->list->sizes[11].y = 48;
	SDL_RenderCopy(obj->list->ren, obj->list->textures[11], NULL,
			&obj->list->sizes[11]);
	obj->list->sizes[12].y = 72;
	SDL_RenderCopy(obj->list->ren, obj->list->textures[12], NULL,
			&obj->list->sizes[12]);
}

void				champ_draw(t_champ *obj, int y)
{
	int		size;

	size = obj->size.w - obj->sizes[1].x;
	if (size >= obj->sizes[1].w)
	{
		size = obj->sizes[1].w;
		obj->srcs[1].x = 0;
		obj->dsts[1].w = obj->sizes[1].w;
	}
	else
	{
		obj->dsts[1].w = size;
		obj->srcs[1].w = size;
		obj->srcs[1].x = (obj->sizes[1].w - obj->srcs[1].w) *
				obj->list->name_scroll / 100;
	}
	obj->size.y = y;
	SDL_SetRenderTarget(obj->list->ren, obj->texture);
	draw_textures(obj);
	SDL_SetRenderTarget(obj->list->ren, obj->list->texture_list);
	SDL_RenderCopy(obj->list->ren, obj->texture, NULL, &obj->size);
}
