/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual_loop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:45 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 13:33:12 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "error.h"
#include "events.h"
#include "utils.h"

static inline int	visual_event_keydown(t_visual *obj, SDL_Event *event)
{
	if (event->key.keysym.sym == SDLK_SPACE)
		obj->vm->paused = !obj->vm->paused;
	else if (event->key.keysym.sym == SDLK_s)
	{
		obj->vm->step = 1;
		obj->vm->paused = 1;
	}
	else if (event->key.keysym.sym == SDLK_n)
	{
		obj->vm->paused = 0;
		obj->vm->pause_at_next_operation = 1;
	}
	else if (event->key.keysym.sym == SDLK_q)
		on_set_speed(obj, -10);
	else if (event->key.keysym.sym == SDLK_w)
		on_set_speed(obj, -1);
	else if (event->key.keysym.sym == SDLK_r)
		on_set_speed(obj, 10);
	else if (event->key.keysym.sym == SDLK_e)
		on_set_speed(obj, 1);
	else
		return (0);
	return (1);
}

int					visual_event_loop(t_visual *obj)
{
	SDL_Event	event;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN &&
				event.key.keysym.sym == SDLK_ESCAPE))
			return (0);
		else if (event.type == SDL_WINDOWEVENT)
		{
			if (event.window.event == SDL_WINDOWEVENT_RESIZED)
				visual_on_resize(obj, event.window.data1, event.window.data2);
		}
		else if (event.type == SDL_KEYDOWN && visual_event_keydown(obj, &event))
			continue ;
		else if (event.type == SDL_KEYDOWN || event.type == SDL_MOUSEWHEEL)
		{
			SDL_GetMouseState(&obj->cur_x, &obj->cur_y);
			memview_on_event(obj->memview, &event, obj->cur_x, obj->cur_y);
			champ_list_on_event(obj->champ_list, &event, obj->cur_x,
					obj->cur_y);
			if (obj->console)
				console_on_event(obj->console, &event, obj->cur_x, obj->cur_y);
		}
	}
	return (1);
}

int					visual_loop(t_visual *obj)
{
	obj->current_tick = SDL_GetTicks();
	while (obj->current_tick - obj->last_tick > obj->tick_rate)
	{
		obj->last_tick += obj->tick_rate;
		visual_on_tick(obj);
	}
	if (!visual_event_loop(obj))
		return (0);
	obj->memview->scroll = int_clamp(obj->memview->scroll, 0,
			obj->memview->scroll_max);
	obj->champ_list->scroll = int_clamp(obj->champ_list->scroll, 0,
			obj->champ_list->scroll_max);
	if (obj->console)
		obj->console->scroll = int_clamp(obj->console->scroll, 0,
				obj->console->scroll_max);
	visual_draw(obj);
	return (1);
}
