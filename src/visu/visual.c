/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:39 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/24 15:08:22 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "error.h"
#include "events.h"
#include "corewar.h"
#include <libft.h>

static inline void	visual_new_set_vars(t_visual *obj, t_vm *vm)
{
	obj->display_writes = vm->args.write;
	obj->display_lives = vm->args.live;
	if (vm->args.aff)
		obj->console = console_new(obj);
	if (vm->args.audio)
		obj->sys = sound_system_new();
	else
		obj->sys = NULL;
}

t_visual			*visual_new(int width, int height, t_mem *mem, t_vm *vm)
{
	t_visual	*obj;

	obj = ft_xmemalloc(sizeof(*obj));
	obj->width = width < 1024 ? 1024 : width;
	obj->height = width < 1024 ? 1024 : height;
	if (!(obj->win = SDL_CreateWindow("Corewar", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, obj->width, obj->height,
			SDL_WINDOW_RESIZABLE)) || !(obj->ren = SDL_CreateRenderer(obj->win,
			-1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE)) ||
			!(obj->base_path = SDL_GetBasePath()))
		return ((t_visual *)err_abort(SDL_GetError(), 1));
	SDL_SetWindowMinimumSize(obj->win, 640, 480);
	SDL_SetWindowMaximumSize(obj->win, 6400, 4800);
	obj->vm = vm;
	obj->tick_rate = 5;
	obj->mem = mem;
	obj->champ_list = champ_list_new(obj, vm);
	obj->memview = memview_new(obj, vm->args.font_size);
	visual_new_set_vars(obj, vm);
	visual_set_colors(obj);
	visual_on_resize(obj, width, height);
	return (obj);
}

void				visual_delete(t_visual *obj)
{
	if (obj->base_path)
		free(obj->base_path);
	if (obj->console)
		console_delete(obj->console);
	if (obj->champ_list)
		champ_list_delete(obj->champ_list);
	if (obj->memview)
		memview_delete(obj->memview);
	if (obj->mem)
		mem_delete(obj->mem);
	if (obj->ren)
		SDL_DestroyRenderer(obj->ren);
	if (obj->win)
		SDL_DestroyWindow(obj->win);
	if (obj->sys)
		sound_system_delete(obj->sys);
	free(obj);
}

t_visual			*initialize_visu(t_vm *vm)
{
	t_visual	*visual;
	t_mem		*mem;
	size_t		i;
	size_t		j;

	if (!SDL_WasInit(SDL_INIT_EVERYTHING) && SDL_Init(SDL_INIT_EVERYTHING))
		return ((t_visual *)err_abort(SDL_GetError(), 1));
	if (TTF_WasInit() == 0 && TTF_Init() != 0)
		return ((t_visual *)err_abort(TTF_GetError(), 1));
	mem = mem_new(vm->memory_size, vm->args.nb_player);
	i = vm->memory_size;
	while (i--)
		mem->mem[i] = *(uint8_t *)(vm->ram + i);
	while (++i < mem->champ_count)
	{
		j = vm->memory_size / mem->champ_count * i +
				vm->champions[i]->size;
		while (j-- > vm->memory_size / mem->champ_count * i)
			mem->ids[j] = (i) + 1;
	}
	visual = visual_new(1024, 768, mem, vm);
	while (i--)
		on_process_arrived(visual, vm->memory_size / mem->champ_count * i);
	champ_list_update_cycle(visual->champ_list);
	return (visual);
}

void				terminate_visu(t_visual *visual)
{
	visual_delete(visual);
	if (TTF_WasInit())
		TTF_Quit();
	if (SDL_WasInit(SDL_INIT_EVERYTHING))
		SDL_Quit();
}
