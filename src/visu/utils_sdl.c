/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_sdl.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:34 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 11:35:03 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "error.h"

SDL_Color	get_color(uint32_t color)
{
	SDL_Color	res;

	res.r = (color & 0xFF000000) >> 24;
	res.g = (color & 0x00FF0000) >> 16;
	res.b = (color & 0x0000FF00) >> 8;
	res.a = (color & 0x000000FF);
	return (res);
}

void		set_sdl_color(SDL_Renderer *ren, SDL_Color *index)
{
	SDL_SetRenderDrawColor(ren, index->r, index->g, index->b, index->a);
}

SDL_Texture	*create_sdl_texture(SDL_Renderer *ren, int width, int height)
{
	SDL_Texture	*texture;

	if (!(texture = SDL_CreateTexture(ren,
			SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
			width, height)))
		err_abort(SDL_GetError(), 1);
	return (texture);
}

SDL_Texture	*sdl_render_text(SDL_Renderer *ren, TTF_Font *font, char *text,
						void *params[2])
{
	SDL_Surface	*surface;
	SDL_Texture	*texture;

	if (!(surface = TTF_RenderText_Blended(font, text,
			*(SDL_Color *)(params[1]))))
		err_abort(TTF_GetError(), 1);
	if (!(texture = SDL_CreateTextureFromSurface(ren, surface)))
		err_abort(SDL_GetError(), 1);
	SDL_QueryTexture(texture, NULL, NULL, &((SDL_Rect *)(params[0]))->w,
			&((SDL_Rect *)(params[0]))->h);
	SDL_FreeSurface(surface);
	return (texture);
}

SDL_Texture	*sdl_render_utf8(SDL_Renderer *ren, TTF_Font *font, char *text,
						void *params[2])
{
	SDL_Surface	*surface;
	SDL_Texture	*texture;

	if (!(surface = TTF_RenderUTF8_Blended(font, text,
			*(SDL_Color *)(params[1]))))
		err_abort(TTF_GetError(), 1);
	if (!(texture = SDL_CreateTextureFromSurface(ren, surface)))
		err_abort(SDL_GetError(), 1);
	SDL_QueryTexture(texture, NULL, NULL, &((SDL_Rect *)(params[0]))->w,
			&((SDL_Rect *)(params[0]))->h);
	SDL_FreeSurface(surface);
	return (texture);
}
