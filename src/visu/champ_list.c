/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   champ_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:48:54 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 12:22:55 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"
#include "error.h"
#include "corewar.h"
#include <libft.h>

static void		champ_list_new_set_vars(t_champ_list *obj)
{
	obj->scroll_fp = 0.0;
	obj->font_path = ft_xpathadd(obj->visual->base_path,
			"/res/noto-regular.ttf");
	if (!(obj->font = TTF_OpenFont(obj->font_path, obj->font_size)))
		err_abort(TTF_GetError(), 1);
	if (!(obj->font2 = TTF_OpenFont(obj->font_path, obj->font2_size)))
		err_abort(TTF_GetError(), 1);
	obj->textures[0] = sdl_render_text(obj->ren, obj->font,
		"cycle rate :", (void *[2]){ &obj->sizes[0], &obj->colors[2] });
	obj->textures[1] = sdl_render_text(obj->ren, obj->font,
		"current cycle :", (void *[2]){ &obj->sizes[1], &obj->colors[2] });
	obj->textures[2] = sdl_render_text(obj->ren, obj->font,
		"cycle delta :", (void *[2]){ &obj->sizes[2], &obj->colors[2] });
	obj->textures[3] = sdl_render_text(obj->ren, obj->font,
		"processes :", (void *[2]){ &obj->sizes[3], &obj->colors[2] });
	obj->textures[8] = sdl_render_text(obj->ren, obj->font2, "player ",
		(void *[2]){ &obj->sizes[8], &obj->colors[2] });
	obj->textures[9] = sdl_render_text(obj->ren, obj->font2, " :",
		(void *[2]){ &obj->sizes[9], &obj->colors[2] });
	obj->textures[10] = sdl_render_text(obj->ren, obj->font2, "name : ",
		(void *[2]){ &obj->sizes[10], &obj->colors[2] });
	obj->textures[11] = sdl_render_text(obj->ren, obj->font2, "lives : ",
		(void *[2]){ &obj->sizes[11], &obj->colors[2] });
	obj->textures[12] = sdl_render_text(obj->ren, obj->font2, "total : ",
		(void *[2]){ &obj->sizes[12], &obj->colors[2] });
}

t_champ_list	*champ_list_new(t_visual *visual, t_vm *vm)
{
	t_champ_list	*obj;
	size_t			i;

	obj = ft_xmemalloc(sizeof(*obj));
	champ_list_set_colors(obj);
	obj->visual = visual;
	obj->ren = visual->ren;
	obj->champ_count = visual->mem->champ_count;
	obj->champs = ft_xmemalloc(sizeof(*(obj->champs)) * obj->champ_count);
	i = obj->champ_count;
	obj->name_scroll_factor = 0.0;
	obj->name_scroll_pos = 0.0;
	obj->count_down = 512;
	obj->font_size = 18;
	obj->font2_size = 16;
	champ_list_new_set_vars(obj);
	obj->sizes[8].x = 8;
	obj->sizes[10].x = 8;
	obj->sizes[11].x = 8;
	obj->sizes[12].x = 8;
	while (i--)
		obj->champs[i] = champ_new(obj, vm->champions[i]->header->prog_name, i);
	champ_list_update_speed(obj);
	champ_list_update_cycle(obj);
	return (obj);
}

void			champ_list_delete(t_champ_list *obj)
{
	size_t	i;

	if (obj->font_path)
		free(obj->font_path);
	if (obj->texture_top)
		SDL_DestroyTexture(obj->texture_top);
	if (obj->texture_list)
		SDL_DestroyTexture(obj->texture_list);
	i = sizeof(obj->sizes) / sizeof(*obj->sizes);
	while (i--)
		if (obj->textures[i])
			SDL_DestroyTexture(obj->textures[i]);
	i = obj->champ_count;
	while (i--)
		champ_delete(obj->champs[i]);
	if (obj->font)
		TTF_CloseFont(obj->font);
	if (obj->font2)
		TTF_CloseFont(obj->font2);
	free(obj->champs);
	free(obj);
}

void			champ_list_draw(t_champ_list *obj)
{
	int		y;
	size_t	i;

	SDL_SetRenderTarget(obj->ren, obj->texture_top);
	set_sdl_color(obj->ren, &obj->colors[0]);
	SDL_RenderClear(obj->ren);
	i = 8;
	while (i--)
		SDL_RenderCopy(obj->ren, obj->textures[i], NULL, &obj->sizes[i]);
	set_sdl_color(obj->ren, &obj->colors[3]);
	SDL_RenderDrawLine(obj->ren, 0, obj->size_top.h - 1, obj->size_top.w,
			obj->size_top.h - 1);
	SDL_SetRenderTarget(obj->ren, obj->texture_list);
	set_sdl_color(obj->ren, &obj->colors[0]);
	SDL_RenderClear(obj->ren);
	y = 12 - (int)obj->scroll_fp;
	i = 0;
	while (i < obj->champ_count)
	{
		champ_draw(obj->champs[i++], y);
		y += obj->champs[i - 1]->size.h + 12;
	}
	SDL_SetRenderTarget(obj->ren, NULL);
	SDL_RenderCopy(obj->ren, obj->texture_top, NULL, &obj->size_top);
	SDL_RenderCopy(obj->ren, obj->texture_list, NULL, &obj->size_list);
}
