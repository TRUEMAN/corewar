/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events_processes.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:20 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/25 15:29:41 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "events.h"
#include "error.h"

void	on_write(t_visual *visual, size_t champ, size_t addr, int8_t value)
{
	visual->mem->ids[addr] = champ;
	visual->mem->mem[addr] = value;
	if (visual->display_writes)
		visual->mem->changed[addr] = visual->cycle;
	memview_update_byte(visual->memview, addr);
}

void	on_process_left(t_visual *visual, size_t addr)
{
	if (visual->mem->pcs[addr] == 0)
		err_abort("Tried to remove a process that didn't exist", 1);
	visual->mem->pcs[addr]--;
	if (visual->mem->pcs[addr] == 0)
		memview_update_byte(visual->memview, addr);
	visual->mem->process_count--;
}

void	on_process_arrived(t_visual *visual, size_t addr)
{
	visual->mem->pcs[addr]++;
	if (visual->mem->pcs[addr] == 1)
		memview_update_byte(visual->memview, addr);
	visual->mem->process_count++;
}

void	on_live(t_visual *visual, size_t champ, size_t addr)
{
	t_champ	*c;

	c = visual->champ_list->champs[champ];
	c->lives++;
	c->total_lives++;
	if (visual->display_lives)
		visual->mem->live[addr] = visual->cycle;
	if (visual->sys)
		visual->sounds[LIVE_SOUND] = 1;
}

void	on_output(t_visual *visual, size_t champ, char character)
{
	if (visual->console)
		console_add_character(visual->console, champ, character);
	visual->sounds[AFF_SOUND] = 1;
}
