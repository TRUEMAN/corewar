/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   champ_list_update.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:27:46 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 12:21:54 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"
#include <libft.h>

static inline void	champ_list_update_set_vars(t_champ_list *obj)
{
	obj->size_top.y = 16;
	obj->size_top.w = int_max(obj->visual->width / 4, 256);
	obj->size_top.x = obj->visual->width - (obj->size_top.w + 16);
	obj->size_top.h = 120;
	obj->size_list.y = obj->size_top.y + obj->size_top.h + 16;
	obj->size_list.w = obj->size_top.w;
	obj->size_list.x = obj->size_top.x;
	obj->size_list.h = obj->visual->height - (obj->size_list.y + 16);
	obj->sizes[0].x = 0;
	obj->sizes[0].y = 0;
	obj->sizes[4].x = obj->size_top.w / 2;
	obj->sizes[4].y = obj->sizes[0].y;
	obj->sizes[1].x = 0;
	obj->sizes[1].y = 28;
	obj->sizes[5].x = obj->size_top.w / 2;
	obj->sizes[5].y = obj->sizes[1].y;
	obj->sizes[2].x = 0;
	obj->sizes[2].y = 56;
	obj->sizes[6].x = obj->size_top.w / 2;
	obj->sizes[6].y = obj->sizes[2].y;
	obj->sizes[3].x = 0;
	obj->sizes[3].y = 84;
	obj->sizes[7].x = obj->size_top.w / 2;
	obj->sizes[7].y = obj->sizes[3].y;
}

void				champ_list_update(t_champ_list *obj)
{
	size_t	i;

	champ_list_update_set_vars(obj);
	if (obj->texture_top)
		SDL_DestroyTexture(obj->texture_top);
	if (obj->texture_list)
		SDL_DestroyTexture(obj->texture_list);
	obj->texture_top = create_sdl_texture(obj->ren, obj->size_top.w,
			obj->size_top.h);
	obj->texture_list = create_sdl_texture(obj->ren, obj->size_list.w,
			obj->size_list.h);
	i = obj->champ_count;
	obj->scroll_max = 12;
	while (i--)
	{
		champ_update(obj->champs[i]);
		obj->scroll_max += obj->champs[i]->size.h + 12;
	}
	obj->scroll_max -= obj->size_list.h;
	if (obj->scroll_max < 0)
		obj->scroll_max = 0;
}

void				champ_list_update_cycle(t_champ_list *obj)
{
	size_t	i;
	char	*tmp;

	obj->cycle = obj->visual->cycle;
	if (obj->textures[5])
		SDL_DestroyTexture(obj->textures[5]);
	if (obj->textures[6])
		SDL_DestroyTexture(obj->textures[6]);
	if (obj->textures[7])
		SDL_DestroyTexture(obj->textures[7]);
	tmp = ft_xitoa(obj->visual->cycle);
	obj->textures[5] = sdl_render_text(obj->ren, obj->font, tmp,
		(void *[2]){ &obj->sizes[5], &obj->colors[1] });
	free(tmp);
	tmp = ft_xitoa(obj->visual->cycle_delta);
	obj->textures[6] = sdl_render_text(obj->ren, obj->font, tmp,
		(void *[2]){ &obj->sizes[6], &obj->colors[1] });
	free(tmp);
	tmp = ft_xitoa(obj->visual->mem->process_count);
	obj->textures[7] = sdl_render_text(obj->ren, obj->font, tmp,
		(void *[2]){ &obj->sizes[7], &obj->colors[1] });
	free(tmp);
	i = obj->champ_count;
	while (i--)
		champ_update_lives(obj->champs[i]);
}

void				champ_list_on_event(t_champ_list *obj, SDL_Event *event,
							int x, int y)
{
	if (x < obj->size_list.x || x > obj->size_list.x + obj->size_list.w ||
			y < obj->size_list.y || y > obj->size_list.y + obj->size_list.h)
		return ;
	if (event->type == SDL_KEYDOWN)
	{
		if (event->key.keysym.sym == SDLK_UP)
			obj->scroll -= 16;
		else if (event->key.keysym.sym == SDLK_DOWN)
			obj->scroll += 16;
		else if (event->key.keysym.sym == SDLK_PAGEUP)
			obj->scroll -= 128;
		else if (event->key.keysym.sym == SDLK_PAGEDOWN)
			obj->scroll += 128;
	}
	else if (event->type == SDL_MOUSEWHEEL)
		obj->scroll += event->wheel.y;
}

void				champ_list_update_speed(t_champ_list *obj)
{
	char	*tmp;

	if (obj->textures[4])
		SDL_DestroyTexture(obj->textures[4]);
	tmp = obj->visual->vm->cycle_per_sec == 0 ? ft_strdup("unlimited") :
			ft_itoa(obj->visual->vm->cycle_per_sec);
	obj->textures[4] = sdl_render_text(obj->ren, obj->font, tmp,
		(void *[2]){ &obj->sizes[4], &obj->colors[1] });
	free(tmp);
}
