/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   console_update.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:10:05 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 12:20:58 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"
#include <libft.h>

void		console_update(t_console *obj)
{
	size_t	i;

	obj->size.x = 16;
	obj->size.w = obj->visual->champ_list->size_top.x - 2 * obj->size.x;
	obj->size.h = 64;
	obj->size.y = obj->visual->height - 16 - obj->size.h;
	if (obj->texture)
		SDL_DestroyTexture(obj->texture);
	obj->texture = create_sdl_texture(obj->ren, obj->size.w,
			obj->size.h);
	obj->char_count = obj->size.w / obj->char_width - 15;
	obj->scroll_max = 0;
	i = obj->visual->mem->champ_count;
	obj->scroll_max = obj->line_height * i - obj->size.h;
	if (obj->scroll_max < 0)
		obj->scroll_max = 0;
	while (i--)
	{
		console_update_string(obj, i);
		obj->sizes[i * 2 + 1].x = obj->sizes[i * 2].w + obj->char_width;
	}
}

void		console_add_character(t_console *obj, size_t champ, char character)
{
	char	tmp[2];
	char	*res;

	tmp[0] = ft_isprint(character) ? character : '.';
	tmp[1] = '\0';
	if (obj->strings[champ])
	{
		res = ft_strjoin(obj->strings[champ], tmp);
		free(obj->strings[champ]);
		obj->strings[champ] = res;
	}
	else
		obj->strings[champ] = ft_strdup(tmp);
	obj->string_len[champ * 2]++;
}

void		console_update_string(t_console *obj, size_t index)
{
	size_t	start;
	size_t	size;
	char	*tmp;

	if (!obj->strings[index])
		return ;
	if (obj->string_len[index * 2] > obj->char_count)
		size = obj->char_count;
	else
		size = obj->string_len[index * 2];
	start = obj->string_len[index * 2] - size;
	tmp = ft_strsub(obj->strings[index], start, size);
	if (obj->textures[index * 2 + 1])
		SDL_DestroyTexture(obj->textures[index * 2 + 1]);
	obj->textures[index * 2 + 1] = sdl_render_text(obj->ren, obj->font, tmp,
		(void *[2]){ &obj->sizes[index * 2 + 1], &obj->colors[4 + index % 8] });
	free(tmp);
}

void		console_on_event(t_console *obj, SDL_Event *event, int x, int y)
{
	if (x < obj->size.x || x > obj->size.x + obj->size.w ||
			y < obj->size.y || y > obj->size.y + obj->size.h)
		return ;
	if (event->type == SDL_KEYDOWN)
	{
		if (event->key.keysym.sym == SDLK_UP)
			obj->scroll -= 16;
		else if (event->key.keysym.sym == SDLK_DOWN)
			obj->scroll += 16;
		else if (event->key.keysym.sym == SDLK_PAGEUP)
			obj->scroll -= 128;
		else if (event->key.keysym.sym == SDLK_PAGEDOWN)
			obj->scroll += 128;
	}
	else if (event->type == SDL_MOUSEWHEEL)
		obj->scroll += event->wheel.y;
}

void		console_draw(t_console *obj)
{
	size_t	i;

	SDL_SetRenderTarget(obj->ren, obj->texture);
	set_sdl_color(obj->ren, &obj->colors[0]);
	SDL_RenderClear(obj->ren);
	i = obj->visual->mem->champ_count * 2;
	while (i--)
		if (obj->textures[i])
		{
			obj->sizes[i].y = obj->line_height * (i / 2) - (int)obj->scroll_fp;
			SDL_RenderCopy(obj->ren, obj->textures[i], NULL, &obj->sizes[i]);
		}
	SDL_SetRenderTarget(obj->ren, NULL);
	SDL_RenderDrawLine(obj->ren, obj->size.x, obj->size.y, obj->size.w,
			obj->size.y);
	SDL_RenderCopy(obj->ren, obj->texture, NULL, &obj->size);
	i = obj->visual->mem->champ_count;
	while (i--)
	{
		if (obj->string_len[i * 2] == obj->string_len[i * 2 + 1])
			continue ;
		obj->string_len[i * 2 + 1] = obj->string_len[i * 2];
		console_update_string(obj, i);
	}
}
