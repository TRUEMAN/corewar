/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memview.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:48:40 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 10:48:42 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"
#include "error.h"
#include <libft.h>

t_memview	*memview_new(t_visual *visual, size_t font_size)
{
	t_memview	*obj;

	obj = ft_xmemalloc(sizeof(*obj));
	memview_set_colors(obj);
	obj->visual = visual;
	obj->ren = visual->ren;
	obj->mem = visual->mem;
	obj->byte_count = 16 * (2 + obj->mem->champ_count);
	obj->bytes = ft_xmemalloc(sizeof(*(obj->bytes)) * obj->byte_count);
	obj->scroll = 0;
	obj->scroll_fp = 0.0;
	obj->line_max = 64;
	obj->font_path = ft_xpathadd(obj->visual->base_path, "/res/noto-mono.ttf");
	obj->stealth = visual->vm->args.stealth_mode;
	memview_set_font_size(obj, 9 + (font_size - 1) * 3);
	return (obj);
}

void		memview_delete(t_memview *obj)
{
	size_t	i;

	if (obj->font_path)
		free(obj->font_path);
	memview_delete_lines(obj);
	i = 0;
	while (i < obj->byte_count)
		byte_texture_delete(obj->bytes[i++]);
	free(obj->bytes);
	if (obj->font)
		TTF_CloseFont(obj->font);
	free(obj);
}

void		memview_delete_lines(t_memview *obj)
{
	size_t	i;

	if (obj->texture)
		SDL_DestroyTexture(obj->texture);
	if (!obj->lines)
		return ;
	i = obj->line_count;
	while (i--)
		SDL_DestroyTexture(obj->lines[i]);
	free(obj->lines);
	obj->lines = NULL;
}

void		memview_set_font_size(t_memview *obj, int font_size)
{
	size_t		i;

	obj->font_size = int_clamp(font_size, 8, 42);
	i = obj->byte_count;
	if (obj->font)
		TTF_CloseFont(obj->font);
	if (!(obj->font = TTF_OpenFont(obj->font_path, obj->font_size)))
		err_abort(TTF_GetError(), 1);
	obj->byte_size = (SDL_Rect){ 0, 0, 0, 0 };
	while (i--)
	{
		if (obj->bytes[i])
			byte_texture_delete(obj->bytes[i]);
		obj->bytes[i] = byte_texture_new(obj, i % 16, obj->colors[i / 16]);
		obj->byte_size.w = int_max(obj->bytes[i]->size.w, obj->byte_size.w);
		obj->byte_size.h = int_max(obj->bytes[i]->size.h, obj->byte_size.h);
	}
	obj->byte_full_size.x = obj->byte_size.w / 3;
	obj->byte_full_size.y = obj->byte_size.h / 12;
	obj->byte_full_size.w = (obj->byte_size.w + obj->byte_full_size.x) * 2;
	obj->byte_full_size.h = obj->byte_size.h + obj->byte_full_size.y * 2;
	champ_list_update(obj->visual->champ_list);
	memview_update(obj);
}
