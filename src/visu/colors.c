/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:11 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 10:49:11 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "utils.h"

void	visual_set_colors(t_visual *obj)
{
	obj->colors[0] = get_color(0x263238FF);
	obj->colors[1] = get_color(0xECEFF1FF);
}

/*
** Colors:
** 0: background
** 1: regular
** 2: champion 1
** 3: champion 2
** 4: champion 3
** 5: champion 4
*/

void	memview_set_colors(t_memview *obj)
{
	obj->colors[0] = get_color(0x263238FF);
	obj->colors[1] = get_color(0xECEFF1FF);
	obj->colors[2] = get_color(0x8BC34AFF);
	obj->colors[3] = get_color(0xFF9800FF);
	obj->colors[4] = get_color(0x03A9F4FF);
	obj->colors[5] = get_color(0xF44336FF);
	obj->colors[6] = get_color(0x9C27B0FF);
	obj->colors[7] = get_color(0x009688FF);
	obj->colors[8] = get_color(0xFFEB3BFF);
	obj->colors[9] = get_color(0x3F51B5FF);
}

void	champ_list_set_colors(t_champ_list *obj)
{
	obj->colors[0] = get_color(0x263238FF);
	obj->colors[1] = get_color(0xECEFF1FF);
	obj->colors[2] = get_color(0xB0BEC5FF);
	obj->colors[3] = get_color(0x607D8BFF);
	obj->colors[4] = get_color(0x8BC34AFF);
	obj->colors[5] = get_color(0xFF9800FF);
	obj->colors[6] = get_color(0x03A9F4FF);
	obj->colors[7] = get_color(0xF44336FF);
	obj->colors[8] = get_color(0x9C27B0FF);
	obj->colors[9] = get_color(0x009688FF);
	obj->colors[10] = get_color(0xFFEB3BFF);
	obj->colors[11] = get_color(0x3F51B5FF);
}

void	console_set_colors(t_console *obj)
{
	obj->colors[0] = get_color(0x263238FF);
	obj->colors[1] = get_color(0xB0BEC5FF);
	obj->colors[2] = get_color(0x607D8BFF);
	obj->colors[4] = get_color(0x8BC34AFF);
	obj->colors[5] = get_color(0xFF9800FF);
	obj->colors[6] = get_color(0x03A9F4FF);
	obj->colors[7] = get_color(0xF44336FF);
	obj->colors[8] = get_color(0x9C27B0FF);
	obj->colors[9] = get_color(0x009688FF);
	obj->colors[10] = get_color(0xFFEB3BFF);
	obj->colors[11] = get_color(0x3F51B5FF);
}
