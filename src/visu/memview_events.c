/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memview_events.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 10:49:29 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/18 10:49:30 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visual.h"
#include "error.h"
#include "utils.h"
#include <libft.h>

void	memview_draw(t_memview *obj)
{
	int			i;
	int			j;
	SDL_Rect	line;

	i = 0;
	j = 0;
	if (obj->scroll_fp > 0)
		i = (int)(obj->scroll_fp / (float)obj->line_height);
	line.x = 0;
	line.y = i * obj->line_height - obj->scroll_fp;
	line.w = obj->size.w;
	SDL_SetRenderTarget(obj->ren, obj->texture);
	set_sdl_color(obj->ren, &obj->colors[0]);
	SDL_RenderClear(obj->ren);
	while (i < obj->line_count && (int)j <= obj->size.h + obj->line_height)
	{
		line.h = obj->line_height;
		SDL_RenderCopy(obj->ren, obj->lines[i], NULL, &line);
		line.y += line.h;
		i++;
		j += obj->line_height;
	}
	SDL_SetRenderTarget(obj->ren, NULL);
	SDL_RenderCopy(obj->ren, obj->texture, NULL, &obj->size);
}

void	memview_on_event(t_memview *obj, SDL_Event *event, int x, int y)
{
	if (x < obj->size.x || x > obj->size.x + obj->size.w ||
			y < obj->size.y || y > obj->size.y + obj->size.h)
		return ;
	if (event->type == SDL_KEYDOWN)
	{
		if (event->key.keysym.sym == SDLK_UP)
			obj->scroll -= obj->line_height;
		else if (event->key.keysym.sym == SDLK_DOWN)
			obj->scroll += obj->line_height;
		else if (event->key.keysym.sym == SDLK_PAGEUP)
			obj->scroll -= (obj->size.h / obj->line_height) * obj->line_height;
		else if (event->key.keysym.sym == SDLK_PAGEDOWN)
			obj->scroll += (obj->size.h / obj->line_height) * obj->line_height;
		else if (event->key.keysym.sym == SDLK_HOME)
			memview_set_font_size(obj, obj->font_size + 1);
		else if (event->key.keysym.sym == SDLK_END)
			memview_set_font_size(obj, obj->font_size - 1);
	}
	else if (event->type == SDL_MOUSEWHEEL)
		obj->scroll += event->wheel.y;
}
