/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_vm.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/06 14:37:52 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 14:09:08 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <SDL.h>

static void	set_starting_vals(t_vm *vm)
{
	vm->crnt_pc = vm->pc;
	vm->end_of_cycle = CYCLE_TO_DIE;
	vm->end_of_cycle_base_val = CYCLE_TO_DIE;
	vm->winner = vm->args.champions_id[vm->args.nb_player - 1];
	vm->paused = 1;
	vm->step = 0;
	vm->nb_live_in_crnt_periode = 0;
	vm->quit = 0;
	vm->checks = 1;
	vm->nb_cycles = 0;
	vm->cycle_per_sec = 1000;
	vm->last_tick = SDL_GetTicks();
	vm->cycle_nxt_op = -1;
	vm->interval = 1000 / vm->cycle_per_sec;
	vm->pause_at_next_operation = 0;
}

static int	handle_err(int err, char *player)
{
	ft_printf("ERROR : ");
	if (err == -1)
		ft_printf("Unable to read file corresponding to player \'%s\'\n",
				player);
	else if (err == -2)
		ft_printf("Bad header (bad size) for player \'%s\'\n", player);
	else if (err == -3)
		ft_printf("Bad header (bad magic) for player \'%s\'\n", player);
	else if (err == -4)
		ft_printf("Champion is too large for player \'%s\'\n", player);
	return (-1);
}

int			init_vm(t_vm *vm, int nb_args, char **args)
{
	int	i;
	int	err;

	if ((pars_args(nb_args, args, &(vm->args))) == -1)
		return (-1);
	vm->memory_size = vm->args.memory_size;
	vm->index_mod = IDX_MOD;
	vm->champions = ft_xmemalloc(sizeof(*vm->champions) * vm->args.nb_player);
	vm->ram = set_up_ram(vm->memory_size);
	err = 0;
	i = -1;
	while (++i < vm->args.nb_player)
	{
		vm->champions[i] = load_champion(vm->args.champions[i],
							vm->args.champions_id[i], &err);
		if (err < 0)
			return (handle_err(err, vm->args.champions[i]));
	}
	load_champs_in_ram(vm, vm->champions, vm->args.nb_player);
	vm->pc = pc_load_champs(vm, &(vm->pc));
	if (vm->pc == NULL)
		return (-1);
	set_starting_vals(vm);
	return (0);
}
