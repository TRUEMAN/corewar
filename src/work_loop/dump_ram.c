/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dump_ram.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 14:55:21 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 18:48:51 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "corewar.h"
#include "visual.h"

static void	wait_until_cariage_return(void)
{
	char	buff[2];

	while (read(0, buff, 2))
		if (buff[0] == '\n')
			break ;
}

void		dump_ram(void *ram, t_vm *vm)
{
	unsigned char	*ptr;
	int				size;
	int				val;

	ptr = (unsigned char*)ram;
	size = vm->memory_size;
	while (--size >= 0)
	{
		if (size % 64 == 63)
			ft_printf("0x%.4x : ", vm->memory_size - size - 1);
		val = (int)*ptr++;
		ft_printf("%.2x%s",
		val, size % 64 == 0 ? " \n" : " ");
	}
	if (vm->args.dump_cycle > 0 && !(vm->args.serial_dump > 0))
		exit(0);
	if (!vm->visual)
		wait_until_cariage_return();
	else
		vm->paused = 1;
}
