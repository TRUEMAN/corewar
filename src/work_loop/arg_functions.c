/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arg_functions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:53:05 by guiricha          #+#    #+#             */
/*   Updated: 2016/11/25 15:30:14 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		parse_argument_ext(char *arg, t_args *args)
{
	if (!ft_strcmp("-a", arg))
		return (args->aff = 1);
	else if (!ft_strcmp("-w", arg))
		return (args->write = 1);
	else if (!ft_strcmp("-l", arg))
		return (args->live = 1);
	else if (!ft_strcmp("-n", arg))
		return (args->enable_visualiser = 1);
	else if (!ft_strcmp("--ignore-max-players", arg))
		return (args->ignore_max_players = 1);
	return (0);
}

int		next_available(t_champll *start)
{
	int			nbr;
	t_champll	*loop;

	loop = start;
	nbr = -1;
	while (start)
	{
		if (start->number == nbr)
		{
			nbr--;
			start = loop;
			continue ;
		}
		start = start->next;
	}
	return (nbr);
}

int		is_used(int nbr, t_champll *start)
{
	while (start)
	{
		if (nbr == start->number)
			return (1);
		start = start->next;
	}
	return (0);
}

void	init_flags(t_args *arg)
{
	arg->err = 0;
	arg->ignore_max_players = 0;
	arg->aff = 0;
	arg->dump_cycle = 0;
	arg->serial_dump = 0;
	arg->verbosity_val = 0;
	arg->enable_visualiser = 0;
	arg->stealth_mode = 0;
	arg->nb_player = 0;
	arg->audio = 0;
	arg->nb_player_set = 0;
	arg->param_error = 0;
	arg->champlist = NULL;
	arg->champions = NULL;
	arg->champions_id = NULL;
	arg->memory_size = MEM_SIZE;
	arg->tog_args = 1;
	arg->write = 0;
	arg->live = 0;
	arg->font_size = 4;
}
