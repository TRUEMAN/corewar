/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_arg_functions.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:51:34 by guiricha          #+#    #+#             */
/*   Updated: 2016/11/25 15:40:05 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include "error.h"

void		print_arguments(t_args *args)
{
	char	**champs;
	int		*champs_id;

	champs = args->champions;
	champs_id = args->champions_id;
	ft_printf("Error value : %hhd\nignore-max-players value : ", args->err);
	ft_printf("%d\nAff value : %d\n", args->ignore_max_players, args->aff);
	ft_printf("Dump Cycle value : %d\n", args->dump_cycle);
	ft_printf("Serial Dump value : %d\n", args->serial_dump);
	ft_printf("Verbosity value : %d\n", args->verbosity_val);
	ft_printf("Write value : %d\n", args->write);
	ft_printf("Live value : %d\n", args->live);
	ft_printf("Visual Output value : %d\n", args->enable_visualiser);
	ft_printf("Visual Stealth value : %d\n", args->stealth_mode);
	ft_printf("nb_players value : %d\nChampions below\n", args->nb_player);
	if (champs == NULL)
		return ;
	while (*champs)
	{
		ft_printf("NAME: %-30s NUMBER: %5d\n", *champs, *champs_id);
		champs = champs + 1;
		champs_id++;
	}
}

int			parse_error(t_args *args, char *arg)
{
	if (args->err > -10)
		ft_putstr("ERROR : ");
	if (args->err == INT_MIN_E)
		ft_putstr("Input for argument smaller than INT_MIN");
	if (args->err == INT_MAX_E)
		ft_putstr("Input for argument larger than INT_MAX");
	if (args->err == INPUT_NAN_E)
		ft_putstr("Input not a number");
	if (args->err == INPUT_NULL_E)
		ft_putstr("No number");
	if (args->err == N_ALREADY_USED_E)
		ft_putstr("Value already used for -p option");
	if (args->err == NO_PLAYERS_E)
		ft_putstr("NO PLAYERS\n");
	if (args->err == MALLOC_E)
		ft_putstr("Memory allocation failed");
	if (args->err == NULL_ARG_E)
		ft_putstr("Null string as argument");
	if (args->err == INVALID_PARAM_E)
		ft_putstr("Unrecognized parameter");
	if (args->err != NO_PLAYERS_E && args->err > -10)
		ft_printf(" on argument \"%s\"\n", arg);
	return (-1);
}

static void	input_error_continued(t_args *a)
{
	if (a->serial_dump < 0)
	{
		a->err = S_DUMP_OUT_OF_RANGE_E;
		ft_printf("ERROR : -s Chosen serial_dump size too small         %s\n",
				"( value cannot be lower than 1 )");
	}
	if (a->nb_player > MAX_PLAYERS && !a->ignore_max_players)
	{
		a->err = TOO_MANY_PLAYERS_E;
		ft_printf("ERROR :    Too many players, max [%d], you have [%d]   %s\n",
				MAX_PLAYERS, a->nb_player, "( --ignore-max-players )");
	}
	if (a->font_size < 1 || a->font_size > 10)
	{
		a->err = -40;
		ft_printf("ERROR : -f Chosen font_size too small or large       %s\n",
				"( value cannot be lower than 1 or greater than 10 )");
	}
}

void		input_error(t_args *a)
{
	if (a->memory_size < (a->nb_player * (CHAMP_MAX_SIZE)) ||
			a->memory_size > CHAMP_MAX_SIZE * 500)
	{
		a->err = MEM_TOO_SMALL_E;
		ft_printf("ERROR : -m Chosen mem_size too small or large."
				"\nMinimum size below\n( "
				"nb_players(%d) * CHAMP_MAX_SIZE(%d)) = %d\nMaximum size below"
				"\nCHAMP_MAX_SIZE(%d) * 500 = %d\n",
				a->nb_player, (CHAMP_MAX_SIZE), a->nb_player * CHAMP_MAX_SIZE,
				CHAMP_MAX_SIZE, CHAMP_MAX_SIZE * 500);
	}
	if ((a->verbosity_val < 0 || a->verbosity_val > 63))
	{
		a->err = VERB_OUT_OF_RANGE_E;
		ft_printf("ERROR : -v Chosen verbosity value is out of range    %s\n",
				"( range is between 0 and 63 )");
	}
	if (a->dump_cycle < 0)
	{
		a->err = DUMP_C_OUT_OF_RANGE_E;
		ft_printf("ERROR : -d Chosen dump_cycle too small               %s\n",
				"( value cannot be lower than 1 )");
	}
	input_error_continued(a);
}

int			usage_options(void)
{
	ft_putendl("Usage: ./corewar [-dN -sN -mN -n --stealth -a -vN]"
		"\n[-pN] <champion1.cor> <...> -- champion2.cor champion3.cor\n"
		"    -dumpN    : Dumps memory after N cycles then exits\n"
		"    -sN       : Runs N cycles, dumps memory, pauses, then repeats\n"
		"    -mN       : Sets available memory to N, overriding MEM_SIZE\n"
		"    -n        : Visual output mode\n"
		"    -l        : Show lives in Visual output mode\n"
		"    -w        : Show writes in Visual output mode\n"
		"    --stealth : Hides the real contents of the memory\n"
		"    -a        : Prints from \"aff\" (Default is to hide it)\n"
		"    -pN       : Sets the next players number to N\n"
		"    -fN       : Sets the visualizer's font size (1-10)\n"
		"    --        : Takes next arguments as champions, even valid args\n"
		"    -z        : Plays sound for certain actions. Try it out!\n"
		"    -vN       : Verbosity levels, can be added together\n"
		"                - 0 : Show only essentials\n"
		"                - 1 : Show lives\n"
		"                - 2 : Show cycles\n"
		"                - 4 : Show operations (Params are NOT litteral ...)\n"
		"                - 8 : Show deaths\n"
		"                - 16 : Show PC movements (Except for jumps)\n"
		"                - 32 : Show all parameter values\n"
		"    --ignore-max-players : Lifts restrictions on champion count");
	return (1);
}
