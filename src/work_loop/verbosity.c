/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verbosity.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 16:45:07 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 14:08:09 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "vm.h"
#include "corewar.h"

void	show_live_verbosity(t_vm *vm, int champion_index)
{
	if ((vm->args.verbosity_val & 0x01) == 1)
		ft_printf("Player %d (%s) is said to be alive\n", champion_index + 1,
			vm->champions[champion_index]->header->prog_name);
}

/*
** Si cycle_to_die_update == 1, alors affiche le message d'edition du cycle to
** die.
*/

void	cycle_verbosity(t_vm *vm, int cycle_to_die_update)
{
	if ((vm->args.verbosity_val & 0x02) == 2 && cycle_to_die_update == 0)
		ft_printf("It is now cycle %d\n", vm->nb_cycles);
	if ((vm->args.verbosity_val & 0x02) == 2 && cycle_to_die_update == 1)
		ft_printf("Cycle to die is now %d\n", vm->end_of_cycle_base_val);
}

void	show_death_verbosity(t_pc_elem *op)
{
	if ((op->vm->args.verbosity_val & 0x08) == 8)
		ft_printf("Process %d hasn't lived for %d cycles (CTD %d)\n",
			op->proc_num + 1, op->vm->nb_cycles - op->last_live_cycle,
			op->vm->end_of_cycle_base_val);
}

void	show_pc_moves_verbosity(t_pc_elem *op, int op_len)
{
	unsigned int	crnt_ram_idx;
	unsigned int	nxt_ram_idx;
	unsigned char	op_str[op_len];
	int				i;

	if ((op->vm->args.verbosity_val & 0x10) != 16)
		return ;
	crnt_ram_idx = (unsigned int)(op->adr_crnt_pros - op->ram_orgn);
	nxt_ram_idx = crnt_ram_idx + op_len;
	if (nxt_ram_idx >= (unsigned int)op->vm->memory_size)
		nxt_ram_idx -= op->vm->memory_size;
	ft_printf("ADV %d (0x%04x -> 0x%04x) ", op_len, crnt_ram_idx, nxt_ram_idx);
	if (crnt_ram_idx < nxt_ram_idx)
		ft_memcpy(op_str, op->adr_crnt_pros, op_len);
	else
	{
		ft_memcpy(op_str, op->adr_crnt_pros,
			op->vm->memory_size - (crnt_ram_idx));
		ft_memcpy(op_str + (op->vm->memory_size - (crnt_ram_idx)),
			op->ram_orgn, nxt_ram_idx);
	}
	i = -1;
	while (++i < op_len)
		ft_printf("%02x ", (unsigned int)op_str[i]);
	ft_printf("\n");
}
