/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   workloop_steps.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 12:52:55 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 17:48:34 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "vm.h"
#include "corewar.h"
#include "pc_scan_steps.h"
#include "visual.h"
#include "events.h"
#include "flags.h"

static int	nxt_to_solve(t_pc_lst *pc, t_vm *vm, size_t current_tick)
{
	int	cycle_count_down;

	vm->last_tick = current_tick;
	if ((vm->cycle_per_sec != 0 && vm->visual)
		|| (vm->args.serial_dump > 0 || vm->args.dump_cycle > 0))
		return (vm->cycle_nxt_op = 1);
	if (pc->process == NULL)
		return (0);
	cycle_count_down = pc->process->instruct_cycle;
	while (pc != NULL)
	{
		if (pc->process == NULL)
			return (0);
		if (cycle_count_down > pc->process->instruct_cycle)
			cycle_count_down = pc->process->instruct_cycle;
		pc = pc->next;
	}
	if (vm->end_of_cycle - cycle_count_down > 0 && vm->step == 0)
		vm->cycle_nxt_op = cycle_count_down <= 0 ? 1 : cycle_count_down;
	else if (vm->end_of_cycle > 0 && vm->step == 0)
		vm->cycle_nxt_op = vm->end_of_cycle;
	else
		vm->cycle_nxt_op = 1;
	return (0);
}

static void	pc_scan(t_pc_lst *pc, t_vm *vm)
{
	int				set_pause;

	set_pause = 0;
	while (pc != NULL)
	{
		if (pc->process == NULL)
			return ;
		else
		{
			step_in_operation(pc->process);
			pc->process->instruct_cycle -= vm->cycle_nxt_op;
			pc->process->cpt_cycles += vm->cycle_nxt_op;
			if (pc->process->instruct_cycle <= 0)
			{
				eval_op(pc->process);
				set_pause = 1;
			}
		}
		pc = pc->next;
	}
	if (set_pause == 1 && vm->pause_at_next_operation == 1)
	{
		vm->paused = 1;
		vm->pause_at_next_operation = 0;
	}
}

void		clean_post_end_of_loop(t_vm *vm)
{
	clean_pc_lst(&(vm->pc));
	if (vm->nb_live_in_crnt_periode >= NBR_LIVE || vm->checks >= MAX_CHECKS)
	{
		vm->end_of_cycle_base_val -= CYCLE_DELTA;
		vm->checks = 1;
		if (vm->end_of_cycle_base_val < 0)
			vm->end_of_cycle_base_val = 0;
		cycle_verbosity(vm, 1);
	}
	else
		++(vm->checks);
	vm->nb_live_in_crnt_periode = 0;
	vm->end_of_cycle = vm->end_of_cycle_base_val;
	if (vm->args.enable_visualiser == 1)
		on_cycle_delta_changed(vm->visual, vm->end_of_cycle);
}

int			cycle_loop(t_vm *vm)
{
	size_t	current_tick;

	if (vm->args.enable_visualiser == 1)
		current_tick = SDL_GetTicks();
	if ((!vm->paused && (vm->args.enable_visualiser == 0 ||
		vm->cycle_per_sec == 0 ||
		vm->last_tick + vm->interval < current_tick)) || vm->step)
	{
		nxt_to_solve(vm->pc, vm, current_tick);
		vm->step = 0;
		vm->end_of_cycle -= vm->cycle_nxt_op;
		vm->nb_cycles += vm->cycle_nxt_op;
		if ((vm->args.dump_cycle == vm->nb_cycles) ||
			(vm->args.serial_dump != 0 &&
			(vm->nb_cycles % vm->args.serial_dump == 0)))
			dump_ram(vm->pc->process->ram_orgn, vm);
		cycle_verbosity(vm, 0);
		pc_scan(vm->pc, vm);
		while (vm->args.enable_visualiser == 1 && vm->cycle_nxt_op-- > 0)
			on_next_cycle(vm->visual);
	}
	if (vm->args.enable_visualiser == 1)
		if (!visual_loop(vm->visual))
			return (1);
	return (0);
}

void		starting_message(const t_vm *vm)
{
	int		i;

	ft_printf("Introducing contestants...\n");
	i = -1;
	while (++i < vm->args.nb_player)
	{
		ft_printf("* Player %d, weighing %d bytes, \"%s\" (\"%s\") !\n",
			i + 1, vm->champions[i]->header->prog_size,
			vm->champions[i]->header->prog_name,
			vm->champions[i]->header->comment);
	}
}
