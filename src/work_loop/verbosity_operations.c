/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verbosity_operations.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 18:47:52 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 14:11:34 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "vm.h"
#include "corewar.h"
#include <libft.h>

static void	print_prss_prm(const t_pc_elem *op, const t_params *prm, int op_idx)
{
	int		i;
	int		prm_val;

	i = -1;
	while (++i < g_op_tab[op_idx].nb_params)
	{
		if (prm->p_type[i] == IND_CODE)
		{
			prm_val = get_indirect_val(get_indirect_adr(prm->val[i], op), op);
			reverse_bytes(&prm_val, sizeof(int));
			ft_printf("       | Param [%d] is indirect. Indirect value = %d\n",
					i, prm_val);
		}
		else if (prm->p_type[i] == REG_CODE)
		{
			ft_memcpy(&prm_val, op->reg[prm->val[i] - 1], sizeof(int));
			reverse_bytes(&prm_val, sizeof(int));
			ft_printf("%s[%d] is a register. Value inside the register = %d\n",
					"       | Param ", i, prm_val);
		}
		else if (prm->p_type[i] == DIR_CODE)
			ft_printf("       | Param [%d] is a direct. Value = %d\n", i,
				prm->val[i]);
	}
}

static void	print_prss_asm(const t_pc_elem *op, const t_params *prm, int op_idx)
{
	int		i;

	i = -1;
	while (++i < g_op_tab[op_idx].nb_params)
	{
		if (prm->p_type[i] != IND_CODE)
			ft_printf(prm->p_type[i] == REG_CODE ? " r%d" : " %%%d",
					prm->val[i]);
		else
			ft_printf(" %d", prm->val[i]);
	}
	i = -1;
	if (op_idx == 0x08)
		ft_printf(op->carry == 1 ? " (DO JUMP)" : " (DON'T JUMP)");
	ft_printf("\n       | Values given to the function as parameters:\n");
	if (g_op_tab[op_idx].has_pcode == 0)
	{
		ft_printf("       | Param [%d] is a direct. Value = %d\n",
				0, prm->val[0]);
		return ;
	}
	print_prss_prm(op, prm, op_idx);
}

void		show_operations_param(const t_pc_elem *op, const t_params *params)
{
	int		op_idx;

	if ((op->vm->args.verbosity_val & 4) != 4 || op->op_code == 0)
		return ;
	op_idx = op->op_code > 0 && op->op_code <= 16 ? op->op_code - 1 : 16;
	ft_printf("P %4d | %s", op->proc_num + 1, g_op_tab[op_idx].name);
	print_prss_asm(op, params, op_idx);
}
