/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_champion.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/06 14:09:06 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/15 13:53:05 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void		reverse_bytes(void *mem, size_t size)
{
	size_t	pos;
	size_t	max;
	char	tmp;

	max = size - 1;
	pos = 0;
	while (pos < size / 2)
	{
		tmp = ((char *)mem)[pos];
		((char *)mem)[pos] = ((char *)mem)[max - pos];
		((char *)mem)[max - pos] = tmp;
		pos++;
	}
}

static int	load_header(int fd, t_champion *champ)
{
	size_t	hdr_pos;
	size_t	hdr_size;
	int		ret;

	hdr_size = sizeof(*champ->header);
	champ->header = ft_xmalloc(hdr_size);
	hdr_pos = 0;
	ret = 1;
	while (ret > 0 && hdr_pos < hdr_size)
		if ((ret = read(fd, champ->header + hdr_pos, hdr_size - hdr_pos)) > 0)
			hdr_pos += ret;
	if (ret != -1 && hdr_pos == hdr_size)
	{
		reverse_bytes(&(champ->header->magic), sizeof(champ->header->magic));
		reverse_bytes(&(champ->header->prog_size),
				sizeof(champ->header->prog_size));
		if (champ->header->magic == COREWAR_EXEC_MAGIC)
			return (1);
		ft_memdel((void **)&(champ->header));
		return (-3);
	}
	ft_memdel((void **)&(champ->header));
	return (ret == -1 ? -1 : -2);
}

static int	load_code(int fd, t_champion *champ)
{
	size_t	pos;
	int		ret;

	champ->size = champ->header->prog_size;
	champ->code = ft_xmalloc(sizeof(*(champ->code)) * champ->size);
	pos = 0;
	ret = 1;
	while (ret > 0 && pos < champ->size)
	{
		ret = read(fd, champ->code + pos, champ->size - pos);
		if (ret > 0)
			pos += ret;
	}
	ret = read(fd, &pos, sizeof(pos));
	if (ret != -1 && pos == champ->size)
		return (1);
	ft_memdel((void **)&(champ->code));
	return (ret == -1 ? -1 : -4);
}

t_champion	*load_champion(char *filename, int number, int *err)
{
	int			fd;
	int			ret;
	t_champion	*champ;

	if ((fd = open(filename, O_RDONLY)) == -1)
	{
		*err = -1;
		return (NULL);
	}
	champ = ft_xmalloc(sizeof(*champ));
	champ->header = NULL;
	champ->code = NULL;
	if ((ret = load_header(fd, champ)) == 1)
		ret = load_code(fd, champ);
	close(fd);
	champ->number = number;
	if (ret == 1)
		return (champ);
	*err = ret;
	if (champ->header)
		free(champ->header);
	if (champ->code)
		free(champ->code);
	free(champ);
	return (NULL);
}

/*
** Error codes:
** -1: unable to read file
** -2: bad header (bad size)
** -3: bad header (bad magic)
** -4: code size doesn't match size specified in header
*/
