/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_args.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 10:40:35 by guiricha          #+#    #+#             */
/*   Updated: 2016/11/25 15:38:50 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include "error.h"

/*
** going for POSIX styled argument parsing, AKA everything is either
** an argument or a champion until I get the (char*) "--"
** after which everything is a champion.
*/

static t_champll	*new_champll(char *name, t_args *args)
{
	t_champll	*new;
	t_champll	*travel;

	travel = args->champlist;
	new = (t_champll *)malloc(sizeof(t_champll));
	if (!new && (args->err = MALLOC_E))
		return (NULL);
	new->number = args->nb_player_set ? args->nb_player :
		next_available(args->champlist);
	if (is_used(new->number, args->champlist) && (args->err = N_ALREADY_USED_E))
		return (NULL);
	args->nb_player = 0;
	args->nb_player_set = 0;
	new->name = ft_strdup(name);
	new->next = NULL;
	if (!travel)
		return (new);
	while (travel && travel->next)
		travel = travel->next;
	travel->next = new;
	return (args->champlist);
}

static int			parse_champion(char *name, t_args *args)
{
	if (!*name)
		return (NULL_ARG_E);
	if (*name == '-' && args->tog_args)
		return (INVALID_PARAM_E);
	args->champlist = new_champll(name, args);
	if (args->champlist == NULL && args->err == MALLOC_E)
		return (MALLOC_E);
	if (args->champlist == NULL && args->err == N_ALREADY_USED_E)
		return (N_ALREADY_USED_E);
	return (1);
}

static int			parse_argument(char *arg, t_args *args)
{
	if (!ft_strcmp(arg, "--"))
		return ((args->tog_args = 0));
	else if (!ft_strncmp("-dump", arg, 5))
		return (ft_strtoint(arg + 5, &(args->dump_cycle)));
	else if (!ft_strncmp("-f", arg, 2))
		return (ft_strtoint(arg + 2, (int *)&(args->font_size)));
	else if (!ft_strncmp("-v", arg, 2))
		return (ft_strtoint(arg + 2, &(args->verbosity_val)));
	else if (!ft_strncmp("-s", arg, 2))
		return (ft_strtoint(arg + 2, &(args->serial_dump)));
	else if (!ft_strncmp("-p", arg, 2) && (args->nb_player_set = 1))
		return (ft_strtoint(arg + 2, &(args->nb_player)));
	else if (!ft_strncmp("-m", arg, 2))
		return (ft_strtoint(arg + 2, &(args->memory_size)));
	else if (!ft_strcmp("--stealth", arg))
		return (args->stealth_mode = 1);
	else if (!ft_strcmp("-z", arg))
		return (args->audio = 1);
	else if (parse_argument_ext(arg, args))
		return (1);
	else
		return (parse_champion(arg, args));
}

static int			convert_champs(t_args *args)
{
	int			len;
	t_champll	*travel;

	travel = args->champlist;
	len = 0;
	while (travel && ++len)
		travel = travel->next;
	if (len == 0)
		return (args->err = -6);
	travel = args->champlist;
	args->champions = (char **)malloc(sizeof(char *) * len + 1);
	args->champions_id = (int *)malloc(sizeof(int) * len);
	if ((!args->champions || !args->champions_id) && (args->err = -7))
		return (0);
	args->champions[len] = NULL;
	args->nb_player = len;
	len = -1;
	while (travel)
	{
		args->champions[++len] = ft_strdup(travel->name);
		args->champions_id[len] = travel->number;
		travel = travel->next;
	}
	return (1);
}

int					pars_args(int argc, char **argv, t_args *args)
{
	int		index;

	index = 1;
	init_flags(args);
	if (argc <= 1 && (usage_options()))
		return (-1);
	while (index < argc)
	{
		if (args->tog_args == 1)
		{
			if ((args->err = parse_argument(argv[index], args)) < 0)
				return (parse_error(args, argv[index]));
		}
		else if ((args->err = parse_champion(argv[index], args)) < 0)
			return (parse_error(args, argv[index]));
		index++;
	}
	convert_champs(args);
	input_error(args);
	if (args->verbosity_val & 0b100000)
		print_arguments(args);
	if (args->err < 0)
		return (parse_error(args, argv[index]));
	return (1);
}
