/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pc_scan_steps.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <vnoon@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 12:54:38 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/24 15:40:30 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pc_elem.h"
#include "op.h"
#include "vm.h"

void		step_in_operation(t_pc_elem *process)
{
	unsigned char	pcode;

	if (process->step_in == 0)
	{
		pcode = *((unsigned char*)process->adr_crnt_pros);
		process->op_code = pcode;
		pcode = pcode == 0 || pcode > 16 ? 16 : pcode - 1;
		process->instruct_cycle = g_op_tab[pcode].nb_cycles;
		process->step_in = 1;
	}
}
