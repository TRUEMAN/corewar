/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decompile.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 19:00:39 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/20 13:11:00 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "disasm.h"
#include <libft.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

static int	load_header(int fd, t_dec_champion *champ)
{
	size_t	hdr_pos;
	size_t	hdr_size;
	int		ret;

	hdr_size = sizeof(*champ->header);
	champ->header = ft_xmalloc(hdr_size);
	hdr_pos = 0;
	ret = 1;
	while (ret > 0 && hdr_pos < hdr_size)
		if ((ret = read(fd, champ->header + hdr_pos, hdr_size - hdr_pos)) > 0)
			hdr_pos += ret;
	if (ret != -1 && hdr_pos == hdr_size)
	{
		reverse_bytes(&(champ->header->magic), sizeof(champ->header->magic));
		reverse_bytes(&(champ->header->prog_size),
				sizeof(champ->header->prog_size));
		if (champ->header->magic == COREWAR_EXEC_MAGIC)
			return (1);
		ft_memdel((void **)&(champ->header));
		return (-3);
	}
	ft_memdel((void **)&(champ->header));
	return (ret == -1 ? -1 : -2);
}

static int	load_code(int fd, t_dec_champion *champ)
{
	size_t	pos;
	int		ret;

	champ->code = create_memory(champ->header->prog_size);
	pos = 0;
	ret = 1;
	while (ret > 0 && pos < champ->header->prog_size)
	{
		ret = read(fd, champ->code->mem + pos, champ->header->prog_size - pos);
		if (ret > 0)
			pos += ret;
	}
	ret = read(fd, &pos, sizeof(pos));
	if (ret != -1 && pos == champ->header->prog_size)
		return (1);
	destroy_memory(&(champ->code));
	return (ret == -1 ? -1 : -4);
}

int			load_operations(t_dec_champion *champ)
{
	size_t		offset;
	t_dec_op	*op;
	t_dec_op	*prev;

	champ->operations = NULL;
	offset = 0;
	prev = NULL;
	while (offset < champ->header->prog_size)
	{
		if (!(op = read_operation(champ->code, offset)))
			break ;
		if (!champ->operations)
			champ->operations = op;
		offset += op->size;
		if (prev)
		{
			prev->next = op;
			op->prev = prev;
		}
		prev = op;
	}
	return (offset == champ->header->prog_size ? 1 : -5);
}

int			decompile_file(char *name)
{
	int				fd;
	int				ret;
	t_dec_champion	*champ;

	if ((fd = open(name, O_RDONLY)) == -1)
		put_error(-1);
	champ = ft_xmalloc(sizeof(*champ));
	champ->header = NULL;
	champ->code = NULL;
	if ((ret = load_header(fd, champ)) == 1)
		if ((ret = load_code(fd, champ)) == 1)
			ret = load_operations(champ);
	if (ret == 1)
		put_source(champ);
	else
		put_error(ret);
	if (champ->code)
		destroy_memory(&(champ->code));
	if (champ->header)
		free(champ->header);
	free(champ);
	close(fd);
	return (1);
}

/*
** Error codes:
** -1: unable to read file
** -2: bad header (bad size)
** -3: bad header (bad magic)
** -4: code size doesn't match size specified in header
** -5: instruction error (bad opcode)
*/
