/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 19:01:38 by jandre-j          #+#    #+#             */
/*   Updated: 2016/09/28 19:01:39 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void		reverse_bytes(void *mem, size_t size)
{
	size_t	pos;
	size_t	max;
	char	tmp;

	max = size - 1;
	pos = 0;
	while (pos < size / 2)
	{
		tmp = ((char *)mem)[pos];
		((char *)mem)[pos] = ((char *)mem)[max - pos];
		((char *)mem)[max - pos] = tmp;
		pos++;
	}
}
