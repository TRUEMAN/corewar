/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   param_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/30 10:34:16 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/20 13:10:44 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "disasm.h"

size_t	get_param_size(t_dec_op *op, size_t index)
{
	if (op->bits[index] == 1)
		return (1);
	if (op->op->has_idx || op->bits[index] == 3)
		return (2);
	return (4);
}

int32_t	get_param_as_int(t_dec_op *op, size_t index)
{
	if (op->bits[index] == REG_CODE)
		return ((int32_t)((int8_t)op->params[index]));
	else if (op->bits[index] == IND_CODE || op->op->has_idx)
		return ((int32_t)((int16_t)op->params[index]));
	return ((int32_t)op->params[index]);
}
