/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memory.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 19:01:41 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/20 13:10:52 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "disasm.h"
#include <libft.h>
#include <stdlib.h>

t_memory	*create_memory(size_t size)
{
	t_memory	*obj;

	obj = ft_xmalloc(sizeof(*obj));
	obj->mem = ft_xmalloc(sizeof(*(obj->mem)) * size);
	obj->size = size;
	return (obj);
}

void		destroy_memory(t_memory **obj)
{
	free((*obj)->mem);
	free(*obj);
	*obj = NULL;
}
