/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_operation.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 19:00:52 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/20 13:10:40 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "disasm.h"
#include <string.h>
#include <libft.h>

/*
** Reads parameter 'i' from a given memory address and a given bitmask.
*/

static int		read_param(t_memory *mem, size_t *addr, t_dec_op *op, size_t i)
{
	size_t	size;

	size = get_param_size(op, i);
	if (*addr + size > mem->size ||
			(op->bits[i] == 1 && !(op->op->param_types[i] & 0b001)) ||
			(op->bits[i] == 2 && !(op->op->param_types[i] & 0b010)) ||
			(op->bits[i] == 3 && !(op->op->param_types[i] & 0b100)))
		return (0);
	if (op->bits[i] == 1)
		op->params[i] = *(uint8_t *)(mem->mem + *addr);
	else if (op->bits[i] == 3 || op->op->has_idx)
		op->params[i] = *(uint16_t *)(mem->mem + *addr);
	else
		op->params[i] = *(uint32_t *)(mem->mem + *addr);
	if (op->bits[i] == T_REG && (op->params[i] < 1 ||
			op->params[i] > REG_NUMBER))
		return (0);
	*addr += size;
	op->size += size;
	reverse_bytes(op->params + i, size);
	return (1);
}

/*
** Reads an operation's bitmask from a memory address.
*/

static int		read_bitmask(t_dec_op *op, t_memory *mem, size_t addr)
{
	size_t	iter;

	op->size += 1;
	if (++addr >= mem->size)
		return (0);
	op->bitmask = ((uint8_t *)mem->mem)[addr++];
	op->bits[0] = (op->bitmask & 0b11000000) >> 6;
	op->bits[1] = (op->bitmask & 0b00110000) >> 4;
	op->bits[2] = (op->bitmask & 0b00001100) >> 2;
	op->bits[3] = (op->bitmask & 0b00000011);
	iter = 0;
	while (iter < 4 && op->bits[iter] != 0)
		if (!read_param(mem, &addr, op, iter++))
			return (-1);
	if (iter != (size_t)op->op->nb_params)
		return (-1);
	return (addr > mem->size ? 0 : 1);
}

/*
** Reads the unique parameter of a bitmask-less operation.
*/

static int		read_shortop(t_dec_op *op, t_memory *mem, size_t addr)
{
	size_t	size;

	size = op->op->has_idx ? 2 : 4;
	op->size += size;
	if (addr + size > mem->size)
		return (0);
	if (size == 2)
		op->params[0] = *(uint16_t *)(mem->mem + addr + 1);
	else
		op->params[0] = *(uint32_t *)(mem->mem + addr + 1);
	reverse_bytes(op->params, size);
	return (1);
}

/*
** Returns an operation (t_dec_op) from a given a memory address.
*/

static t_dec_op	*read_opcode(t_memory *mem, size_t addr)
{
	t_dec_op	*op;
	size_t		iter;

	iter = 0;
	while (g_op_tab[iter].name &&
			g_op_tab[iter].opcode != ((uint8_t *)mem->mem)[addr])
		iter++;
	if (g_op_tab[iter].opcode == 0 ||
			g_op_tab[iter].opcode != ((uint8_t *)mem->mem)[addr])
		return (NULL);
	op = ft_xmemalloc(sizeof(*op));
	op->op = g_op_tab + iter;
	op->opcode = op->op->opcode;
	op->size = 1;
	op->addr = addr;
	return (op);
}

/*
** Reads and returns an operation from a memory address.
*/

t_dec_op		*read_operation(t_memory *mem, size_t addr)
{
	t_dec_op	*op;

	if (addr >= mem->size)
		return (NULL);
	if (!(op = read_opcode(mem, addr)))
		return (NULL);
	if (op->op->has_pcode)
	{
		if (read_bitmask(op, mem, addr) != 1)
			ft_memdel((void **)&op);
	}
	else
	{
		if (!read_shortop(op, mem, addr))
			ft_memdel((void **)&op);
	}
	return (op);
}
