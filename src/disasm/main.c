/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 12:45:15 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/25 16:57:08 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "disasm.h"
#include <libft.h>

int	main(int argc, char **argv)
{
	if (argc != 2)
	{
		ft_putendl("Usage: disasm <filename>");
		return (0);
	}
	decompile_file(argv[1]);
	return (0);
}
