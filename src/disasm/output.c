/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 15:08:06 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/25 16:56:31 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "disasm.h"
#include <libft.h>
#include <stdlib.h>

void		put_error(int error_code)
{
	if (error_code == -1)
		ft_putendl_fd("Error: Unable to read file", 2);
	if (error_code == -2)
		ft_putendl_fd("Error: Invalid header (bad size)", 2);
	if (error_code == -3)
		ft_putendl_fd("Error: Invalid header (bad magic)", 2);
	if (error_code == -4)
		ft_putendl_fd(
				"Error: Code size does not match size specified in header", 2);
	if (error_code == -5)
		ft_putendl_fd("Error: Bad instruction", 2);
	exit(0);
}

static int	put_operation_name(t_dec_op *op)
{
	if (op->label)
	{
		ft_putstr("label");
		ft_putnbr(op->label);
		ft_putchar(LABEL_CHAR);
		ft_putchar('\n');
	}
	ft_putchar('\t');
	ft_putstr(op->op->name);
	ft_putchar('\t');
	return (-1);
}

static void	put_label(int id)
{
	ft_putchar(LABEL_CHAR);
	ft_putstr("label");
	ft_putnbr(id);
}

static void	put_operation(t_dec_op *op)
{
	int	i;

	i = put_operation_name(op);
	if (!op->op->has_pcode)
	{
		ft_putchar('%');
		if (op->labels[0])
			put_label(op->labels[0]->label);
		else
			ft_putnbr((int)get_param_as_int(op, 0));
	}
	else
		while (i + 1 < 4 && op->bits[i + 1] != 0)
		{
			if (++i > 0)
				ft_putstr(", ");
			if (op->bits[i] == REG_CODE)
				ft_putchar('r');
			else if (op->bits[i] == DIR_CODE)
				ft_putchar('%');
			if (op->labels[i])
				put_label(op->labels[i]->label);
			else
				ft_putnbr((int)get_param_as_int(op, i));
		}
}

void		put_source(t_dec_champion *champ)
{
	t_dec_op	*op;
	t_dec_op	*tmp;

	resolve_labels(champ);
	set_label_ids(champ);
	ft_putstr(NAME_CMD_STRING);
	ft_putstr(" \"");
	ft_putstr(champ->header->prog_name);
	ft_putendl("\"");
	ft_putstr(COMMENT_CMD_STRING);
	ft_putstr(" \"");
	ft_putstr(champ->header->comment);
	ft_putendl("\"\n");
	op = champ->operations;
	while (op)
	{
		put_operation(op);
		ft_putchar('\n');
		tmp = op;
		op = op->next;
		free(tmp);
	}
}
