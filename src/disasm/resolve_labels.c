/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve_labels.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/30 10:34:24 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/20 13:10:34 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "disasm.h"

static void	find_label_up(t_dec_op *op, size_t index, size_t addr)
{
	t_dec_op	*cur;

	cur = op->next;
	while (cur && cur->addr <= addr)
	{
		if (cur->addr == addr)
		{
			cur->label = 1;
			op->labels[index] = cur;
			return ;
		}
		cur = cur->next;
	}
}

static void	find_label_down(t_dec_op *op, size_t index, size_t addr)
{
	t_dec_op	*cur;

	cur = op->prev;
	while (cur && cur->addr >= addr)
	{
		if (cur->addr == addr)
		{
			cur->label = 1;
			op->labels[index] = cur;
			return ;
		}
		cur = cur->prev;
	}
}

void		find_label(t_dec_op *op, size_t index, size_t addr)
{
	if (addr >= op->addr && addr < op->addr + op->size)
		return ;
	if (addr > op->addr)
		find_label_up(op, index, addr);
	else
		find_label_down(op, index, addr);
}

void		resolve_labels(t_dec_champion *champ)
{
	t_dec_op	*op;
	size_t		iter;

	op = champ->operations;
	while (op)
	{
		if (op->op->has_pcode)
		{
			iter = 0;
			while (iter < 4 && op->bits[iter] != 0)
			{
				if (op->bits[iter] != REG_CODE)
					find_label(op, iter, (size_t)(op->addr +
							get_param_as_int(op, iter)));
				iter++;
			}
		}
		else
			find_label(op, 0, (size_t)(op->addr + get_param_as_int(op, 0)));
		op = op->next;
	}
}

void		set_label_ids(t_dec_champion *champ)
{
	t_dec_op	*op;
	size_t		count;

	op = champ->operations;
	count = 1;
	while (op)
	{
		if (op->label)
			op->label = count++;
		op = op->next;
	}
}
