/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 14:36:33 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 17:40:51 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include "visual.h"
#include "events.h"
#include "pc_scan_steps.h"
#include "workloop_steps.h"
#include "flags.h"

static void	init_visu(t_vm *vm)
{
	if (vm->args.enable_visualiser == 1)
	{
		vm->visual = initialize_visu(vm);
		on_cycle_delta_changed(vm->visual, vm->end_of_cycle);
	}
	else
		vm->paused = 0;
}

static void	print_winner(t_args args, int winner, t_vm vm)
{
	int	i;

	i = 1;
	while (*(args.champions) && *(args.champions_id) != winner)
	{
		i++;
		args.champions = args.champions + 1;
		args.champions_id = args.champions_id + 1;
	}
	if (i <= args.nb_player)
		ft_printf("Contestant %d, \"%s\", has won !\n",
				i, (vm.champions[i - 1]->header->prog_name));
}

static void	end_of_fight(t_vm *vm)
{
	if (vm->nb_cycles < vm->args.dump_cycle)
		dump_ram(vm->ram, vm);
	print_winner(vm->args, vm->winner, *vm);
	if (vm->args.enable_visualiser == 1 && vm->visual && vm->visual->sys)
		vm->visual->sounds[WIN_SOUND] = 1;
	while (vm->args.enable_visualiser == 1 && !vm->quit)
		vm->quit = !visual_loop(vm->visual);
	if (vm->args.enable_visualiser == 1)
		terminate_visu(vm->visual);
}

int			main(int argc, char **argv)
{
	t_vm		vm;

	if (init_vm(&vm, argc, argv) != 0)
		return (-1);
	starting_message(&vm);
	init_visu(&vm);
	while (vm.pc != NULL)
	{
		while (1)
		{
			if (vm.end_of_cycle <= 0)
				clean_post_end_of_loop(&vm);
			if ((vm.pc != NULL) && ((vm.quit = cycle_loop(&vm)) == 1))
				break ;
			if (vm.end_of_cycle_base_val <= 0 || vm.pc == NULL)
				break ;
		}
		if ((vm.quit))
			break ;
	}
	end_of_fight(&vm);
	return (0);
}
