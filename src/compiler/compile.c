/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compile.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 13:03:52 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/25 16:35:06 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"
#include <stdlib.h>
#include <get_next_line.h>

int			read_file(int fd, t_prog *p)
{
	int		size;
	int		cont;
	char	*line;

	while ((size = get_next_line(fd, &line)) > 0)
	{
		p->line++;
		if (!(cont = valid_line(line, p)))
			return (0);
		free(line);
		if (cont == 2)
			continue ;
	}
	if (line)
		free(line);
	return (1);
}

int			execute_labels(t_file *in, t_prog *p)
{
	t_lbl		*tmp;
	int			i;

	while (in)
	{
		if (in->has_lbl && (i = -1))
		{
			while (++i < in->num_par)
			{
				if ((tmp = p->lbls) && !in->par[i].lbl)
					continue ;
				while (tmp && tmp->name)
				{
					if (insert_label(tmp, in, i))
						break ;
					tmp = tmp->next;
				}
				if (!tmp->name)
					return (err_arg(in->line_num, "referring imaginary label"));
			}
		}
		p->prog_size += in->in_siz;
		in = in->next;
	}
	return (1);
}

int			code_instruction(t_file *ins)
{
	int		in_type;
	int		i;
	int		j;
	int		shift;
	int		off;

	off = 1;
	in_type = ins->in_type;
	ft_memcpy(ins->code, &(g_op_tab[in_type].opcode), 1);
	if (g_op_tab[in_type].has_pcode && ++off)
		ft_memcpy(ins->code + 1, &(ins->pcode), 1);
	i = -1;
	while (++i < MAX_ARGS_NUMBER)
	{
		j = 0;
		while (j++ < ins->par[i].size)
		{
			shift = 8 * (ins->par[i].size - j);
			*(ins->code + off) = (ins->par[i].value >> shift) & 255;
			off++;
		}
	}
	return (1);
}

void		clean(t_file *ins, t_prog *p)
{
	while (ins)
	{
		if (ins->num_par && !code_instruction(ins))
			error_out("bad labels");
		ins = del_ins(ins);
	}
	p->curr_lbl = p->lbls;
	while (p->curr_lbl)
		p->curr_lbl = del_label(p->curr_lbl);
	free(p->title);
	free(p->desc);
}

/*
** This is a good comment
*/

int			compile_file(int fd, char *oname, int out)
{
	t_file	*instructions;
	t_prog	pars;

	if (!(instructions = make_new_ins()))
		return (error_out("Memory shortage"));
	if (!init_pars(&pars, instructions))
		return (error_out("Unreadable file"));
	pars.out = out;
	if (!read_file(fd, &pars) || !instructions->line)
		return (error_out("Invalid instructions"));
	if (!pars.title || !pars.desc)
		return (error_out("Wrong header"));
	if (!execute_labels(instructions, &pars))
		return (error_out("Unreadable file"));
	if ((fd = open(oname, O_WRONLY | O_CREAT | O_TRUNC, 0644)) <= -1)
		return (error_out("Unwritable file"));
	print_head(&pars, fd);
	print_instructions(instructions, fd, &pars);
	close(fd);
	fd = 0;
	clean(instructions, &pars);
	if (!(out & AFF))
		ft_printf("Writing output program to %s\n", oname);
	return (1);
}
