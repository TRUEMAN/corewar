/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_head.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 12:16:51 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 15:34:38 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
** This ouptuts the character where the header command starts
** If no '"' char is detected it outputs an error
** THIS ERROR IS TO FINISH WITH LINE NUMBER (XXX) AND CHAR NUMBER (YYY)
*/

int			head_start(char *line, t_prog *p)
{
	int		i;

	i = 0;
	while (ft_isspace(line[i]))
		i++;
	if (!ft_strncmp(line + i, NAME_CMD_STRING, p->s_tit) && (p->is_tit = 1))
		i += p->s_tit;
	if (!ft_strncmp(line + i, COMMENT_CMD_STRING, p->s_des) && (p->is_des = 1))
		i += p->s_des;
	while (line[i] && line[i] != '"')
		i++;
	if (!line[i] || (line[i] && line[i] != '"'))
		err_arg(p->line, "expecting opening double quote");
	return (i + 1);
}

/*
** This HAS to be modified to store in variables in p the title and description
*/

int			add_to_head(t_prog *p, char *line, int st, int end)
{
	char	*prev;
	char	**head;
	int		len;

	prev = (p->is_tit ? p->title : p->desc);
	len = (prev ? ft_strlen(prev) : 0) + end - st + 1;
	if (p->is_tit && len > PROG_NAME_LENGTH + 1)
		return (error_out("program name is too long"));
	if (!p->is_tit && len > COMMENT_LENGTH + 1)
		return (error_out("comment is too long"));
	head = (p->is_tit ? &(p->title) : &(p->desc));
	if (!(*head = ft_strnew(len)))
		return (error_out("Malloc failed"));
	if (prev)
		ft_strcat(*head, prev);
	if (prev)
		free(prev);
	ft_strncat(*head, line + st, end - st);
	if (!line[end])
		ft_strcat(*head, "\n");
	return (1);
}

/*
** This checks that the end of line is correct : no commands other than comments
*/

void		check_end_line(char *line, int end, t_prog *p)
{
	while (ft_isspace(line[++end]))
		;
	if (line[end] && line[end] != COMMENT_CHAR && line[end] != COMMENT_CHAR_2)
		err_arg(p->line, "Non parsable line after double quote");
}

int			add_header(char *line, t_prog *p)
{
	int		st;
	int		end;

	st = (!p->is_tit && !p->is_des) ? head_start(line, p) : 0;
	end = st;
	while (line[end] && line[end] != '"')
		end++;
	add_to_head(p, line, st, end);
	if (line[end])
	{
		check_end_line(line, end, p);
		if (p->is_tit && (p->tit_ok = 1))
			p->is_tit = 0;
		if (p->is_des && (p->des_ok = 1))
			p->is_des = 0;
	}
	return (2);
}
