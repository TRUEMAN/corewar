/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_ins.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 17:00:40 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/21 14:45:53 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void		update_instruction(t_file *ins, int num, t_arg *arg)
{
	ins->par[num].type = arg->type;
	ins->par[num].value = arg->value;
	ins->par[num].offset = arg->offset;
	if ((arg->type & T_LAB))
		ins->has_lbl = 1;
	if (!g_op_tab[ins->in_type].has_pcode)
		return ;
	if ((arg->type & T_REG))
		ins->pcode += REG_CODE << 2 * (MAX_ARGS_NUMBER - num - 1);
	if ((arg->type & T_IND))
		ins->pcode += IND_CODE << 2 * (MAX_ARGS_NUMBER - num - 1);
	if ((arg->type & T_DIR))
		ins->pcode += DIR_CODE << 2 * (MAX_ARGS_NUMBER - num - 1);
}

int			calc_size(t_file *ins)
{
	int		i;
	int		add;

	i = -1;
	ins->in_siz = 1 + g_op_tab[ins->in_type].has_pcode;
	while (++i < ins->num_par)
	{
		if ((ins->par[i].type & T_REG))
			add = 1;
		else if ((ins->par[i].type & T_IND) || (g_op_tab[ins->in_type].has_idx))
			add = 2;
		else if ((ins->par[i].type & T_DIR))
			add = 4;
		else
			return (0);
		ins->par[i].size = add;
		ins->in_siz += add;
	}
	ins->code = (uint8_t *)ft_strnew(ins->in_siz);
	return (1);
}

int			read_ins(char *line, int op, t_prog *p)
{
	char	**args;
	char	*arg;
	int		i;

	args = (char **)ft_strchr(line, COMMENT_CHAR_2);
	if (!(arg = ft_strchr(line, COMMENT_CHAR)) || (args && (char *)args < arg))
		arg = (char *)args;
	if (!(arg = ft_strsub(line, 0, (arg ? arg - line : ft_strlen(line)))))
		return (0);
	if (!(args = ft_strsplit(arg, SEPARATOR_CHAR)))
		return (0);
	if (!(i = -good_arg_nb(p, arg, op)))
		return (err_arg(p->line, (arg ? arg : "invalid argument number")));
	free(arg);
	while (args[++i])
	{
		if (!(arg = clean_arg(args[i])) ||
				(i < g_op_tab[op].nb_params && !is_valid_arg(arg, i, op, p)))
			return (err_arg(p->line, (arg ? arg : "invalid argument")));
		free(arg);
		free(args[i]);
	}
	free(args);
	return ((i < g_op_tab[op].nb_params || !calc_size(p->ins)) ?
			(err_arg(p->line, "invalid argument number")) : 1);
}

void		print_ins(t_file *ins)
{
	int		i;

	ft_printf("Instruction %s (%dp) :",
			g_op_tab[ins->in_type].name, ins->num_par);
	ft_printf(" %d Bytes\ncode : ", ins->in_siz);
	i = -1;
	while (++i < ins->in_siz)
		ft_printf("%.2X ", (int)(ins->code[i]));
	ft_putstr("\n");
	i = -1;
	while (++i < ins->num_par && ft_printf(" arg %d : type is ", i + 1))
	{
		if ((ins->par[i].type & T_DIR))
			ft_putstr("direct");
		if ((ins->par[i].type & T_IND))
			ft_putstr("indirect");
		if ((ins->par[i].type & T_REG))
			ft_putstr("registry");
		if ((ins->par[i].type & T_LAB))
			ft_putstr("&label");
		if ((ins->par[i].type & T_LAB))
			ft_printf("\n\t label is %s", ins->par[i].lbl);
		ft_printf("\n\t value is %d\n", ins->par[i].value);
	}
	return ;
}

int			is_ins(char *line, int lbl, t_prog *p)
{
	int		i;
	int		j;
	size_t	len;

	i = (lbl ? lbl + 1 : lbl);
	while (ft_isspace(line[i]))
		i++;
	j = -1;
	len = 0;
	while (!ft_isspace(line[i + len]))
		len++;
	while (g_op_tab[++j].name && ft_strncmp(g_op_tab[j].name, line + i, len))
		;
	p->ins->in_type = j;
	if (!g_op_tab[j].name || !(i = read_ins(line + i + len + 1, j, p)))
		return (-i);
	p->is_lbl = 0;
	if (!(p->ins->next = make_new_ins()))
		return (0);
	p->ins->line = ft_strdup(line);
	p->ins->line_num = p->line;
	p->ins->next->in_pos = p->ins->in_pos + p->ins->in_siz;
	p->ins = p->ins->next;
	return (1);
}
