/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_init.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 13:38:44 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 12:45:24 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

t_file		*cpy_ins(t_file *dest, t_file *src)
{
	int		i;

	if ((i = -1) && !src)
		return (NULL);
	if (!dest && !(dest = make_new_ins()))
		return (NULL);
	if (src->line && !(dest->line = ft_strdup(src->line)))
		return (NULL);
	if (src->code && !(dest->code = ft_memdup(src->code, (size_t)src->in_siz)))
		return (NULL);
	dest->in_siz = src->in_siz;
	dest->in_pos = src->in_pos;
	dest->in_type = src->in_type;
	dest->has_lbl = src->has_lbl;
	dest->num_par = src->num_par;
	while (++i < MAX_ARGS_NUMBER)
	{
		dest->par[i].type = src->par[i].type;
		dest->par[i].value = src->par[i].value;
		dest->par[i].offset = src->par[i].offset;
		dest->par[i].size = src->par[i].size;
		if (src->par[i].lbl && !(dest->par[i].lbl = ft_strdup(src->par[i].lbl)))
			return (NULL);
	}
	return (dest);
}

t_file		*del_ins(t_file *ins)
{
	t_file	*tmp;
	int		i;

	tmp = ins;
	if (tmp->line)
		free(tmp->line);
	i = -1;
	while (++i < MAX_ARGS_NUMBER)
		if (tmp->par[i].lbl)
			free(tmp->par[i].lbl);
	if (tmp->code)
		free(tmp->code);
	tmp = tmp->next;
	free(ins);
	return (tmp);
}

t_file		*make_new_ins(void)
{
	t_file	*new;
	int		i;

	if (!(new = malloc(sizeof(t_file))))
		return (NULL);
	new->next = NULL;
	new->line = NULL;
	new->in_type = -1;
	new->in_siz = 0;
	new->in_pos = 0;
	new->has_lbl = 0;
	new->num_par = 0;
	new->pcode = 0;
	new->code = NULL;
	i = -1;
	while (++i < MAX_ARGS_NUMBER)
	{
		new->par[i].type = 0;
		new->par[i].value = 0;
		new->par[i].offset = 0;
		new->par[i].size = 0;
		new->par[i].lbl = NULL;
	}
	return (new);
}

int			init_pars(t_prog *p, t_file *ins)
{
	p->is_des = 0;
	p->line = 0;
	p->is_tit = 0;
	p->tit_ok = 0;
	p->des_ok = 0;
	p->s_des = ft_strlen(COMMENT_CMD_STRING);
	p->s_tit = ft_strlen(NAME_CMD_STRING);
	p->title = NULL;
	p->desc = NULL;
	p->prog_size = 0;
	p->ins = ins;
	if (!(p->lbls = new_label()))
		return (0);
	p->curr_lbl = p->lbls;
	p->is_lbl = 0;
	return (1);
}
