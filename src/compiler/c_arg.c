/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_arg.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/19 12:02:22 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/21 14:38:14 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int			read_reg(char *arg, int limit)
{
	int		res;

	while (*arg == '0')
		arg++;
	res = 0;
	while (*arg >= '0' && *arg <= '9' && res <= limit)
	{
		res = res * 10 + (*arg - '0');
		arg++;
	}
	if (res <= 0 || res > limit || (*arg && !ft_isspace(*arg)))
		res = 0;
	return (res);
}

char		*lbl_copy(char *str, t_arg *arg)
{
	char	*lbl;
	size_t	len;
	int		sign;

	len = 0;
	while (str[len] && ft_strchr(LABEL_CHARS, str[len]))
		len++;
	if (!(lbl = (char *)ft_memdup(str, len + 1)))
		return (0);
	lbl[len] = 0;
	if (OFFSET_BONUS && str[len])
	{
		len += num_spaces(str + len);
		sign = (str[len++] == '+') ? 1 : -1;
		len += num_spaces(str + len);
		arg->offset = sign * read_num(str + len, is_num(str + len));
	}
	return (lbl);
}

int			arg_set(char *str, t_arg *arg, char **lbl)
{
	int		is;

	arg->type = 0;
	arg->value = 0;
	is = is_num(str);
	if (is && ((arg->type = T_IND) || 1))
		arg->value = read_num(str, is);
	else if (str[0] == 'r' && ((arg->type = T_REG) || 1))
		arg->value = read_reg(str + 1, REG_NUMBER);
	else if (str[0] == LABEL_CHAR && ((arg->type = T_LAB | T_IND) || 1))
		*lbl = lbl_copy(str + 1, arg);
	else if (str[0] == DIRECT_CHAR && ((arg->type = T_DIR) || 1))
	{
		if (str[1] == LABEL_CHAR && ((arg->type = arg->type | T_LAB) || 1))
			*lbl = lbl_copy(str + 2, arg);
		else if ((is = is_num(str + 1)))
			arg->value = read_num(str + 1, is);
		else
			return (0);
	}
	else
		return (0);
	if (arg->type == T_REG && arg->value == 0)
		return (0);
	return (((arg->type & T_LAB) && !*lbl) ? 0 : 1);
}

/*
** Checks if an argument is valid or not.
*/

int			is_valid_arg(char *arg, int num, int op, t_prog *p)
{
	int		end;
	t_arg	tmp_arg;
	char	**lbl;

	tmp_arg.offset = 0;
	lbl = &(p->ins->par[num].lbl);
	if (*lbl)
	{
		free(*lbl);
		*lbl = NULL;
	}
	while (ft_isspace(*arg))
		arg++;
	end = ft_strlen(g_op_tab[op].name);
	if (!arg_set(arg, &tmp_arg, lbl))
		return (0);
	if (end == 0 || !(g_op_tab[op].param_types[num] & tmp_arg.type))
		return (0);
	if ((tmp_arg.type & T_LAB) && !*lbl)
		return (0);
	update_instruction(p->ins, num, &tmp_arg);
	return (1);
}

/*
** This returns the argument trimmed to the first word
*/

char		*clean_arg(char *arg)
{
	int		len;

	len = 0;
	while (ft_isspace(*arg))
		arg++;
	if ((len = len_reg(arg)) < 0)
		return (NULL);
	if ((len += len_label(arg)) < 0)
		return (NULL);
	if ((len += len_direct(arg)) < 0)
		return (NULL);
	if ((len += len_indirect(arg)) < 0)
		return (NULL);
	if (len == 0)
		return (NULL);
	return (ft_strsub(arg, 0, len));
}
