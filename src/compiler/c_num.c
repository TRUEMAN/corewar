/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_num.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 09:38:12 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/19 18:58:38 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
** This functions are used to read numbers, decimal or hex
*/

int		hex_value(char c)
{
	if (c >= '0' && c <= '9')
		return (c - '0');
	if (c >= 'A' && c <= 'F')
		return (c + 10 - 'A');
	return (-1);
}

int		is_num(char *str)
{
	int		i;

	if (!*str)
		return (0);
	if (HEX_BONUS && str[0] == '0' && str[1] == 'x' && (i = 2))
	{
		while (hex_value(str[i]) >= 0)
			i++;
		return ((!str[i] || ft_isspace(str[i])) ? HEX_TYPE : 0);
	}
	i = (str[0] == '-' ? 1 : 0);
	while (ft_isdigit(str[i]))
		i++;
	return ((!str[i] || ft_isspace(str[i])) ? INT_TYPE : 0);
}

int		strhextoi(char *str)
{
	long long	res;
	long long	mask;

	res = 0;
	while ((mask = hex_value(*str)) >= 0)
	{
		res = (res << 4) + mask;
		str++;
	}
	return ((int)res);
}

int		read_num(char *str, int type)
{
	if (type == INT_TYPE)
		return (ft_atoi(str));
	if (type == HEX_TYPE)
		return (strhextoi(str + 2));
	return (0);
}
