/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_clean.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 14:41:07 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/19 19:16:35 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		num_spaces(char *s)
{
	int		res;

	res = 0;
	while (ft_isspace(s[res]))
		res++;
	return (res);
}

int		len_reg(char *str)
{
	int		len;

	if (*str != 'r')
		return (0);
	len = 1;
	while (ft_isdigit(str[len]))
		len++;
	if (len == 1)
		return (-1);
	str += len;
	str += num_spaces(str);
	return (*str ? 0 : len);
}

int		len_indirect(char *str)
{
	int		len;

	len = 0;
	if (!is_num(str))
		return (0);
	while (str[len] && !ft_isspace(str[len]))
		len++;
	str += len;
	str += num_spaces(str);
	return (*str ? 0 : len);
}

int		len_label(char *str)
{
	int		len;
	int		len2;

	if (*str != ':')
		return (0);
	len = 1;
	while (str[len] && ft_strchr(LABEL_CHARS, str[len]))
		len++;
	if (len == 1)
		return (-1);
	len2 = num_spaces(str + len);
	if (!OFFSET_BONUS || !str[len + len2])
		return (str[len + len2] ? 0 : len);
	if (ft_strchr("+-", str[len + len2]) && (len += len2 + 1))
	{
		len += num_spaces(str + len);
		if (!is_num(str + len))
			return (-1);
		while (str[len] && !(ft_isspace(str[len])))
			len++;
	}
	str += len;
	str += num_spaces(str);
	return (*str ? 0 : len);
}

int		len_direct(char *str)
{
	int		len;

	if (*str != '%')
		return (0);
	if ((len = len_label(str + 1)) < 0)
		return (-1);
	if (len == 0)
		len = len_indirect(str + 1);
	if (len == 0)
		return (-1);
	return (len + 1);
}
