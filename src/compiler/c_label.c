/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_label.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/17 14:28:19 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 12:45:35 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

t_lbl		*del_label(t_lbl *lbl)
{
	t_lbl	*tmp;

	if (!lbl)
		return (NULL);
	tmp = lbl->next;
	if (lbl->name)
		free(lbl->name);
	free(lbl);
	return (tmp);
}

t_lbl		*new_label(void)
{
	t_lbl	*new;

	if (!(new = malloc(sizeof(t_lbl))))
		return (NULL);
	new->next = NULL;
	new->name = NULL;
	new->ins = NULL;
	return (new);
}

int			add_label(t_lbl *lbl, char *line, t_lbl *lbls)
{
	size_t	len;

	while (ft_isspace(*line))
		line++;
	len = 0;
	while (line[len] != LABEL_CHAR)
		len++;
	if (len == 0)
		return (error_out("Inexistant label"));
	if (!(lbl->name = ft_memdup(line, len + 1)))
		return (error_out("Memory shortage"));
	lbl->name[len] = 0;
	while (lbls != lbl)
	{
		if (!ft_strcmp(lbls->name, lbl->name))
			return (-len);
		lbls = lbls->next;
	}
	if (!(lbl->next = new_label()))
		return (error_out("Memory shortage"));
	return (1);
}

int			insert_label(t_lbl *lbl, t_file *in, int argn)
{
	uint32_t	delta;
	t_arg		*arg;

	if (ft_strcmp(lbl->name, in->par[argn].lbl))
		return (0);
	delta = lbl->ins->in_pos - in->in_pos;
	if (OFFSET_BONUS)
		delta += in->par[argn].offset;
	arg = &(in->par[argn]);
	if (arg->size == 1)
		arg->value = (unsigned char)delta;
	if (arg->size == 2)
		arg->value = (unsigned short)delta;
	if (arg->size == 4)
		arg->value = (unsigned int)delta;
	return (1);
}

/*
** checks if the line starts with a label
** returns the position of the character following LABEL_CHAR
** or 0 if no label is found
*/

int			starts_with_label(char *line, t_prog *p)
{
	int		i;

	i = 0;
	while (ft_isspace(line[i]))
		i++;
	while (ft_strchr(LABEL_CHARS, line[i]))
		i++;
	if (i == 0 && line[i] == LABEL_CHAR)
		return (error_out("Empty label"));
	if (line[i] != LABEL_CHAR)
		return (0);
	if (add_label(p->curr_lbl, line, p->lbls) < 0)
		return (error_out("Duplicate label"));
	p->curr_lbl->ins = p->ins;
	p->curr_lbl = p->curr_lbl->next;
	p->is_lbl = 1;
	return (i);
}
