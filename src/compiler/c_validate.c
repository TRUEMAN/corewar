/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_validate.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/30 10:32:39 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 15:30:46 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
** Checks if line is a comment or empty line
*/

int			is_useless_line(char *line, t_prog *p)
{
	int		i;

	if (p->is_tit || p->is_des)
		return (0);
	i = 0;
	while (ft_isspace(line[i]))
		i++;
	if (line[i] == COMMENT_CHAR || line[i] == COMMENT_CHAR_2)
		return (1);
	if (!line[i])
		return (1);
	return (0);
}

/*
** Checks if a line belongs to a title or a description
*/

int			is_head(char *line, t_prog *p)
{
	int		i;

	if (p->is_tit || p->is_des)
		return (1);
	i = 0;
	while (ft_isspace(line[i]))
		i++;
	if (!ft_strncmp(line + i, NAME_CMD_STRING, p->s_tit))
		return (!p->tit_ok ? 1 : -1);
	if (!ft_strncmp(line + i, COMMENT_CMD_STRING, p->s_des))
		return (!p->des_ok ? 1 : -1);
	return (0);
}

/*
** Checks if the number of arguments is right for the given instruction
*/

int			good_arg_nb(t_prog *p, char *args, int op)
{
	int		i;

	i = 1;
	while (*args && ++args)
		if (args[-1] == SEPARATOR_CHAR)
			++i;
	if (i != g_op_tab[op].nb_params)
		return (err_arg(p->line, "Bad argument number"));
	p->ins->num_par = g_op_tab[op].nb_params;
	return (1);
}

/*
** Scans a line for labels and instructions
*/

int			scan_instruct(char *line, t_prog *p)
{
	int		lbl;
	int		ret;

	lbl = starts_with_label(line, p);
	if (lbl < 0)
		return (lbl);
	if (is_useless_line(line + lbl + 1, p))
		return (2);
	if ((ret = is_ins(line, lbl, p)) <= 0)
		err_arg(p->line, "Unauthorized label character");
	return (1);
}

int			valid_line(char *line, t_prog *p)
{
	int		ret;

	if (is_useless_line(line, p))
		return (2);
	if ((ret = is_head(line, p)) < 0)
		return (0);
	if (ret == 1)
		return (add_header(line, p));
	if (!(p->tit_ok && p->des_ok))
		err_arg(p->line, "Expecting header before instructions");
	if ((ret = scan_instruct(line, p)) < 0)
		return (0);
	return (ret);
}
