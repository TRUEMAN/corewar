/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_print.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/21 10:31:25 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/20 12:45:39 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"
#include <unistd.h>

void			pretty_print_arg(t_file *in, int arg, t_prog *p)
{
	int		rel;
	t_lbl	*tmp;

	ft_putstr((in->par[arg].type & T_REG) ? "REG" : "");
	ft_putstr((in->par[arg].type & T_DIR) ? "DIR" : "");
	ft_putstr((in->par[arg].type & T_IND) ? "IND" : "");
	ft_putstr((in->par[arg].type & T_LAB) ? " & LBL" : "");
	ft_putstr("\033[0m\n");
	if ((in->par[arg].type & T_REG) && write(1, "\t  reg n. ", 10) > 0)
		ft_printf("%d\n", in->par[arg].value);
	else if (in->par[arg].type & T_LAB)
	{
		tmp = p->lbls;
		while (tmp && ft_strcmp(tmp->name, in->par[arg].lbl))
			tmp = tmp->next;
		rel = tmp->ins->in_pos - in->in_pos;
		ft_printf("\t  label -> %s (%0.4d", in->par[arg].lbl, tmp->ins->in_pos);
		ft_printf(")\n\t  rel : %d, val : %d\n", rel, in->par[arg].value);
	}
	if (in->par[arg].type & T_DIR)
		ft_printf("\t  value %d\n", in->par[arg].value);
	if (in->par[arg].type & T_IND)
		ft_printf("\t  value at PC + %d\n", in->par[arg].value);
}

void			pretty_print_instruction(t_file *in, t_prog *p)
{
	int		i;
	int		j;
	int		c;

	i = in->in_type;
	ft_printf("\e[36m%0.4d\e[0m : %.4s (", in->in_pos, g_op_tab[i].name);
	ft_printf("%d): %.2X %.2X ", in->in_siz, g_op_tab[i].opcode, in->pcode);
	i = g_op_tab[i].has_pcode;
	c = 0;
	j = 0;
	while (++i < in->in_siz)
	{
		if (j + in->par[c].size <= i - 1 - g_op_tab[in->in_type].has_pcode)
			j += in->par[c++].size;
		ft_printf("\033[%dm%.2X", 31 + c, in->code[i]);
	}
	ft_putstr("\033[0m\n");
	i = -1;
	while (++i < in->num_par)
	{
		ft_printf("\t\033[%dmarg %d \033[0m: ", 31 + i, i);
		pretty_print_arg(in, i, p);
	}
}

void			pretty_print_head(t_prog *p)
{
	ft_putstr("Program size : ");
	ft_putnbr(p->prog_size);
	ft_putstr(" Bytes\nName : \"");
	ft_putstr(p->title);
	ft_putstr("\"\nComment : \"");
	ft_putstr(p->desc);
	ft_putendl("\"\n");
}

void			print_instructions(t_file *ins, int fd, t_prog *p)
{
	p->curr_lbl = p->lbls;
	while (ins)
	{
		if (ins->num_par && !code_instruction(ins))
			error_out("bad labels");
		if (!p->out)
			write(fd, ins->code, ins->in_siz);
		else if (ins->line)
		{
			while (p->curr_lbl && p->curr_lbl->ins == ins)
			{
				ft_printf("\e[36m%0.4d\e[0m : ", p->curr_lbl->ins->in_pos);
				ft_putendl(p->curr_lbl->name);
				p->curr_lbl = p->curr_lbl->next;
			}
			pretty_print_instruction(ins, p);
		}
		ins = ins->next;
	}
}

void			print_head(t_prog *p, int fd)
{
	t_header		tmp;
	int				i;
	int				len;
	unsigned char	*addr;

	ft_bzero(&tmp, sizeof(t_header));
	len = (int)sizeof(tmp.magic);
	addr = (unsigned char *)&(tmp.magic);
	i = len;
	while (i--)
		addr[len - 1 - i] = (COREWAR_EXEC_MAGIC & (0xFF << (8 * i))) >> (8 * i);
	ft_memcpy(&(tmp.prog_name), p->title, ft_strlen(p->title));
	len = (int)sizeof(tmp.prog_size);
	addr = (unsigned char *)&(tmp.prog_size);
	i = len;
	while (i--)
		addr[len - 1 - i] = (p->prog_size & (0xFF << (8 * i))) >> (8 * i);
	ft_memcpy(&(tmp.comment), p->desc, ft_strlen(p->desc));
	if (!p->out)
		write(fd, &tmp, sizeof(t_header));
	else
		pretty_print_head(p);
}
