/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/26 17:04:35 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/21 13:24:29 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"
#include <get_next_line.h>
#include <libft.h>
#include <stdlib.h>

char	*outname(char *name)
{
	char	*new;
	size_t	len;
	size_t	len_init;
	size_t	ext_len;

	len_init = ft_strlen(name);
	len = len_init + 1;
	ext_len = ft_strlen(FILE_EXT);
	while (--len > 0 && name[len] != '.' && name[len] != '/')
		;
	if (name[len] == '/' || len == 0)
		len = len_init;
	if (!(new = ft_strnew(len + ext_len)))
		return (NULL);
	ft_memcpy(new, name, len);
	ft_memcpy(new + len, FILE_EXT, ext_len);
	return (new);
}

int		check_args(int argc, char **argv, int *opt)
{
	int		ret;

	*opt = 0;
	ret = 0;
	if (argc < 1)
		error_usage("asm");
	if (argc == 1)
		error_usage(argv[0]);
	while (ret + 1 < argc && argv[ret + 1][0] == '-')
	{
		if (0 == ft_strcmp(argv[ret + 1], "-a") && (*opt |= AFF))
			ret++;
		else if ((0 == ft_strcmp(argv[ret + 1], "--") && (++ret)))
			break ;
		else
		{
			ft_putstr("Unrecognized option : ");
			ft_putendl(argv[1]);
			error_usage(argv[0]);
		}
	}
	if (ret + 1 == argc)
		error_out("No filename given");
	return (ret);
}

int		main(int argc, char **argv)
{
	int		i;
	int		fd;
	int		off;
	int		opt;
	char	*oname;

	off = check_args(argc, argv, &opt);
	i = off;
	while (++i < argc)
	{
		if (!(oname = outname(argv[i])))
			error_out("Out of memory. Exiting cleanly.");
		if ((opt & AFF) && argc - off > 2)
			ft_printf("\n%s\n", argv[i]);
		else if (argc - off > 2)
			ft_printf("%s -> %s\n", argv[i], oname);
		if ((fd = open(argv[i], O_RDONLY)) <= -1)
			ft_printf("Unable to open file : %s\n", argv[i]);
		else
			compile_file(fd, oname, opt);
		close(fd);
		free(oname);
	}
	i = 0;
	return (0);
}
