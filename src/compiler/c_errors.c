/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_errors.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 14:41:52 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/21 12:02:01 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		err_arg(int line_num, char *arg)
{
	ft_printf("Error line %03d : %s\n", line_num, arg);
	exit(1);
}

int		error_out(char *s)
{
	ft_putendl(s);
	exit(1);
	return (1);
}

void	error_usage(char *name)
{
	ft_putstr("Usage : ");
	ft_putstr(name);
	ft_putendl(" [-a] <sourcefile1.s> [<sourcefile2.s> ...]");
	ft_putstr("\t-a : Instead of creating a .cor file,\n\toutputs a stripped");
	ft_putendl(" and annotated version\n\tof the code to the standard output");
	exit(1);
}
