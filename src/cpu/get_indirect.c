/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_indirect.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/05 15:07:19 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:13:24 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

/*
** get the adress of the first byte of an indirect parameter.
*/

void	*get_indirect_adr(int32_t ind_val, const t_pc_elem *op)
{
	void		*adr;
	int16_t		idx;

	idx = 0;
	idx |= ind_val;
	idx = idx % op->vm->memory_size;
	if (op->op_code != 0x0d && op->op_code != 0x0e)
		idx = idx % (op->vm->index_mod);
	adr = op->adr_crnt_pros;
	adr += idx;
	if (adr >= op->ram_orgn + op->vm->memory_size)
		adr -= op->vm->memory_size;
	else if (adr < op->ram_orgn)
		adr += op->vm->memory_size;
	return (adr);
}

/*
** get the value at the adress given in parameter and return it in BIG_INDIAN !
*/

int		get_indirect_val(const void *adr, const t_pc_elem *op)
{
	int		val;
	int		over_lap;
	int		diff;
	void	*cpy;

	cpy = 0;
	over_lap = adr - op->ram_orgn - op->vm->memory_size + (R_IND_SIZE(op));
	if (over_lap > 0)
	{
		diff = DIR_SIZE - over_lap;
		cpy = (void*)&val;
		ft_memcpy(cpy, adr, diff);
		ft_memcpy(cpy + diff, adr - op->vm->memory_size + diff, over_lap);
	}
	else
		ft_memcpy(&val, adr, R_IND_SIZE(op));
	return (val);
}
