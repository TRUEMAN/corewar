/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval_op.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 17:11:33 by vnoon             #+#    #+#             */
/*   Updated: 2016/10/11 17:01:20 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

/*
** Initiate the virtual CPU by giving to it the adresses of all the instructions
** the V_CPU can do.
*/

static int		cpu_init(void ***v_cpu)
{
	if ((*v_cpu = (void **)malloc(sizeof(void*) * 18)) == NULL)
		return (-1);
	(*v_cpu)[17] = NULL;
	(*v_cpu)[16] = &(aff);
	(*v_cpu)[15] = &(lfork);
	(*v_cpu)[14] = &(lldi);
	(*v_cpu)[13] = &(lld);
	(*v_cpu)[12] = &(sfork);
	(*v_cpu)[11] = &(sti);
	(*v_cpu)[10] = &(ldi);
	(*v_cpu)[9] = &(zjmp);
	(*v_cpu)[8] = &(comp_xor);
	(*v_cpu)[7] = &(comp_or);
	(*v_cpu)[6] = &(comp_and);
	(*v_cpu)[5] = &(sub);
	(*v_cpu)[4] = &(add);
	(*v_cpu)[3] = &(st);
	(*v_cpu)[2] = &(ld);
	(*v_cpu)[1] = &(live);
	(*v_cpu)[0] = &(zero);
	return (0);
}

/*
** Do the operation that the virtual CPU have selected.
*/

static int		do_op(void **v_cpu, int op_code, t_pc_elem *op)
{
	int	(*f_ptr)(void *);
	int	ret;

	if (op_code < 0 || op_code > 16)
		op_code = 0;
	f_ptr = v_cpu[op_code];
	ret = (f_ptr)((t_pc_elem *)op);
	return (ret);
}

/*
** Select the operation that the virtual CPU must do. It return -1 if a function
** failed, 0 if no error occured, and if a live instruction was made it return
** the number of the player.
*/

int				eval_op(t_pc_elem *op)
{
	static void		**v_cpu;
	int				ret;

	if (op == NULL)
		return (-1);
	ret = 0;
	if (v_cpu == NULL)
		if (cpu_init(&v_cpu) == -1)
			return (0);
	ret = do_op(v_cpu, op->op_code, op);
	return (ret);
}
