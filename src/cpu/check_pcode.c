/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_pcode.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 13:13:58 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 12:42:16 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"
#include "flags.h"

static int	check_type(const t_params *param, int i, int op_index)
{
	if (((g_op_tab[op_index].param_types[i] & T_REG) == T_REG) &&
		(param->p_type[i] == REG_CODE) &&
		(param->val[i] > 0 && param->val[i] <= REG_NUMBER))
		return (1);
	if (((g_op_tab[op_index].param_types[i] & T_DIR) == T_DIR) &&
		(param->p_type[i] == DIR_CODE))
		return (1);
	if (((g_op_tab[op_index].param_types[i] & T_IND) == T_IND) &&
		(param->p_type[i] == IND_CODE))
		return (1);
	return (0);
}

int			check_pcode(const t_pc_elem *op, const t_params *param)
{
	int	i;
	int	global_p_size;
	int	is_valid;
	int	op_index;

	is_valid = 1;
	op_index = op->op_code == 0 || op->op_code > 16 ? 16 : op->op_code - 1;
	global_p_size = g_op_tab[op_index].has_pcode == 1 ? 1 : 0;
	i = g_op_tab[op_index].nb_params;
	while (--i >= 0)
	{
		if (check_type(param, i, op_index) == 0)
			is_valid = 0;
		if (param->p_type[i] == REG_CODE)
			global_p_size += P_SIZE_REG;
		else if (param->p_type[i] == IND_CODE)
			global_p_size += P_SIZE_IND;
		else if (param->p_type[i] == DIR_CODE)
			global_p_size += P_SIZE_DIR(op_index);
	}
	if (is_valid == 1)
		show_operations_param((t_pc_elem *)op, (t_params *)param);
	return (is_valid == 1 ? global_p_size : -global_p_size);
}
