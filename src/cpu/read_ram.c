/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_ram.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 15:02:43 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:16:56 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

int		read_in_ram(t_pc_elem *op, int index, int size)
{
	int				mem_val;
	unsigned char	*ptr;
	unsigned char	cpy_tab[DIR_SIZE];
	int				i;
	int				j;

	i = 0;
	j = size;
	while (op->adr_crnt_pros + index + i < op->ram_orgn + op->vm->memory_size
			&& i < size)
	{
		ptr = op->adr_crnt_pros + index + (i++);
		cpy_tab[--j] = *ptr;
	}
	while (op->adr_crnt_pros + index + i > op->ram_orgn + op->vm->memory_size
			&& i < size)
	{
		ptr = op->adr_crnt_pros + index + (i++) - op->vm->memory_size;
		cpy_tab[--j] = *ptr;
	}
	while (--j >= 0)
		cpy_tab[j] = 0;
	ft_memcpy(&mem_val, cpy_tab, size);
	return (mem_val);
}
