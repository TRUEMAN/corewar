/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fetch_next_op.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/21 17:37:07 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:13:09 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"
#include "flags.h"
#include "events.h"
#include "corewar.h"
#include "flags.h"

int			fetch_next_op(t_pc_elem *op, int op_len, int nxt_op_dst)
{
	int		succes;

	if (op->vm->args.enable_visualiser == 1)
		on_process_left(((t_vm *)op->vm)->visual,
			op->adr_crnt_pros - op->ram_orgn);
	succes = op_len > 0 ? 0 : -1;
	if (op_len < 0)
		op_len = -op_len;
	if ((op->op_code != 0x09) && (op->op_code != 0) &&
		((op->op_code != 0x0c || op->op_code != 0x0f) && op_len != 1))
		show_pc_moves_verbosity(op, op_len);
	if (op->op_code == 0x09 || op->op_code == 0x0c || op->op_code == 0x0f)
		op->adr_crnt_pros += nxt_op_dst;
	else
		op->adr_crnt_pros += op_len;
	if (op->adr_crnt_pros >= op->ram_orgn + op->vm->memory_size)
		op->adr_crnt_pros -= op->vm->memory_size;
	else if (op->adr_crnt_pros < op->ram_orgn)
		op->adr_crnt_pros += op->vm->memory_size;
	op->step_in = 0;
	if (op->vm->args.enable_visualiser == 1)
		on_process_arrived(((t_vm *)op->vm)->visual,
			op->adr_crnt_pros - op->ram_orgn);
	return (succes);
}
