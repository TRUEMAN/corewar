/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lld.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 03:43:00 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:29:51 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

int			lld(t_pc_elem *ptr)
{
	t_params	param;
	int			param_len;
	int32_t		ld_val;

	get_param(ptr, &param);
	if ((param_len = check_pcode(ptr, &param)) < 0)
		return (fetch_next_op(ptr, param_len - 1, 0));
	if (param.p_type[0] == IND_CODE)
	{
		ld_val = get_indirect_val(get_indirect_adr(param.val[0], ptr), ptr);
		ft_memcpy(ptr->reg[param.val[1] - 1] + (REG_SIZE - IND_SIZE),
				&ld_val, IND_SIZE);
	}
	else if (param.p_type[0] == DIR_CODE)
	{
		ld_val = param.val[0];
		reverse_bytes(&param.val[0], DIR_SIZE);
		ft_memcpy(ptr->reg[param.val[1] - 1], &(param.val[0]), DIR_SIZE);
	}
	ptr->carry = *((int*)ptr->reg[param.val[1] - 1]) == 0 ? 1 : 0;
	return (fetch_next_op(ptr, param_len + 1, 0));
}
