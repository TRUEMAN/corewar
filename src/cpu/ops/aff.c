/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 14:06:34 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:34:48 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"
#include "events.h"
#include "flags.h"

int			aff(t_pc_elem *ptr)
{
	t_params	prm;
	int			op_len;
	char		character;

	get_param(ptr, &prm);
	if ((op_len = check_pcode(ptr, &prm)) < 0)
		return (fetch_next_op(ptr, op_len - 1, 0));
	character = *((char *)ptr->reg[prm.val[0] - 1] + 3);
	if (ptr->vm->args.enable_visualiser == 1)
		on_output((ptr->vm)->visual, ptr->process_owner, character);
	else if (ptr->vm->args.aff == 1)
		ft_printf("Aff: %c\n", character);
	return (fetch_next_op(ptr, op_len + 1, 3));
}
