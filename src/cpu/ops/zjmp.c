/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zjmp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 14:00:10 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:31:54 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"
#include "flags.h"
#include "events.h"

int				zjmp(t_pc_elem *ptr)
{
	t_params	prm;
	int			dst;
	int16_t		tmp;

	get_param(ptr, &prm);
	tmp = prm.val[0] & 0xffffffff;
	dst = ptr->carry == 1 ? tmp % ptr->vm->index_mod : 3;
	on_process_jumped((ptr->vm)->visual);
	show_operations_param(ptr, &prm);
	return (fetch_next_op(ptr, 3, dst));
}
