/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ldi.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/17 22:36:07 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:29:07 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

static void		get_val_one(t_params prm, t_pc_elem *ptr, int *val)
{
	if (prm.p_type[0] == DIR_CODE)
		*val = prm.val[0];
	else if (prm.p_type[0] == REG_CODE)
	{
		ft_memcpy(val, ptr->reg[prm.val[0] - 1], REG_SIZE);
		reverse_bytes(val, DIR_SIZE);
	}
	else
	{
		*val = get_indirect_val(get_indirect_adr(prm.val[0], ptr), ptr);
		reverse_bytes(val, DIR_SIZE);
	}
}

int				ldi(t_pc_elem *ptr)
{
	t_params	prm;
	int			op_len;
	int			val[3];

	get_param(ptr, &prm);
	if ((op_len = check_pcode(ptr, &prm)) < 0)
		return (fetch_next_op(ptr, op_len - 1, 0));
	get_val_one(prm, ptr, &val[0]);
	if (prm.p_type[1] == DIR_CODE)
		val[1] = prm.val[1];
	else
	{
		ft_memcpy(&val[1], ptr->reg[prm.val[1] - 1], REG_SIZE);
		reverse_bytes(&val[1], DIR_SIZE);
	}
	val[2] = get_indirect_val(get_indirect_adr(val[0] + val[1], ptr), ptr);
	ft_memcpy(ptr->reg[prm.val[2] - 1], &val[2], REG_SIZE);
	return (fetch_next_op(ptr, op_len + 1, 0));
}
