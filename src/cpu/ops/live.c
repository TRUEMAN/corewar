/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   live.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 02:59:41 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:29:37 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"
#include "events.h"
#include "flags.h"

static int	is_it_a_champ_id(t_vm *vm, int id)
{
	int		i;

	i = -1;
	while (++i < vm->args.nb_player)
		if (vm->args.champions_id[i] == id)
			return (i);
	return (-1);
}

int			live(t_pc_elem *ptr)
{
	t_params	param;
	int			live_index;

	get_param(ptr, &param);
	if (ptr == NULL)
		return (-1);
	show_operations_param(ptr, &param);
	ptr->nb_live_since_last_cycle++;
	ptr->nb_live++;
	live_index = is_it_a_champ_id(ptr->vm, param.val[0]);
	if (live_index != -1)
	{
		ptr->vm->winner = param.val[0];
		show_live_verbosity(ptr->vm, live_index);
		if (ptr->vm->args.enable_visualiser == 1)
			on_live((ptr->vm)->visual, (size_t)live_index,
					(size_t)((uintptr_t)ptr->adr_crnt_pros -
					(uintptr_t)ptr->ram_orgn));
	}
	ptr->vm->nb_live_in_crnt_periode++;
	ptr->last_live_cycle = ptr->vm->nb_cycles;
	return (fetch_next_op(ptr, 5, 0));
}
