/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/18 17:01:38 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:28:28 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"
#include "flags.h"

int				sfork(t_pc_elem *ptr)
{
	t_params	prm;
	t_pc_lst	**new_pc_orgn;
	int16_t		tmp;

	get_param(ptr, &prm);
	if (pc_add_process(ptr->pc_orgn, ptr) == 0)
	{
		new_pc_orgn = ptr->pc_orgn;
		tmp = 0xffff & prm.val[0];
		fetch_next_op((*new_pc_orgn)->process, 1, tmp % ptr->vm->index_mod);
	}
	show_operations_param(ptr, &prm);
	return (fetch_next_op(ptr, 3, 3));
}
