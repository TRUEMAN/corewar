/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   and.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 13:42:15 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:28:12 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

static void	get_val_one(t_params param, t_pc_elem *ptr, int *val)
{
	if (param.p_type[0] == DIR_CODE)
	{
		*val = param.val[0];
		reverse_bytes(val, DIR_SIZE);
	}
	else if (param.p_type[0] == REG_CODE)
		ft_memcpy(val, ptr->reg[param.val[0] - 1], REG_SIZE);
	else if (param.p_type[0] == IND_CODE)
		*val = get_indirect_val(get_indirect_adr(param.val[0], ptr), ptr);
}

int			comp_and(t_pc_elem *ptr)
{
	t_params	param;
	int32_t		op_len;
	int32_t		val[3];

	get_param(ptr, &param);
	if ((op_len = check_pcode(ptr, &param)) < 0)
		return (fetch_next_op(ptr, op_len - 1, 0));
	get_val_one(param, ptr, &val[0]);
	if (param.p_type[1] == DIR_CODE)
	{
		val[1] = param.val[1];
		reverse_bytes(&val[1], DIR_SIZE);
	}
	else if (param.p_type[1] == REG_CODE)
		ft_memcpy(&val[1], ptr->reg[param.val[1] - 1], REG_SIZE);
	else if (param.p_type[1] == IND_CODE)
		val[1] = get_indirect_val(get_indirect_adr(param.val[1], ptr), ptr);
	val[2] = val[0] & val[1];
	ft_memcpy(ptr->reg[param.val[2] - 1], &val[2], REG_SIZE);
	ptr->carry = *((int*)ptr->reg[param.val[2] - 1]) == 0 ? 1 : 0;
	return (fetch_next_op(ptr, op_len + 1, 0));
}
