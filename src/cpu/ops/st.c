/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   st.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 13:42:15 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:30:41 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

int			st(t_pc_elem *ptr)
{
	t_params	prm;
	int			param_len;
	int16_t		ind;

	get_param(ptr, &prm);
	if ((param_len = check_pcode(ptr, &prm)) < 0)
		return (fetch_next_op(ptr, param_len - 1, 0));
	if (prm.p_type[1] == REG_CODE)
		ft_memcpy(ptr->reg[prm.val[1] - 1], ptr->reg[prm.val[0] - 1], DIR_SIZE);
	else if (prm.p_type[1] == IND_CODE)
	{
		ind = 0;
		ind |= prm.val[1];
		write_in_ram(get_indirect_adr(ind, ptr), ptr,
		*((int32_t *)ptr->reg[prm.val[0] - 1]));
	}
	return (fetch_next_op(ptr, param_len + 1, 0));
}
