/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sub.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 11:45:33 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:31:17 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

int			sub(t_pc_elem *ptr)
{
	t_params	prm;
	int			op_len;
	int			sum;
	int			val[2];

	get_param(ptr, &prm);
	if ((op_len = check_pcode(ptr, &prm)) < 0)
		return (fetch_next_op(ptr, op_len - 1, 0));
	val[0] = *((int*)ptr->reg[prm.val[0] - 1]);
	val[1] = *((int*)ptr->reg[prm.val[1] - 1]);
	reverse_bytes(&val[0], REG_SIZE);
	reverse_bytes(&val[1], REG_SIZE);
	sum = val[0] - val[1];
	reverse_bytes(&sum, REG_SIZE);
	ft_memcpy(ptr->reg[prm.val[2] - 1], &sum, REG_SIZE);
	ptr->carry = *((int*)ptr->reg[prm.val[2] - 1]) == 0 ? 1 : 0;
	return (fetch_next_op(ptr, op_len + 1, 0));
}
