/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_in_ram.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/05 15:33:46 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:18:43 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"
#include "events.h"

static void	wrt_visu(t_visual *visual, void *adr, t_pc_elem *op, uint32_t val)
{
	size_t	addr;

	addr = (size_t)((uintptr_t)adr - (uintptr_t)op->ram_orgn);
	on_write(visual, op->process_owner + 1, (addr + 3) % op->vm->memory_size,
	(uint8_t)((val >> 24) & 0xFF));
	on_write(visual, op->process_owner + 1, (addr + 2) % op->vm->memory_size,
	(uint8_t)((val >> 16) & 0xFF));
	on_write(visual, op->process_owner + 1, (addr + 1) % op->vm->memory_size,
	(uint8_t)((val >> 8) & 0xFF));
	on_write(visual, op->process_owner + 1, (addr + 0) % op->vm->memory_size,
	(uint8_t)((val >> 0) & 0xFF));
}

/*
** Receive a value in little indiant and write it in the ram at the given
** adress.
*/

void		write_in_ram(void *adr, const t_pc_elem *op, int32_t val)
{
	int		over_lap;
	int		diff_var;
	void	*cpy;

	over_lap = adr - op->ram_orgn - op->vm->memory_size + 4;
	cpy = (void*)&val;
	if (over_lap > 0)
	{
		diff_var = (R_IND_SIZE(op)) - over_lap;
		ft_memmove(adr, cpy, diff_var);
		ft_memmove(adr - op->vm->memory_size + diff_var,
					cpy + diff_var, over_lap);
	}
	else
		ft_memmove(adr, cpy, DIR_SIZE);
	if (op->vm->args.enable_visualiser == 1)
		wrt_visu(((t_vm *)op->vm)->visual, adr, (t_pc_elem *)op, val);
}
