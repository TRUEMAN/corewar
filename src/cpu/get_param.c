/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_param.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <vnoon@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 14:25:22 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:25:32 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

static void	get_pcode(t_pc_elem *op, unsigned char *p_type, int *op_index)
{
	int				i;
	unsigned char	p_code;

	*op_index = op->op_code == 0 || op->op_code > 16 ? 16 : op->op_code - 1;
	if (g_op_tab[*op_index].has_pcode == 0)
	{
		p_type[0] = DIR_CODE;
		return ;
	}
	if (op->adr_crnt_pros + 1 >= op->ram_orgn + op->vm->memory_size)
		p_code = *((unsigned char *)op->ram_orgn);
	else
		p_code = *((unsigned char *)op->adr_crnt_pros + 1);
	i = g_op_tab[*op_index].nb_params;
	while (--i >= 0)
		p_type[i] = ((p_code << 2 * i) & 0xc0) >> 6;
}

static int	get_non_reg(t_params *param, int i, t_pc_elem *op, int op_index)
{
	int		index;
	int		j;
	int		var_size;

	index = g_op_tab[op_index].has_pcode == 1 ? 2 : 1;
	j = -1;
	while (++j < i)
		if (param->p_type[j] == DIR_CODE)
			index += P_SIZE_DIR(op_index);
		else if (param->p_type[j] == IND_CODE)
			index += P_SIZE_IND;
		else
			index += P_SIZE_REG;
	var_size = param->p_type[i] == DIR_CODE ? P_SIZE_DIR(op_index) : P_SIZE_IND;
	return (param->val[i] = read_in_ram(op, index, var_size));
}

static void	get_reg(t_params *param, int i, t_pc_elem *op, int op_index)
{
	int		index;
	int		j;
	void	*adr;

	index = g_op_tab[op_index].has_pcode == 1 ? 2 : 1;
	j = -1;
	while (++j < i)
		if (param->p_type[j] == DIR_CODE)
			index += P_SIZE_DIR(op_index);
		else if (param->p_type[j] == IND_CODE)
			index += P_SIZE_IND;
		else
			index += P_SIZE_REG;
	adr = op->adr_crnt_pros + index;
	while (adr >= op->ram_orgn + op->vm->memory_size)
		adr -= op->vm->memory_size;
	j = (int)*((unsigned char *)adr);
	if (j > 0 && j <= REG_NUMBER)
	{
		param->val[i] = *((unsigned char *)adr);
		param->adr_param[i] = adr;
	}
}

void		get_param(t_pc_elem *op, t_params *param)
{
	int			op_index;
	int			i;

	get_pcode(op, param->p_type, &op_index);
	i = g_op_tab[op_index].nb_params;
	while (--i >= 0)
	{
		if (param->p_type[i] == DIR_CODE || param->p_type[i] == IND_CODE)
			param->val[i] = get_non_reg(param, i, op, op_index);
		else
			get_reg(param, i, op, op_index);
	}
}
