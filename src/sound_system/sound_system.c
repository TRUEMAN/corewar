/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sound_system.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:08:40 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/24 14:20:45 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sound_system.h"
#include "error.h"
#include <libft.h>

t_sound_system	*sound_system_new(void)
{
	t_sound_system	*obj;
	char			*tmp;

	obj = ft_xmemalloc(sizeof(*obj));
	if (!(tmp = SDL_GetBasePath()))
		return ((t_sound_system *)err_abort(SDL_GetError(), 1));
	obj->base_path = ft_xpathadd(tmp, "res/");
	free(tmp);
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		err_abort(Mix_GetError(), 1);
	obj->sounds[KILL_SOUND] = sound_system_load_wav(obj, "critical.wav");
	obj->sounds[LIVE_SOUND] = sound_system_load_wav(obj, "nsmb_coin.wav");
	obj->sounds[JUMP_SOUND] = sound_system_load_wav(obj, "nsmb_jump.wav");
	obj->sounds[FORK_SOUND] = sound_system_load_wav(obj, "nsmb_power-up.wav");
	obj->sounds[AFF_SOUND] = sound_system_load_wav(obj, "ding.wav");
	obj->sounds[WIN_SOUND] = sound_system_load_wav(obj, "tada.wav");
	return (obj);
}

void			sound_system_delete(t_sound_system *obj)
{
	size_t	i;

	if (obj->base_path)
		free(obj->base_path);
	i = sizeof(obj->sounds) / sizeof(*obj->sounds);
	while (i--)
		Mix_FreeChunk(obj->sounds[i]);
	Mix_CloseAudio();
	Mix_Quit();
	free(obj);
}

Mix_Chunk		*sound_system_load_wav(t_sound_system *obj, char *filename)
{
	char		*path;
	Mix_Chunk	*chunk;

	path = ft_xpathadd(obj->base_path, filename);
	if (!(chunk = Mix_LoadWAV(path)))
		err_abort(Mix_GetError(), 1);
	free(path);
	return (chunk);
}

void			sound_system_play(t_sound_system *obj, size_t id)
{
	Mix_PlayChannel(-1, obj->sounds[id], 0);
}
