/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olaurent <olaurent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/20 12:07:41 by olaurent          #+#    #+#             */
/*   Updated: 2016/11/25 14:16:39 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H
# include "op.h"
# include <fcntl.h>
# include <stdlib.h>
# include <libft.h>
# define COMMENT_CHAR_2 ';'
# define FILE_EXT ".cor"
# define AFF 0b1
# define INT_TYPE 1
# define HEX_TYPE 2
# ifndef BONUS
#  define OFFSET_BONUS 0
#  define HEX_BONUS 0
# else
#  define OFFSET_BONUS 1
#  define HEX_BONUS 1
# endif

typedef struct		s_par
{
	int				n_files;
	char			**files;
	int				o_compile;
}					t_par;

typedef struct		s_arg
{
	int				type;
	int				value;
	int				size;
	int				offset;
	char			*lbl;
}					t_arg;

typedef struct		s_file
{
	struct s_file	*next;
	char			*line;
	unsigned int	line_num;
	int				in_siz;
	uint32_t		in_pos;
	int				in_type;
	int				has_lbl;
	int				num_par;
	uint32_t		pcode;
	uint8_t			*code;
	t_arg			par[MAX_ARGS_NUMBER];
}					t_file;

typedef struct		s_lbl
{
	struct s_lbl	*next;
	char			*name;
	t_file			*ins;
}					t_lbl;

typedef struct		s_prog
{
	unsigned int	line;
	int				is_des;
	int				des_ok;
	int				s_des;
	char			*desc;
	int				is_tit;
	int				tit_ok;
	int				s_tit;
	char			*title;
	t_lbl			*lbls;
	t_lbl			*curr_lbl;
	int				is_lbl;
	t_file			*ins;
	unsigned int	prog_size;
	int				out;
}					t_prog;

int					error_out(char *s);
void				error_usage(char *name);
int					err_arg(int line_num, char *arg);
int					compile_file(int fd, char *oname, int out);
t_file				*make_new_ins(void);
t_file				*del_ins(t_file *ins);
int					init_pars(t_prog *p, t_file *ins);
int					valid_line(char *line, t_prog *p);
int					add_header(char *line, t_prog *p);
void				check_end_line(char *line, int end, t_prog *p);
int					starts_with_label(char *line, t_prog *p);
int					add_label(t_lbl *lbl, char *line, t_lbl *lbls);
t_lbl				*new_label(void);
t_lbl				*del_label(t_lbl *lbl);
int					is_ins(char *line, int lbl, t_prog *p);
int					good_arg_nb(t_prog *p, char *arg, int op);
char				*clean_arg(char *arg);
int					is_valid_arg(char *arg, int num, int op, t_prog *p);
int					arg_set(char *str, t_arg *arg, char **lbl);
int					read_reg(char *arg, int limit);
void				update_instruction(t_file *ins, int num, t_arg *arg);
int					insert_label(t_lbl *lbl, t_file *in, int argn);
int					code_instruction(t_file *ins);
void				print_ins(t_file *ins);
void				print_head(t_prog *p, int fd);
void				print_instructions(t_file *ins, int fd, t_prog *p);
int					is_num(char *str);
int					read_num(char *str, int type);
int					strhextoi(char *str);
int					hexvalue(char c);
int					len_reg(char *s);
int					len_label(char *s);
int					len_direct(char *s);
int					len_indirect(char *s);
int					num_spaces(char *s);

#endif
