/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pc.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 19:13:23 by vnoon             #+#    #+#             */
/*   Updated: 2016/10/08 15:21:15 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PC_H
# define PC_H

# include "op.h"
# include "libft.h"
# include "corewar.h"
# include "vm.h"
# include "pc_elem.h"
# include <stdlib.h>

/*
** Prototypes from pc_kill_process.c
*/

void				kill_process(t_pc_elem *process);
void				clean_pc_lst(t_pc_lst **pc);

/*
** Prototypes from pc_add_process.c
*/

int					pc_add_process(t_pc_lst **pc, t_pc_elem *process);
int					add_to_pc_lst(t_pc_lst **pc, t_pc_elem *process);

/*
** Prototypes from dup_op_byte.c
*/

void				dup_op_byte(t_pc_elem *elm);

/*
** Prototypes from pc_loader.c
*/

t_pc_lst			*pc_load_champs(t_vm *vm, t_pc_lst **pc_orgn);

#endif
