/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ram.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 22:40:14 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:01:46 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAM_H
# define RAM_H

# include "op.h"
# include "libft.h"
# include <stdlib.h>

struct s_vm;
struct s_champion;

/*
** Prototypes from set_up_ram.c
*/

void	*set_up_ram(int memory_size);

/*
** Prototypes from load_champs.c
*/
void	load_champs_in_ram(struct s_vm *vm, struct s_champion **champ,
				int nb_player);

#endif
