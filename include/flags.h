/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <vnoon@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 14:56:08 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 18:09:06 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_H
# define FLAGS_H

void		dump_ram(void *ram, t_vm *op);

void		cycle_verbosity(t_vm *vm, int cycle_to_die_update);
void		show_live_verbosity(t_vm *vm, int champion_index);
void		show_death_verbosity(t_pc_elem *op);
void		show_pc_moves_verbosity(t_pc_elem *op, int op_len);
void		show_operations_param(const t_pc_elem *op, const t_params *params);

#endif
