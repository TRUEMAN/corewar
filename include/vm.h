/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/08 15:01:50 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:30:20 by guiricha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VM_H
# define VM_H
# include "op.h"
# include <stdlib.h>

struct s_pc_lst;
struct s_champion;

typedef struct	s_champll
{
	int					number;
	char				*name;
	struct s_champll	*next;
}				t_champll;

typedef struct	s_args
{
	char		err;
	int			ignore_max_players;
	int			aff;
	int			dump_cycle;
	int			serial_dump;
	int			verbosity_val;
	int			write;
	int			live;
	int			enable_visualiser;
	int			audio;
	int			stealth_mode;
	int			nb_player;
	char		nb_player_set;
	int			param_error;
	t_champll	*champlist;
	char		**champions;
	int			*champions_id;
	char		tog_args;
	int			memory_size;
	size_t		font_size;
}				t_args;

typedef struct	s_vm
{
	void				*ram;
	struct s_pc_lst		*pc;
	struct s_pc_lst		*crnt_pc;
	struct s_champion	**champions;
	struct s_visual		*visual;
	struct s_args		args;
	int					nb_live_in_crnt_periode;
	int					end_of_cycle;
	int					end_of_cycle_base_val;
	int					winner;
	int					paused;
	int					step;
	int					quit;
	int					checks;
	int					nb_cycles;
	int					memory_size;
	int					index_mod;
	int					pause_at_next_operation;
	int					cycle_per_sec;
	int					cycle_nxt_op;
	size_t				last_tick;
	size_t				interval;
	size_t				font_size;
}				t_vm;

#endif
