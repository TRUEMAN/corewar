/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 16:00:09 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 16:00:09 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVENTS_H
# define EVENTS_H
# include "visual.h"
# include "sound_system.h"
# include <stdint.h>

void	on_write(t_visual *visual, size_t champ, size_t addr, int8_t value);
void	on_process_left(t_visual *visual, size_t addr);
void	on_process_arrived(t_visual *visual, size_t addr);
void	on_set_speed(t_visual *visual, int modifier);
void	on_next_cycle(t_visual *visual);
void	on_cycle_delta_changed(t_visual *visual, size_t delta);
void	on_live(t_visual *visual, size_t champ, size_t addr);
void	on_output(t_visual *visual, size_t champ, char character);
void	on_process_forked(t_visual *visual);
void	on_process_killed(t_visual *visual);
void	on_process_jumped(t_visual *visual);

#endif
