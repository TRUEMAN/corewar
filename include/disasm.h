/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   disasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 19:04:11 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/20 13:12:27 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISASM_H
# define DISASM_H
# include "op.h"
# include "memory.h"
# include <unistd.h>
# include <stdint.h>

typedef struct	s_memory
{
	size_t		size;
	uint8_t		*mem;
}				t_memory;

typedef struct	s_dec_op
{
	t_op			*op;
	size_t			size;
	size_t			addr;
	uint8_t			opcode;
	uint8_t			bitmask;
	uint8_t			bits[4];
	uint32_t		params[4];
	struct s_dec_op	*labels[4];
	int				label;
	struct s_dec_op	*next;
	struct s_dec_op	*prev;
}				t_dec_op;

typedef struct	s_dec_champion
{
	t_header	*header;
	t_memory	*code;
	t_dec_op	*operations;
}				t_dec_champion;

int				decompile_file(char *name);
t_dec_op		*read_operation(t_memory *mem, size_t addr);
void			resolve_labels(t_dec_champion *champ);
void			set_label_ids(t_dec_champion *champ);
size_t			get_param_size(t_dec_op *op, size_t index);
int32_t			get_param_as_int(t_dec_op *op, size_t index);
t_memory		*create_memory(size_t size);
void			destroy_memory(t_memory **obj);
void			put_error(int error_code);
void			put_source(t_dec_champion *champ);
void			reverse_bytes(void *mem, size_t size);

#endif
