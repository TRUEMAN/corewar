/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pc_elem.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/08 15:18:05 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:02:22 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PC_ELEM_H
# define PC_ELEM_H

struct s_vm;

typedef struct		s_pc_elem
{
	struct s_vm		*vm;
	void			*adr_crnt_pros;
	void			*ram_orgn;
	void			**reg;
	struct s_pc_lst	**pc_orgn;
	int				step_in;
	int				cpt_cycles;
	int				instruct_cycle;
	int				process_owner;
	int				nb_live;
	int				nb_live_since_last_cycle;
	int				carry;
	int				last_live_cycle;
	unsigned char	op_code;
	unsigned int	proc_num;
}					t_pc_elem;

typedef struct		s_pc_lst
{
	t_pc_elem		*process;
	struct s_pc_lst	*next;
	struct s_pc_lst	*prev;
}					t_pc_lst;

#endif
