/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 15:58:43 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 15:58:43 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_H
# define ERROR_H

# define INT_MIN_E -1
# define INT_MAX_E -2
# define INPUT_NAN_E -3
# define INPUT_NULL_E -4
# define N_ALREADY_USED_E -5
# define NO_PLAYERS_E -6
# define MALLOC_E -7
# define NULL_ARG_E -8
# define INVALID_PARAM_E -9
# define VERB_OUT_OF_RANGE_E -10
# define MEM_TOO_SMALL_E -11
# define DUMP_C_OUT_OF_RANGE_E -12
# define S_DUMP_OUT_OF_RANGE_E -13
# define TOO_MANY_PLAYERS_E -14

int		put_error(const char *msg, int error_code);
void	*err_abort(const char *msg, int quit);

#endif
