/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   workloop_steps.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 13:15:43 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:03:10 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WORKLOOP_STEPS_H
# define WORKLOOP_STEPS_H

int		cycle_loop(t_vm *vm);
void	clean_post_end_of_loop(t_vm *vm);
void	starting_message(const t_vm *vm);

#endif
