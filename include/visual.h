/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:39:58 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 15:38:37 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VISUAL_H
# define VISUAL_H
# include "vm.h"
# include "sound_system.h"
# include <SDL.h>
# include <SDL_ttf.h>
# include <stddef.h>
# include <stdint.h>

typedef struct	s_mem
{
	size_t	size;
	size_t	champ_count;
	int		process_count;
	uint8_t	*mem;
	uint8_t	*ids;
	size_t	*pcs;
	size_t	*changed;
	size_t	*live;
}				t_mem;

typedef struct	s_byte_texture {
	SDL_Texture	*texture;
	SDL_Rect	size;
}				t_byte_texture;

typedef struct	s_memview
{
	struct s_visual	*visual;
	SDL_Renderer	*ren;
	SDL_Rect		size;
	SDL_Texture		*texture;
	t_mem			*mem;
	char			*font_path;
	TTF_Font		*font;
	int				font_size;
	size_t			byte_count;
	t_byte_texture	**bytes;
	int				line_size;
	int				line_max;
	int				line_count;
	int				line_offset;
	int				line_height;
	SDL_Texture		**lines;
	SDL_Rect		byte_size;
	SDL_Rect		byte_full_size;
	SDL_Color		colors[10];
	int				scroll;
	int				scroll_max;
	float			scroll_fp;
	int				stealth;
}				t_memview;

typedef struct	s_champ
{
	struct s_champ_list	*list;
	char				*name;
	int					id;
	int					total_lives;
	int					lives;
	int					prev_lives;
	SDL_Texture			*texture;
	SDL_Rect			size;
	SDL_Texture			*textures[4];
	SDL_Rect			sizes[4];
	SDL_Rect			srcs[4];
	SDL_Rect			dsts[4];
}				t_champ;

typedef struct	s_champ_list
{
	struct s_visual	*visual;
	SDL_Renderer	*ren;
	SDL_Rect		size_top;
	SDL_Rect		size_list;
	SDL_Texture		*texture_top;
	SDL_Texture		*texture_list;
	char			*font_path;
	TTF_Font		*font;
	TTF_Font		*font2;
	size_t			font_size;
	size_t			font2_size;
	size_t			champ_count;
	t_champ			**champs;
	size_t			cycle;
	SDL_Color		colors[12];
	SDL_Texture		*textures[13];
	SDL_Rect		sizes[13];
	float			name_scroll;
	float			name_scroll_factor;
	float			name_scroll_pos;
	int				count_down;
	int				scroll;
	int				scroll_max;
	float			scroll_fp;
}				t_champ_list;

typedef struct	s_console
{
	struct s_visual	*visual;
	SDL_Renderer	*ren;
	SDL_Rect		size;
	SDL_Texture		*texture;
	t_mem			*mem;
	char			*font_path;
	TTF_Font		*font;
	int				font_size;
	SDL_Color		colors[12];
	SDL_Texture		*title_texture;
	SDL_Rect		title_size;
	SDL_Texture		**textures;
	SDL_Rect		*sizes;
	size_t			line_height;
	size_t			name_len;
	char			**strings;
	size_t			*string_len;
	size_t			char_width;
	size_t			char_count;
	int				scroll;
	int				scroll_max;
	float			scroll_fp;
}				t_console;

typedef struct	s_visual
{
	SDL_Window		*win;
	SDL_Renderer	*ren;
	struct s_vm		*vm;
	t_sound_system	*sys;
	int				width;
	int				height;
	char			*base_path;
	int				cur_x;
	int				cur_y;
	size_t			tick_rate;
	size_t			last_tick;
	size_t			current_tick;
	t_mem			*mem;
	t_memview		*memview;
	t_champ_list	*champ_list;
	t_console		*console;
	SDL_Color		colors[2];
	size_t			cycle;
	size_t			cycle_delta;
	int				display_writes;
	int				display_lives;
	int				stealth;
	int				sounds[6];
}				t_visual;

t_mem			*mem_new(size_t size, size_t champ_count);
void			mem_delete(t_mem *obj);

t_byte_texture	*byte_texture_new(t_memview *memview, uint8_t value,
						SDL_Color color);
void			byte_texture_delete(t_byte_texture *obj);

t_memview		*memview_new(t_visual *visual, size_t font_size);
void			memview_delete(t_memview *obj);
void			memview_delete_lines(t_memview *obj);
void			memview_set_colors(t_memview *obj);
void			memview_update(t_memview *obj);
void			memview_update_lines(t_memview *obj);
void			memview_update_byte(t_memview *obj, size_t index);
void			memview_draw(t_memview *obj);
void			memview_set_font_size(t_memview *obj, int font_size);
void			memview_on_event(t_memview *obj, SDL_Event *event,
						int x, int y);

t_visual		*visual_new(int width, int height, t_mem *mem,
						t_vm *vm);
void			visual_delete(t_visual *obj);
void			visual_set_colors(t_visual *obj);
int				visual_loop(t_visual *obj);
int				visual_event_loop(t_visual *obj);
void			visual_on_resize(t_visual *obj, int width, int height);
void			visual_on_tick(t_visual *obj);
void			visual_draw(t_visual *obj);

t_champ			*champ_new(t_champ_list *list, char *name, int id);
void			champ_delete(t_champ *obj);
void			champ_update(t_champ *obj);
void			champ_update_lives(t_champ *obj);
void			champ_draw(t_champ *obj, int y);

t_champ_list	*champ_list_new(t_visual *visual, t_vm *vm);
void			champ_list_delete(t_champ_list *obj);
void			champ_list_set_colors(t_champ_list *obj);
void			champ_list_update(t_champ_list *obj);
void			champ_list_update_cycle(t_champ_list *obj);
void			champ_list_update_speed(t_champ_list *obj);
void			champ_list_draw(t_champ_list *obj);
void			champ_list_on_event(t_champ_list *obj, SDL_Event *event, int x,
						int y);

t_console		*console_new(t_visual *visual);
void			console_delete(t_console *obj);
void			console_set_colors(t_console *obj);
void			console_update(t_console *obj);
void			console_draw(t_console *obj);
void			console_add_character(t_console *obj, size_t champ,
						char character);
void			console_update_string(t_console *obj, size_t index);
void			console_on_event(t_console *obj, SDL_Event *event, int x,
						int y);

t_visual		*initialize_visu(t_vm *vm);
void			terminate_visu(t_visual *visual);

#endif
