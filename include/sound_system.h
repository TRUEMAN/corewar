/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sound_system.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:40:10 by jandre-j          #+#    #+#             */
/*   Updated: 2016/11/23 15:38:10 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SOUND_SYSTEM_H
# define SOUND_SYSTEM_H
# include <SDL.h>
# include <SDL_mixer.h>
# define KILL_SOUND	0
# define LIVE_SOUND	1
# define JUMP_SOUND	2
# define FORK_SOUND	3
# define AFF_SOUND	4
# define WIN_SOUND	5

typedef struct	s_sound_system
{
	char		*base_path;
	Mix_Chunk	*sounds[6];
}				t_sound_system;

t_sound_system	*sound_system_new(void);
void			sound_system_delete(t_sound_system *obj);
Mix_Chunk		*sound_system_load_wav(t_sound_system *obj, char *filename);
void			sound_system_play(t_sound_system *obj, size_t id);

#endif
