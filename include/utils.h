/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/10 11:55:50 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:02:43 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H
# include "visual.h"
# include <SDL.h>
# include <stdint.h>
# include <unistd.h>

void		reverse_byte(void *mem, size_t size);
char		get_hex_representation(uint8_t value);
void		byte_to_string(uint8_t value, char buffer[3]);
SDL_Color	get_color(uint32_t color);
void		set_sdl_color(SDL_Renderer *ren, SDL_Color *index);
SDL_Texture	*create_sdl_texture(SDL_Renderer *ren, int width, int height);
SDL_Texture	*sdl_render_text(SDL_Renderer *ren, TTF_Font *font, char *text,
					void *params[2]);
SDL_Texture	*sdl_render_utf8(SDL_Renderer *ren, TTF_Font *font, char *text,
					void *params[2]);
int			int_clamp(int value, int min, int max);
int			int_min(int a, int b);
int			int_max(int a, int b);
float		float_clamp(float value, float min, float max);
float		float_serp(float start, float end, float value);
size_t		utf8_len(char *str);

#endif
