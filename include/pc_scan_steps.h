/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pc_scan_steps.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 12:59:37 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/23 16:00:49 by jandre-j         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PC_SCAN_STEPS_H
# define PC_SCAN_STEPS_H

# include "pc.h"

void	step_in_operation(t_pc_elem *process);

#endif
