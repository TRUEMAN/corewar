/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cpu.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 17:12:29 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 15:26:29 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPU_H
# define CPU_H

# define P_SIZE_DIR(op) g_op_tab[op].has_idx == 1 ? 2 : 4
# define P_SIZE_IND 2
# define P_SIZE_REG 1

# define IS_DIR(param, i) param->val[i] == DIR_CODE
# define R_IND_SIZE(op) op->op_code == 12 ? 2 : 4

# include "pc.h"

typedef struct		s_params
{
	int				val[4];
	void			*adr_param[4];
	unsigned char	p_type[4];
}					t_params;

/*
** Prototypes from get_indirect.c
*/

void				*get_indirect_adr(int ind_val, const t_pc_elem *op);
int					get_indirect_val(const void *adr, const t_pc_elem *op);

/*
** Prototypes form write_in_ram
*/

void				write_in_ram(void *adr, const t_pc_elem *op, int val);

/*
** Prototypes from read_ram
*/

int					read_in_ram(t_pc_elem *op, int index, int size);

/*
** Prototypes from get_param.c
*/

void				get_param(t_pc_elem *elem, t_params *param);

/*
** Prototypes from check_pcode.c
*/

int					check_pcode(const t_pc_elem *op, const t_params *param);

/*
** Prototypes from fetch_next_op.c
*/

int					fetch_next_op(t_pc_elem *op, int op_len, int nxt_op_dst);

/*
** eval_op.c
*/

int					eval_op(t_pc_elem *op);

/*
** Prototypes in the OPS/ directory
*/

int					zero(t_pc_elem *ptr);
int					live(t_pc_elem *ptr);
int					ld(t_pc_elem *ptr);
int					st(t_pc_elem *ptr);
int					add(t_pc_elem *ptr);
int					sub(t_pc_elem *ptr);
int					comp_and(t_pc_elem *ptr);
int					comp_or(t_pc_elem *ptr);
int					comp_xor(t_pc_elem *ptr);
int					zjmp(t_pc_elem *ptr);
int					ldi(t_pc_elem *ptr);
int					sti(t_pc_elem *ptr);
int					sfork(t_pc_elem *ptr);
int					lld(t_pc_elem *ptr);
int					lldi(t_pc_elem *ptr);
int					lfork(t_pc_elem *ptr);
int					aff(t_pc_elem *ptr);

#endif
