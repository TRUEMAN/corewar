/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jandre-j <jandre-j@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 14:37:29 by vnoon             #+#    #+#             */
/*   Updated: 2016/11/25 13:56:59 by olaurent         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COREWAR_H
# define COREWAR_H

# include "cpu.h"
# include "pc.h"
# include "ram.h"
# include "op.h"
# include "vm.h"
# include <stdint.h>
# include <unistd.h>
# include <fcntl.h>

# define VALID_ARGS	" -d -s -v -n --stealth -a "

typedef struct	s_champion
{
	t_header	*header;
	uint8_t		*code;
	size_t		size;
	int			number;
}				t_champion;

/*
** Prototypes from load_champion.c
*/

t_champion		*load_champion(char *filename, int number, int *err);
void			reverse_bytes(void *mem, size_t size);

/*
** Prototypes from pars_args.c
*/

int				pars_args(int argc, char **argv, t_args *args);

/*
** Prototypes from arg_functions.c
*/

int				parse_argument_ext(char *arg, t_args *args);
int				next_available(t_champll *start);
int				is_used(int nbr, t_champll *start);
void			init_flags(t_args *arg);

/*
** Prototypes from print_arg_functions.c
*/

void			print_arguments(t_args *args);
int				parse_error(t_args *args, char *arg);
void			input_error(t_args *a);
int				usage_options(void);

/*
** Prototypes from init_vm.c
*/

int				init_vm(t_vm *vm, int nb_args, char **args);

/*
** Prototypes from ft_xmalloc.c
*/

void			*ft_xmalloc(size_t size);

#endif
